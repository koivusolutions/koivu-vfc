package com.koivusolutions.vfc.nifi.processor.util;

import static java.time.temporal.ChronoField.YEAR;

import java.time.*;
import java.time.chrono.IsoChronology;
import java.time.format.*;
import java.time.temporal.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;

import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@SuppressWarnings("boxing")
@Tags({"Koivu", "ValueFlow", "VFC", "Koivu.Cloud", "Date", "Formatter", "ISODate"})
@CapabilityDescription("Takes in a ISO date and formats with by given format strings and adds formatted strings as attributes")
@InputRequirement(InputRequirement.Requirement.INPUT_ALLOWED)
@DynamicProperty(name = "The property name for the inserted attribute for formatted date.", value = "ISODate format string. See Java SimpleDateTime. Adds Y for Week based year, q for quarter and w for week using ISO standard. ", description = "User-defined properties identify attributes to add.", expressionLanguageScope = ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
public class ISODateFormatter extends AbstractProcessor implements VfcReadProcessor
{
  private static final Map<Character, TemporalField> FIELD_MAP = new HashMap<>( );
  static
  {
    FIELD_MAP.put( 'G', ChronoField.ERA );
    FIELD_MAP.put( 'y', ChronoField.YEAR_OF_ERA );
    FIELD_MAP.put( 'u', ChronoField.DAY_OF_WEEK );
    FIELD_MAP.put( 'Q', IsoFields.QUARTER_OF_YEAR );
    FIELD_MAP.put( 'q', IsoFields.QUARTER_OF_YEAR );
    FIELD_MAP.put( 'M', ChronoField.MONTH_OF_YEAR );
    FIELD_MAP.put( 'L', ChronoField.MONTH_OF_YEAR );
    FIELD_MAP.put( 'D', ChronoField.DAY_OF_YEAR );
    FIELD_MAP.put( 'd', ChronoField.DAY_OF_MONTH );
    FIELD_MAP.put( 'F', ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH );
    FIELD_MAP.put( 'E', ChronoField.DAY_OF_WEEK );
    FIELD_MAP.put( 'c', ChronoField.DAY_OF_WEEK );
    FIELD_MAP.put( 'e', ChronoField.DAY_OF_WEEK );
    FIELD_MAP.put( 'a', ChronoField.AMPM_OF_DAY );
    FIELD_MAP.put( 'H', ChronoField.HOUR_OF_DAY );
    FIELD_MAP.put( 'k', ChronoField.CLOCK_HOUR_OF_DAY );
    FIELD_MAP.put( 'K', ChronoField.HOUR_OF_AMPM );
    FIELD_MAP.put( 'h', ChronoField.CLOCK_HOUR_OF_AMPM );
    FIELD_MAP.put( 'm', ChronoField.MINUTE_OF_HOUR );
    FIELD_MAP.put( 's', ChronoField.SECOND_OF_MINUTE );
    FIELD_MAP.put( 'S', ChronoField.MILLI_OF_SECOND );
    FIELD_MAP.put( 'A', ChronoField.MILLI_OF_DAY );
    FIELD_MAP.put( 'n', ChronoField.NANO_OF_SECOND );
    FIELD_MAP.put( 'N', ChronoField.NANO_OF_DAY );
  }

  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE;
  private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ISO_LOCAL_TIME;

  static final PropertyDescriptor PROPERTY_DATE = new PropertyDescriptor.Builder( )
      .name( "input.date" ) //
      .displayName( "Date, time or date time ISO string. If not given, current date is used" ) //
      .defaultValue( "" ) //
      .required( false ) //
      .description( "The date to be formatted." ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public ISODateFormatter( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_DATE );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile flowFile = session.get( );
    if( flowFile == null )
    {
      flowFile = session.create( );
    }
    try
    {

      String dateStr = context.getProperty( PROPERTY_DATE ).evaluateAttributeExpressions( flowFile )
          .getValue( );
      LocalDateTime ldt = stringToDateTime( dateStr );

      Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
      for( PropertyDescriptor propertyDescriptor : props )
      {
        if( propertyDescriptor.isDynamic( ) )
        {
          String name = propertyDescriptor.getName( );
          String format = context.getProperty( name ).evaluateAttributeExpressions( flowFile )
              .getValue( );
          DateTimeFormatter fmt = buildISODateTimeFormatter( format );
          VfcReadProcessor.setSessionAttribute( session, flowFile, name, fmt.format( ldt ) );
        }
      }

      session.getProvenanceReporter( ).modifyAttributes( flowFile );
      session.transfer( flowFile, REL_SUCCESS );
    }

    catch( final FlowFileAccessException t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      throw new ProcessException( t );
    }
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description( "Attributes to add. Use" ).dynamic( true ).required( false )
        .addValidator( new StringValidator( ) ).build( );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

  private LocalDateTime stringToDateTime( String datetimeStr )
  {
    LocalDateTime datetime = null;
    try
    {
      datetime = LocalDateTime.parse( datetimeStr, DATE_TIME_FORMAT );
    }
    catch( Throwable t )
    {
      if( StringUtils.length( datetimeStr ) > 10 && datetimeStr.charAt( 10 ) == ' ' )
      {
        StringBuilder buf = new StringBuilder( datetimeStr );
        buf.setCharAt( 10, 'T' );
        try
        {
          datetime = LocalDateTime.parse( buf.toString( ), DATE_TIME_FORMAT );
        }
        catch( Throwable x )
        {
        }
      }
      if( datetime == null )
      {
        LocalDate date = null;
        LocalTime time = null;
        try
        {
          date = LocalDate.parse( datetimeStr, DATE_FORMAT );
        }
        catch( Throwable t2 )
        {
          try
          {
            time = LocalTime.parse( datetimeStr, TIME_FORMAT );
          }
          catch( Throwable t3 )
          {
          }
        }
        if( date == null && time != null )
        {
          datetime = LocalDate.now( ).atTime( time );
        }
        else if( date != null )
        {
          datetime = date.atStartOfDay( );
        }
      }
    }
    if( datetime != null )
    {
      return datetime;
    }
    LocalDateTime tmpDate = LocalDateTime.now( ).truncatedTo( ChronoUnit.SECONDS );

    throw new DateTimeParseException( //
        "Cannot parse date, time or datetime from a string:'" + datetimeStr //
            + "'. Expected formats are: '" //
            + DATE_FORMAT.format( tmpDate ) + "', '" //
            + TIME_FORMAT.format( tmpDate ) + "' or '" //
            + DATE_TIME_FORMAT.format( tmpDate ) + "'" //
        , datetimeStr, 0 );

  }

  private DateTimeFormatter buildISODateTimeFormatter( String dateFmt )
  {
    DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder( );
    builder.parseCaseInsensitive( );
    for( int i = 0; i < dateFmt.length( ); )
    {
      char formatChar = dateFmt.charAt( i );
      int count = count( dateFmt, i, formatChar ) + 1;
      boolean literal = false;
      i += count;
      switch( formatChar )
      {
        case 'Y':
          builder.appendValue( IsoFields.WEEK_BASED_YEAR, count, 10, SignStyle.EXCEEDS_PAD );
          break;
        case 'y':
          builder.appendValue( YEAR, count, 10, SignStyle.EXCEEDS_PAD );
          break;
        case 'w':
          builder.appendValue( IsoFields.WEEK_OF_WEEK_BASED_YEAR, count );
          break;
        case '\'':
          literal = true;
          break;
        default:
          TemporalField tf = FIELD_MAP.get( formatChar );
          if( literal || tf == null )
          {
            builder.appendLiteral( formatChar );
          }
          else
          {
            builder.appendValue( tf, count );
          }
      }
    }
    builder.optionalStart( );
    builder.appendOffsetId( );
    return builder.toFormatter( ).withChronology( IsoChronology.INSTANCE );
  }

  private int count( String dateFmt, int i, char formatChar )
  {
    int count = 0;
    while( i + 1 < dateFmt.length( ) && dateFmt.charAt( i + 1 ) == formatChar )
    {
      i++;
      count++;
    }
    return count;
  }

}
