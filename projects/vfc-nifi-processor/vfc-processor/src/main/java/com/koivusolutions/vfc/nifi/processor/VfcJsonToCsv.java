package com.koivusolutions.vfc.nifi.processor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.StreamCallback;
import org.json.JSONArray;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.VfcNameValidator;

@Tags({"csv", "json", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Converts flow file containing VFC data in JSON format to CSV")
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@DynamicProperty(name = "The column name for the csv record.", value = "A VFC Path Expression that will be evaluated against each VFC record. The result of the VFC Path will be the value of the "
    + "field whose name is the same as the property name.", description = "User-defined properties identify how to extract specific fields from a VFC record in order to create a CSV Record", expressionLanguageScope = ExpressionLanguageScope.NONE)
public class VfcJsonToCsv extends AbstractProcessor implements VfcReadProcessor
{
  private static final String PROPERTIES_PREFIX = CloudTableRow.COLUMN_TYPE_PROPERTIES
      + VfcReadProcessor.DOT;
  private static final String OUTPUT_NAMED = "named";
  private static final String OUTPUT_ALL = "all";
  private static final String OUTPUT_ALL_PARTIALLY_NAME = "all_partially_named";

  static final PropertyDescriptor PROPERTY_KEY_FLATTENING_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "header.separator" ) //
      .displayName( "Key Separator" ) //
      .defaultValue( DOT ) //
      .required( true ) //
      .description( "The separator character used for flattening json field names." ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  static final PropertyDescriptor PROPERTY_OUTPUT = new PropertyDescriptor.Builder( )
      .name( "fields.output" ) //
      .displayName( "Output Fields" ) //
      .allowableValues( //
          new AllowableValue( OUTPUT_ALL, "All fields", "Output all fields" ), //
          new AllowableValue( OUTPUT_NAMED, "Named fields", "Output ONLY named fields" ), //
          new AllowableValue( OUTPUT_ALL_PARTIALLY_NAME, "All fields, partially named",
              "Output all fields, use name mapping, if present" ) ) //
      .defaultValue( OUTPUT_ALL ) //
      .required( true ) //
      .description( "Define fields included in the CSV output." ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public VfcJsonToCsv( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_OUTPUT );
    props.add( PROPERTY_CSV_SEPARATOR );
    props.add( PROPERTY_CSV_HEADER );
    props.add( PROPERTY_KEY_FLATTENING_SEPARATOR );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_FAILURE );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile flowFile = session.get( );
    if( flowFile == null )
    {
      return;
    }
    try
    {
      final char csvSeparator = context.getProperty( PROPERTY_CSV_SEPARATOR )
          .evaluateAttributeExpressions( flowFile ).getValue( ).charAt( 0 );
      final String csvHeader = context.getProperty( PROPERTY_CSV_HEADER )
          .evaluateAttributeExpressions( flowFile ).getValue( );

      final String[] columns = getActiveColumns( context, flowFile );
      final String[] csvColumns = getCsvColumns( context, columns );

      session.write( flowFile, new StreamCallback( ) {

        @Override
        public void process( InputStream in, OutputStream out ) throws IOException
        {
          OutputStreamWriter writer = new OutputStreamWriter( new BufferedOutputStream( out ),
              StandardCharsets.UTF_8 );
          BufferedReader reader = new BufferedReader(
              new InputStreamReader( in, StandardCharsets.UTF_8 ) );

          CSVFormat.Builder formatBuilder = CSVFormat.DEFAULT.builder( )
              .setDelimiter( csvSeparator );
          if( CSV_HEADER_YES.equals( csvHeader ) )
          {
            formatBuilder = formatBuilder.setHeader( csvColumns );
          }
          CSVFormat format = formatBuilder.build( );

          try (CSVPrinter csv = new CSVPrinter( writer, format ))
          {
            String line;
            while( ( line = reader.readLine( ) ) != null )
            {
              Object[] record = getColumns( line, columns );
              if( record != null )
              {
                csv.printRecord( record );
              }
            }
            csv.flush( );
          }
        }

        private String[] getColumns( String line, String[] flattenedColumns )
        {
          if( line != null && line.length( ) > 0 )
          {
            if( line.charAt( 0 ) == '[' || line.charAt( 0 ) == ']' )
            {
              return null;
            }
            String[] record = new String[flattenedColumns.length];
            JSONObject json = new JSONObject( line );
            for( int i = 0; i < flattenedColumns.length; i++ )
            {
              String columnKey = flattenedColumns[i];
              if( columnKey.startsWith( CloudTableRow.COLUMN_TYPE_PROPERTIES + DOT ) )
              {
                JSONObject jsonObject = json.optJSONObject( CloudTableRow.COLUMN_TYPE_PROPERTIES );
                record[i] = jsonObject != null
                    ? jsonObject.optString( columnKey
                        .substring( ( CloudTableRow.COLUMN_TYPE_PROPERTIES + DOT ).length( ) ) )
                    : "";
              }
              else if( columnKey.startsWith( CloudTableRow.COLUMN_TYPE_EXPRESSIONS + DOT ) )
              {
                JSONObject jsonObject = json.optJSONObject( CloudTableRow.COLUMN_TYPE_EXPRESSIONS );
                record[i] = jsonObject != null
                    ? jsonObject.optString( columnKey
                        .substring( ( CloudTableRow.COLUMN_TYPE_EXPRESSIONS + DOT ).length( ) ) )
                    : "";
              }
              else if( columnKey.startsWith(
                  CloudTableRow.COLUMN_TYPE_KEYS + DOT + CloudTableRow.KEY_COLUMN_PREFIX ) )
              {
                String keyNumStr = columnKey.substring(
                    ( CloudTableRow.COLUMN_TYPE_KEYS + DOT + CloudTableRow.KEY_COLUMN_PREFIX )
                        .length( ) );
                if( NumberUtils.isDigits( keyNumStr ) )
                {
                  int keyNo = NumberUtils.createInteger( keyNumStr ).intValue( ) - 1;
                  JSONArray keys = json.getJSONArray( CloudTableRow.COLUMN_TYPE_KEYS );
                  if( keyNo >= 0 && keys != null && keys.length( ) > keyNo )
                  {
                    String keyValue = keys.optString( keyNo );
                    record[i] = keyValue != null ? keyValue : "";
                  }
                }
              }
              else
              {
                String value = json.optString( columnKey );
                record[i] = value != null ? value : "";
              }
            }
            return record;
          }
          return null;
        }
      } );
      session.getProvenanceReporter( ).modifyContent( flowFile );
      VfcReadProcessor.setSessionAttribute( session, flowFile, "mime.type", "text/csv" );
      session.transfer( flowFile, REL_SUCCESS );
    }
    catch( final RuntimeException e )
    {
      Throwable t = e.getCause( );
      t = t != null ? t : e;
      getLogger( ).warn( "{} failed to process due to {}", new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
  }

  protected String[] getCsvColumns( final ProcessContext context, final String[] columns )
  {
    final String output = context.getProperty( PROPERTY_OUTPUT ).getValue( );
    boolean partiallyNamed = OUTPUT_ALL_PARTIALLY_NAME.equals( output );
    final String keySeparator = context.getProperty( PROPERTY_KEY_FLATTENING_SEPARATOR )
        .getValue( );
    Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
    final String[] csvColumns = new String[columns.length];
    for( int i = 0; i < columns.length; i++ )
    {
      csvColumns[i] = getColumnName( context, partiallyNamed, props, columns[i],
          columns[i].replaceAll( "\\" + DOT, keySeparator ) );
    }
    return csvColumns;
  }

  protected String getColumnName( final ProcessContext context, boolean partiallyNamed,
      Set<PropertyDescriptor> props, String column, String defaultValue )
  {
    for( PropertyDescriptor propertyDescriptor : props )
    {
      if( propertyDescriptor.isDynamic( ) )
      {
        String value = context.getProperty( propertyDescriptor ).getValue( );
        if( value.equals( column ) )
        {
          return propertyDescriptor.getName( );
        }
      }
    }
    if( partiallyNamed )
    {
      if( defaultValue.startsWith( PROPERTIES_PREFIX ) )
      {
        return defaultValue.substring( PROPERTIES_PREFIX.length( ) );
      }
    }
    return defaultValue;
  }

  protected String[] getActiveColumns( ProcessContext context, FlowFile flowFile )
  {
    final String output = context.getProperty( PROPERTY_OUTPUT ).getValue( );
    String[] allColumns = VfcReadProcessor.getColumns( flowFile );
    if( OUTPUT_ALL.equals( output ) || OUTPUT_ALL_PARTIALLY_NAME.equals( output ) )
    {
      return allColumns;
    }
    else
    {
      List<String> activeColumns = new ArrayList<>( );
      Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
      for( PropertyDescriptor propertyDescriptor : props )
      {
        if( propertyDescriptor.isDynamic( ) )
        {
          String value = context.getProperty( propertyDescriptor ).getValue( );
          if( ArrayUtils.contains( allColumns, value ) )
          {
            activeColumns.add( value );
          }
        }
      }
      return activeColumns.toArray( new String[activeColumns.size( )] );
    }
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description(
            "VFC Path that indicates how to retrieve the value from a VFC record for the '"
                + propertyDescriptorName + "' column" )
        .dynamic( true ).required( false ).addValidator( new VfcNameValidator( true ) ).build( );
  }

  @Override
  protected Collection<ValidationResult> customValidate( final ValidationContext validationContext )
  {
    final String output = validationContext.getProperty( PROPERTY_OUTPUT ).getValue( );
    if( OUTPUT_ALL.equals( output ) )
    {
      return Collections.emptyList( );
    }

    boolean pathSpecified = false;
    for( final PropertyDescriptor property : validationContext.getProperties( ).keySet( ) )
    {
      if( property.isDynamic( ) )
      {
        pathSpecified = true;
        break;
      }
    }

    if( pathSpecified )
    {
      return Collections.emptyList( );
    }

    return Collections.singleton( new ValidationResult.Builder( ).subject( "CSV columns" )
        .valid( false ).explanation( "No CSV Columns are defined" ).build( ) );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
