package com.koivusolutions.vfc.nifi.processor.io;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.apache.commons.csv.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.StreamCallback;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class CsvToVfcFileWriterCallback extends FlowFileWriter implements StreamCallback
{
  private final String[] vfcColumns;
  private final String cloudTable;
  private final CSVFormat format;
  private final boolean skipEmpty;

  public CsvToVfcFileWriterCallback( String cloudTable, CSVFormat format, String[] vfcColumns,
      final ProcessSession session, final FlowFile flowFile, boolean skipEmpty )
  {
    super( cloudTable, -1, session, flowFile );
    this.vfcColumns = vfcColumns;
    this.cloudTable = cloudTable;
    this.format = format;
    this.skipEmpty = skipEmpty;
  }

  @Override
  public void process( InputStream in, OutputStream out ) throws IOException
  {
    BufferedReader reader = new BufferedReader(
        new InputStreamReader( in, StandardCharsets.UTF_8 ) );

    try (CSVParser csv = format.parse( reader ))
    {
      super.open( out );
      for( CSVRecord row : csv )
      {
        CloudTableRow record = getVfcRecord( row, vfcColumns, skipEmpty );
        if( record != null )
        {
          super.write( record );
        }
      }
    }
    finally
    {
      super.close( );
    }
  }

  private CloudTableRow getVfcRecord( CSVRecord record, String[] vfcPaths, boolean skipEmptyValues )
  {
    if( record != null )
    {
      CloudTableRow row = new CloudTableRow( );
      row.setCategory( cloudTable );

      for( int i = 0; i < vfcPaths.length; i++ )
      {
        String columnKey = vfcPaths[i];
        if( columnKey != null )
        {
          String value = record.get( i );
          if( value == null )
          {
            value = "";
          }

          if( columnKey.startsWith( CloudTableRow.COLUMN_TYPE_PROPERTIES + VfcReadProcessor.DOT ) )
          {
            if( !( skipEmptyValues && StringUtils.isBlank( value ) ) )
            {
              row.addProperty(
                  columnKey.substring(
                      ( CloudTableRow.COLUMN_TYPE_PROPERTIES + VfcReadProcessor.DOT ).length( ) ),
                  value );
            }
          }
          else if( columnKey.startsWith( CloudTableRow.COLUMN_TYPE_KEYS + VfcReadProcessor.DOT
              + CloudTableRow.KEY_COLUMN_PREFIX ) )
          {
            String keyNumStr = columnKey.substring( ( CloudTableRow.COLUMN_TYPE_KEYS
                + VfcReadProcessor.DOT + CloudTableRow.KEY_COLUMN_PREFIX ).length( ) );
            if( NumberUtils.isDigits( keyNumStr ) )
            {
              int keyNo = NumberUtils.createInteger( keyNumStr ).intValue( ) - 1;
              if( keyNo >= 0 )
              {
                String[] keys = row.getKeys( );
                if( keys == null )
                {
                  keys = new String[keyNo + 1];
                }
                else if( keys.length <= keyNo )
                {
                  String[] oldKeys = keys;
                  keys = new String[keyNo + 1];
                  System.arraycopy( oldKeys, 0, keys, 0, oldKeys.length );
                }
                keys[keyNo] = value;
                row.setKeys( keys );
              }
            }
          }
          else if( columnKey.equals( CloudTableRow.SYSTEM_CATEGORY )
              && StringUtils.isBlank( cloudTable ) )
          {
            row.setCategory( value );
          }
          else if( columnKey.equals( CloudTableRow.SYSTEM_DESCRIPTION ) )
          {
            row.setDescription( value );
          }
          else
          {
            // No need to put as it is not read by VFC
          }
        }
      }
      return row;
    }
    return null;
  }
}
