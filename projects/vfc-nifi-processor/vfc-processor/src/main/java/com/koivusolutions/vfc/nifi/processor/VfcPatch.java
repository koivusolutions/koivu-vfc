package com.koivusolutions.vfc.nifi.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;

import com.koivusolutions.vfc.client.cloudtable.*;

@Tags({"patch", "write", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("PATCHes VFC data collections. i.e Merges new data with existing data. ")
public class VfcPatch extends VfcWrite
{

  public VfcPatch( )
  {
  }

  @Override
  protected CloudTableUpdater<CloudTableRow> getUpdater( CloudTables cloudTables )
  {
    return cloudTables.patchCloudTable( );
  }

  @Override
  protected List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_SKIP_IDENTICAL );
    return props;
  }

}
