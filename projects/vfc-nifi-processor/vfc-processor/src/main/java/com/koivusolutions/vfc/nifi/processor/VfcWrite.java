package com.koivusolutions.vfc.nifi.processor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;
import com.koivusolutions.vfc.client.cloudtable.*;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.io.VfcWriteResponseWriter;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.services.VfcService;

@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
abstract class VfcWrite extends AbstractProcessor implements VfcWriteProcessor
{
  private final static String TRUE = "true";
  private final static String FALSE = "false";

  protected static final PropertyDescriptor PROPERTY_VFC_SERVICE = new PropertyDescriptor.Builder( )
      .name( "Value Flow Cloud Service" ) //
      .displayName( "Koivu.Cloud Service" ) //
      .description( "Koivu.Cloud Service to define Koivu.Cloud instance connection info" ) //
      .required( true ) //
      .addValidator( Validator.VALID ) //
      .identifiesControllerService( VfcService.class ).build( );

  protected static final PropertyDescriptor PROPERTY_SKIP_IDENTICAL = new PropertyDescriptor.Builder( )
      .name( "Skip Identical" ) //
      .displayName( "Skip Identical Data Versions" ) //
      .description(
          "Do not add a new data version, if new data is identical to the latest version in the database" ) //
      .required( false ) //
      .allowableValues( //
          new AllowableValue( TRUE, "Skip identical versions" ), //
          new AllowableValue( FALSE, "Always add version" ) ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public VfcWrite( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_VFC_SERVICE );

    List<PropertyDescriptor> processorProps = initProperties( );
    if( processorProps != null )
    {
      props.addAll( processorProps );
    }
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_DATA_NOT_FOUND );
    rels.add( DATA_REJECTED );
    rels.add( REL_FAILURE );
    rels.add( REL_ORIGINAL );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  protected List<PropertyDescriptor> initProperties( )
  {
    return null;
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile originalFlowFile = session.get( );
    if( originalFlowFile == null )
    {
      return;
    }

    FlowFile flowFile = originalFlowFile;
    try
    {
      VfcService vfcService = context.getProperty( PROPERTY_VFC_SERVICE )
          .asControllerService( VfcService.class );
      VfcClient vfcClient = vfcService.getVfcClient( flowFile );

      final CloudTableUpdater<CloudTableRow> updater = getUpdater( vfcClient.getCloudTables( ) );

      String skip = context.getProperty( PROPERTY_SKIP_IDENTICAL ).evaluateAttributeExpressions( )
          .getValue( );
      if( TRUE.equals( skip ) )
      {
        updater.setSkipIdentical( true );
      }

      final VfcWriteResponseWriter success = new VfcWriteResponseWriter( session, flowFile );
      final VfcWriteResponseWriter fail = new VfcWriteResponseWriter( session, flowFile );

      VfcIterator<VfcRowUpdateStatus<CloudTableRow>> results;
      try (BufferedReader reader = new BufferedReader(
          new InputStreamReader( session.read( flowFile ), StandardCharsets.UTF_8 ) ))
      {
        String line;
        while( ( line = reader.readLine( ) ) != null )
        {
          if( line == null || line.length( ) == 0 || line.charAt( 0 ) == '['
              || line.charAt( 0 ) == ']' )
          {
            continue;
          }
          JSONObject json = new JSONObject( line );
          json.remove( "writeResponse" );
          CloudTableRow row = new CloudTableRow( json );
          updater.addUpdate( row );

          results = updater.getResponses( );
          processResponses( results, session, success, fail );
        }
        results = updater.getAllResponses( );
        processResponses( results, session, success, fail );
        String transitUri = getTransitURI( results );
        session.getProvenanceReporter( ).send( flowFile, transitUri );
      }
      FlowFile successFlowFile = success.commit( REL_SUCCESS );
      FlowFile failFlowFile = fail.commit( DATA_REJECTED );

      if( successFlowFile == null && failFlowFile == null )
      {
        if( results != null )
        {
          getLogger( ).debug( "Data not found; {}",
              new Object[]{results.getResponseStatus( ).toString( )} );
          setVariables( session, flowFile, results.getResponseStatus( ) );
        }
        session.getProvenanceReporter( ).route( flowFile, REL_DATA_NOT_FOUND );
        session.transfer( flowFile, REL_DATA_NOT_FOUND );
      }
      else
      {
        getLogger( ).info( "{} processed ok", new Object[]{results.getResponseStatus( )} );
        session.getProvenanceReporter( ).route( flowFile, REL_ORIGINAL );
        session.transfer( flowFile, REL_ORIGINAL );
      }

    }
    catch( final FlowFileAccessException | IOException | NotInitialized t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      throw new ProcessException( t );
    }
    catch( final VfcClientException t )
    {
      getLogger( ).warn( "VFC status: {}, {} failed to process due to {}",
          new Object[]{t.getResponseStatus( ).toString( ), this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      setVariables( session, flowFile, t.getResponseStatus( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
  }

  private void processResponses( VfcIterator<VfcRowUpdateStatus<CloudTableRow>> results,
      ProcessSession session, VfcWriteResponseWriter success, VfcWriteResponseWriter fail )
      throws NoSuchElementException,
      VfcClientException,
      IOException
  {
    String firstMessage = null;
    int failCount = 0;
    while( results.hasNext( ) )
    {
      VfcRowUpdateStatus<CloudTableRow> result = results.next( );
      if( result.isError( ) )
      {
        fail.write( result, results.getResponseStatus( ) );
        failCount++;
        if( firstMessage == null )
        {
          firstMessage = result.getResponseMessage( );
        }
      }
      else
      {
        success.write( result, results.getResponseStatus( ) );
      }
    }
    if( failCount > 0 )
    {
      fail.setErrorMessage( String.format( "%d failed rows. First error message:'%s'",
          Integer.valueOf( failCount ), firstMessage ) );
    }
  }

  protected String getTransitURI( VfcIterator<VfcRowUpdateStatus<CloudTableRow>> results )
      throws VfcClientException
  {
    String transitUri = results.getResponseStatus( ) != null
        ? ( ( results.getResponseStatus( ).getMeta( ) != null )
            ? results.getResponseStatus( ).getMeta( ).get( "request" )
            : results.getResponseStatus( ).toString( ) )
        : "";
    return transitUri;
  }

  abstract protected CloudTableUpdater<CloudTableRow> getUpdater( CloudTables cloudTables );

  private void setVariables( ProcessSession session, FlowFile flowFile,
      VfcResponseStatus responseStatus )
  {
    VfcReadProcessor.setVariables( session, flowFile, responseStatus );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
