package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.processor.Relationship;

public interface VfcWriteProcessor extends VfcReadProcessor
{
  final static Relationship REL_ORIGINAL = new Relationship.Builder( ).name( "original" )
      .description( "Original input passthru" ).build( );

  final static Relationship DATA_REJECTED = new Relationship.Builder( ).name( "rejected" )
      .description( "Operation not entirely successfull. Contains rejected data." ).build( );

}
