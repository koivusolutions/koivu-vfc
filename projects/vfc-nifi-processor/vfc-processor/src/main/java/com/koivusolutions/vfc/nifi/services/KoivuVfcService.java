package com.koivusolutions.vfc.nifi.services;

import static com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AUTH_TYPE;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.nifi.annotation.lifecycle.OnDisabled;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.reporting.InitializationException;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AuthType;
import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.logging.Logger;
import com.koivusolutions.vfc.nifi.logging.Logging;
import com.koivusolutions.vfc.nifi.validators.PositiveNumberValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

public abstract class KoivuVfcService extends AbstractControllerService implements VfcService
{
  // COMPRESSION
  private static final PropertyDescriptor PROPERTY_COMPRESS_LIMIT = new PropertyDescriptor.Builder( )
      .name( "http.gzip.limit" ) //
      .displayName( "Compression Limit" ) //
      .description( "Request compression limit in bytes when sending data" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( false ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "1024" ).build( );

  // PROXY
  private static final PropertyDescriptor PROPERTY_PROXY_SCHEME = new PropertyDescriptor.Builder( )
      .name( "http.proxy.scheme" ) //
      .displayName( "Proxy Server Scheme" ) //
      .description( "Proxy server scheme" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( false ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .build( );
  private static final PropertyDescriptor PROPERTY_PROXY_PORT = new PropertyDescriptor.Builder( )
      .name( "http.proxy.port" ) //
      .displayName( "Proxy Server Port" ) //
      .description( "Proxy server port number" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( false ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .build( );
  private static final PropertyDescriptor PROPERTY_PROXY_HOST = new PropertyDescriptor.Builder( )
      .name( "http.proxy.host" ) //
      .displayName( "Proxy Server" ) //
      .description( "Proxy server host name" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( false ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .build( );

  // HTTP
  private static final PropertyDescriptor PROPERTY_REQUEST_TIMEOUT = new PropertyDescriptor.Builder( )
      .name( "http.connection.connectionrequesttimeout" ) //
      .displayName( "Request Timeout" ) //
      .description( "VFC client request timeout in milliseconds" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "15000" ).build( );

  private static final PropertyDescriptor PROPERTY_CONNECTION_TIMEOUT = new PropertyDescriptor.Builder( )
      .name( "http.connection.connectiontimeout" ) //
      .displayName( "Connection Timeout" ) //
      .description( "VFC client connection timeout in milliseconds" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "15000" ).build( );

  private static final PropertyDescriptor PROPERTY_SOCKET_TIMEOUT = new PropertyDescriptor.Builder( )
      .name( "http.connection.sockettimeout" ) //
      .displayName( "Socket Timeout" ) //
      .description( "VFC client socket timeout in milliseconds" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "120000" ).build( );

  private static final PropertyDescriptor PROPERTY_CONNECTION_POOL_SIZE = new PropertyDescriptor.Builder( )
      .name( "http.connection.poolsize" ) //
      .displayName( "Connection Pool Size" ) //
      .description( "VFC client connection pool size. Denotes max concurrent requests also." ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "4" ).build( );

  // VFC
  private static final PropertyDescriptor PROPERTY_BATCH_SIZE = new PropertyDescriptor.Builder( )
      .name( "vfc.api.batchsize" ) //
      .displayName( "Batch size" ) //
      .description(
          "Koivu.Cloud API data read request batch size when retrieving data from VFC server. For max efficiency use value of ( N * 2048 - 1 )." ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( false ).sensitive( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .defaultValue( "20479" ).build( );

  private static final PropertyDescriptor PROPERTY_BRANCH = new PropertyDescriptor.Builder( )
      .name( "vfc.api.branch" ) //
      .displayName( "Branch" ) //
      .description( "Koivu.Cloud API branch in a solution" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( false ).sensitive( false ).build( );

  private static final PropertyDescriptor PROPERTY_VFC_TENANT = new PropertyDescriptor.Builder( )
      .name( "vfc.api.tenant" ) //
      .displayName( "Tenant" ).description( "Koivu.Cloud API organization id" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( false ).sensitive( false ).build( );

  private static final PropertyDescriptor PROPERTY_SOLUTION = new PropertyDescriptor.Builder( )
      .name( "vfc.api.solution" ) //
      .displayName( "Solution" ) //
      .description( "Koivu.Cloud API solution for accessing data" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( false ).build( );

  private static final PropertyDescriptor PROPERTY_API_VERSION = new PropertyDescriptor.Builder( )
      .name( "vfc.api.version" ) //
      .displayName( "Version" ) //
      .description( "Koivu.Cloud API version" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .defaultValue( "valueflow/v1/" ).build( );

  private static final PropertyDescriptor PROPERTY_CLIENT_ID = new PropertyDescriptor.Builder( )
      .name( "vfc.client.id" ) //
      .displayName( "HTTP Client ID" ) //
      .description(
          "HTTP Client Id visible in HTTP traffic on the VFC server. Change this to any string which identifies this service on the VFC server side as request source." ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .defaultValue( "Koivu Value Flow Client - Nifi" ).build( );

  // Authentication
  protected static final PropertyDescriptor PROPERTY_VFC_URL = new PropertyDescriptor.Builder( )
      .name( "vfc" ) //
      .displayName( "Koivu.Cloud URL" ) //
      .description( "VFC Host URL. For example: https://myserver.com:8080" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( false ).build( );

  protected static final PropertyDescriptor PROPERTY_VFC_CLIENT_ID = new PropertyDescriptor.Builder( )
      .name( "clientid" ) //
      .displayName( "Client ID" ) //
      .description( "VFC client ID in Auth0 " ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( true ).build( );

  protected static final PropertyDescriptor PROPERTY_VFC_CLIENT_SECRET = new PropertyDescriptor.Builder( )
      .name( "clientsecret" ) //
      .displayName( "Client Secret" ) //
      .description( "VFC client secret in Auth0" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( true ).build( );

  protected static void addPropertyDescriptors( List<PropertyDescriptor> props )
  {
    // Authentication
    props.add( PROPERTY_VFC_URL );

    // VFC
    props.add( PROPERTY_CLIENT_ID );
    props.add( PROPERTY_API_VERSION );
    props.add( PROPERTY_VFC_TENANT );
    props.add( PROPERTY_SOLUTION );
    props.add( PROPERTY_BRANCH );
    props.add( PROPERTY_BATCH_SIZE );

    // HTTP
    props.add( PROPERTY_CONNECTION_POOL_SIZE );
    props.add( PROPERTY_SOCKET_TIMEOUT );
    props.add( PROPERTY_CONNECTION_TIMEOUT );
    props.add( PROPERTY_REQUEST_TIMEOUT );

    // Proxy
    props.add( PROPERTY_PROXY_HOST );
    props.add( PROPERTY_PROXY_PORT );
    props.add( PROPERTY_PROXY_SCHEME );

    // Compression
    props.add( PROPERTY_COMPRESS_LIMIT );
  }

  protected Logging logger;
  private VfcClient vfc = null;

  public KoivuVfcService( )
  {
  }

  protected abstract AuthType getAuthenticationType( );

  @OnEnabled
  public void enabled( final ConfigurationContext context ) throws InitializationException
  {
    enableLogger( );

    Map<String, String> allProperties = context.getAllProperties( );

    logger.logInfo( "Enabling " + this.getClass( ).getSimpleName( ) + ": "
        + allProperties.get( PROPERTY_VFC_URL.getName( ) ) );

    try
    {
      logger.logInfo( "-- Starting VFC client" );
      VfcProperties vfcProperties = new VfcProperties( );
      for( PropertyDescriptor key : context.getProperties( ).keySet( ) )
      {
        vfcProperties.put( key.getName( ), context.getProperty( key ).getValue( ) == null ? ""
            : context.getProperty( key ).getValue( ) );
      }
      vfcProperties.put( AUTH_TYPE, getAuthenticationType( ).toString( ) );

      String strVfcURL = context.getProperty( PROPERTY_VFC_URL ).getValue( );
      String vfcClientId = context.getProperty( PROPERTY_VFC_CLIENT_ID ).getValue( );
      String vfcClientSecret = context.getProperty( PROPERTY_VFC_CLIENT_SECRET ).getValue( );

      URL vfcURL = new URL( strVfcURL );
      logger.logInfo( "  VFC URL:" + vfcURL );
      vfc = new VfcClient( vfcURL, vfcProperties, vfcClientId, vfcClientSecret );
      logger.logInfo( "-- Successfully started VFC client" );
    }
    catch( VfcClientException | IOException vfcStartError )
    {
      StringBuilder error = new StringBuilder( );
      Throwable t = vfcStartError;
      while( t != null )
      {
        error.append( t.getClass( ).getSimpleName( ) ).append( ":'" );
        error.append( t.getMessage( ) ).append( "'. " );
        t = t.getCause( );
      }
      logger.logError( error.toString( ) );
      throw new InitializationException( vfcStartError );
    }
  }

  @OnDisabled
  public void disabled( final ConfigurationContext context )
  {
    enableLogger( );

    logger.logInfo( "Disabling " + this.getClass( ).getSimpleName( ) + ": "
        + context.getAllProperties( ).get( PROPERTY_VFC_URL.getName( ) ) );

    logger.logInfo( "-- Stopping VFC client" );
    if( vfc != null )
    {
      vfc.close( );
      vfc = null;
    }
    logger.logInfo( "-- Successfully stopped VFC client" );
  }

  protected void enableLogger( )
  {
    if( logger == null )
    {
      logger = new Logging( getLogger( ) );
      Logger.setExternalLogger( logger );
    }
  }

  @Override
  public VfcClient getVfcClient( FlowFile flowfile ) throws NotInitialized
  {
    if( vfc == null )
    {
      throw new NotInitialized( this.getClass( ).getSimpleName( ) + " is not active." );
    }
    return vfc;
  }

}
