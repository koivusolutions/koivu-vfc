package com.koivusolutions.vfc.nifi.processor.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.StreamCallback;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class VfcCloudTableFlowfileAppenderCallback extends FlowFileWriter implements StreamCallback
{
  private final VfcIterator<CloudTableRow> cursor;
  private final boolean append;

  public VfcCloudTableFlowfileAppenderCallback( CloudTable t, VfcIterator<CloudTableRow> cursor,
      boolean append, final ProcessSession session, final FlowFile flowFile )
  {
    super( t.getCategory( ), -1, session, flowFile );
    this.cursor = cursor;
    this.append = append;
  }

  public VfcCloudTableFlowfileAppenderCallback( String category, VfcIterator<CloudTableRow> cursor,
      boolean append, final ProcessSession session, final FlowFile flowFile )
  {
    super( category, -1, session, flowFile );
    this.cursor = cursor;
    this.append = append;
  }

  @Override
  public void process( InputStream in, OutputStream out ) throws IOException
  {
    try
    {
      BufferedReader reader = new BufferedReader(
          new InputStreamReader( in, StandardCharsets.UTF_8 ) );

      super.open( out );

      String line;
      while( ( line = reader.readLine( ) ) != null )
      {
        if( line != null && line.length( ) > 0 )
        {
          if( line.charAt( 0 ) == '[' || line.charAt( 0 ) == ']' )
          {
            continue;
          }
          if( append )
          {
            JSONObject json = new JSONObject( line );
            CloudTableRow existingRow = new CloudTableRow( json );
            super.write( existingRow );
          }
        }
      }

      while( cursor.hasNext( ) )
      {
        CloudTableRow currentRow = cursor.next( );
        super.write( currentRow );
      }
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
    catch( VfcClientException e )
    {
      VfcReadProcessor.setVariables( session, flowFile, e.getResponseStatus( ) );
      throw new IOException( e );
    }
    finally
    {
      super.close( );
    }
  }

}
