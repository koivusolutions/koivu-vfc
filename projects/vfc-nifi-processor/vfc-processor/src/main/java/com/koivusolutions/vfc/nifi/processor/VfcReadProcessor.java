package com.koivusolutions.vfc.nifi.processor;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;

import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.io.VfcCloudTableFileWriterCallback;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;

public interface VfcReadProcessor
{
  static final String DOT = ".";

  static final String CSV_HEADER_NO = "NO";
  static final String CSV_HEADER_YES = "YES";

  static final String ATTRIBUTE_ERROR = "error";

  static final String ATTRIBUTE_RESPONSE_COLUMNS = "vfc.response.columns";
  static final String ATTRIBUTE_ROW_COUNT = "vfc.row.count";

  final static Relationship REL_SUCCESS = new Relationship.Builder( ).name( "success" )
      .description( "Operation successfull" ).build( );

  final static Relationship REL_DATA_NOT_FOUND = new Relationship.Builder( ).name( "not found" )
      .description( "Operation successfull but no data found" ).build( );

  final static Relationship REL_FAILURE = new Relationship.Builder( ).name( "failure" )
      .description( "Operation failed" ).build( );

  static String writeFlowFile( CloudTable t, VfcIterator<CloudTableRow> cursor, int splitSize,
      final ProcessSession session, final FlowFile flowFile )
  {
    VfcCloudTableFileWriterCallback flowFileWriter = new VfcCloudTableFileWriterCallback( t, cursor,
        splitSize, session, flowFile );
    session.write( flowFile, flowFileWriter );
    setSessionAttribute( session, flowFile, ATTRIBUTE_ROW_COUNT,
        String.valueOf( flowFileWriter.getCount( ) ) );
    session.adjustCounter( "VFC rows read", flowFileWriter.getCount( ), false );
    setSessionAttribute( session, flowFile, ATTRIBUTE_RESPONSE_COLUMNS,
        flowFileWriter.getFields( ) );
    return flowFileWriter.getLastDataTimeStamp( );
  }

  public static final PropertyDescriptor PROPERTY_CSV_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "csv.separator" ) //
      .displayName( "CSV field separator" ) //
      .defaultValue( "," ) //
      .required( true ) //
      .description( "The separator character used in CSV output." ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  public static final PropertyDescriptor PROPERTY_CSV_HEADER = new PropertyDescriptor.Builder( )
      .name( "csv.header" ) //
      .displayName( "CSV Header" ) //
      .allowableValues( //
          new AllowableValue( CSV_HEADER_YES, "Contains header line", "CSV have a header line" ), //
          new AllowableValue( CSV_HEADER_NO, "No header line", "CSV does not have header line" ) ) //
      .defaultValue( CSV_HEADER_YES ) //
      .required( true ) //
      .description( "Define if CSV data contains header line." ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  static String[] getColumns( FlowFile flowFile )
  {
    String fields = flowFile.getAttribute( ATTRIBUTE_RESPONSE_COLUMNS );
    if( StringUtils.isNotEmpty( fields ) )
    {
      return fields.split( "\\|" );
    }
    return new String[0];
  }

  static void setBatchNumberVariable( ProcessSession session, FlowFile flowFile, int batchNumber )
  {
    flowFile = setSessionAttribute( session, flowFile, "vfc.response.batch.no",
        String.valueOf( batchNumber ) );
  }

  static void setVariables( ProcessSession session, FlowFile flowFile,
      VfcResponseStatus responseStatus )
  {
    if( responseStatus != null )
    {
      Map<String, String> response = responseStatus.getResponse( );
      flowFile = setSessionAttribute( session, flowFile, "vfc.response.status",
          response.get( "status" ) );
      flowFile = setSessionAttribute( session, flowFile, "vfc.response.httpStatus",
          response.get( "httpStatus" ) );

      Map<String, String> error = responseStatus.getError( );
      flowFile = setSessionAttribute( session, flowFile, "vfc.error.errorMessage",
          error.get( "errorMessage" ) );
      flowFile = setSessionAttribute( session, flowFile, "vfc.error.errorCode",
          error.get( "errorCode" ) );
      flowFile = setSessionAttribute( session, flowFile, "vfc.error.stackTrace",
          error.get( "stackTrace" ) );

      flowFile = setSessionAttribute( session, flowFile, "vfc.meta",
          responseStatus.getMetaAsJson( ).toString( ) );

      Map<String, String> pagination = responseStatus.getPagination( );
      flowFile = setSessionAttribute( session, flowFile, "vfc.pagination.responseCount",
          pagination.get( "responseCount" ) );
      flowFile = setSessionAttribute( session, flowFile, "vfc.pagination.hasMoreData",
          pagination.get( "hasMoreData" ) );
      flowFile = setSessionAttribute( session, flowFile, "vfc.pagination.continueToken",
          pagination.get( "continueToken" ) );
    }
  }

  static FlowFile setSessionAttribute( final ProcessSession session, final FlowFile flowFile,
      String name, String value )
  {
    return session.putAttribute( flowFile, name, value != null ? value : "" );
  }

}
