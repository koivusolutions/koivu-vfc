package com.koivusolutions.vfc.nifi.processor.vfc;

import java.io.IOException;
import java.util.List;

import org.apache.nifi.components.*;
import org.apache.nifi.context.PropertyContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessContext;

import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.services.VfcService;
import com.koivusolutions.vfc.nifi.validators.PositiveNumberValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

public abstract class VfcReaderWrapper
{
  private final static String TRUE = "true";
  private final static String FALSE = "false";

  protected static final PropertyDescriptor PROPERTY_VFC_SERVICE = new PropertyDescriptor.Builder( )
      .name( "Value Flow Cloud Service" ) //
      .displayName( "Koivu.Cloud Service" ) //
      .description( "Koivu.Cloud Service to define Koivu.Cloud instance connection info" ) //
      .required( true ) //
      .addValidator( Validator.VALID ) //
      .identifiesControllerService( VfcService.class ).build( );

  protected static final PropertyDescriptor PROPERTY_CLOUD_TABLE = new PropertyDescriptor.Builder( )
      .name( "Cloud Table Name" ) //
      .displayName( "Cloud Table Name" ) //
      .description( "Name of the Cloud Table" ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_EXPRESSION = new PropertyDescriptor.Builder( )
      .name( "Expressions" ) //
      .displayName( "Expressions" ) //
      .description(
          "Read Expression for VFC data query for providing dynamic read time evaluated data columns." ) //
      .required( false ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_FILTER = new PropertyDescriptor.Builder( )
      .name( "Filter" ) //
      .displayName( "Filter" ) //
      .description( "Filter Expression for VFC data query" ) //
      .required( false ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_FILTER_BY_LEVELS = new PropertyDescriptor.Builder( )
      .name( "FilterByLevels" ) //
      .displayName( "Filter by Levels" ) //
      .description( "Use levels in filter expression when filtering" ) //
      .required( false ) //
      .allowableValues( //
          new AllowableValue( TRUE, "Normal, filter also sub levels if parent is filtered" ), //
          new AllowableValue( FALSE, "Filter without using level information" ) ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  public static final PropertyDescriptor PROPERTY_NEWER_THAN = new PropertyDescriptor.Builder( )
      .name( "CatchUpHours" ) //
      .displayName( "Catch Up Hours" ) //
      .description(
          "If defined, search only for newer rows from the history during the first query" ) //
      .required( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  public static final PropertyDescriptor PROPERTY_SPLIT_SIZE = new PropertyDescriptor.Builder( )
      .name( "Result Split Size" ) //
      .displayName( "Result Split Size" ) //
      .description(
          "Splits VFC data to own flow files containing max split size of rows, if response contains more rows than split size. For max efficiency align this value with VFC Connection service Batch size." ) //
      .required( false ) //
      .addValidator( new PositiveNumberValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  public static final PropertyDescriptor PROPERTY_INCLUDE_DELETED = new PropertyDescriptor.Builder( )
      .name( "IncludeDeleted" ) //
      .displayName( "Include deleted rows" ) //
      .description( "If defined, search also for rows which are deleted" ) //
      .required( false ) //
      .allowableValues( //
          new AllowableValue( TRUE, "Return also deleted rows" ), //
          new AllowableValue( FALSE, "Normal, do not return deleted rows" ) ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_IMPERSONATE_BUSINESS = new PropertyDescriptor.Builder( )
      .name( "ImpersonateBusiness" ) //
      .displayName( "Impersonate to business" ) //
      .description(
          "Request is made by impersonating signed in user to be assigned another business" ) //
      .required( false ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  public VfcReaderWrapper( )
  {
  }

  public abstract List<PropertyDescriptor> initProperties( );

  public abstract CloudTable getCloudTable( final ProcessContext context, FlowFile flowFile,
      String newerThanString ) throws IOException,
      NotInitialized;

  protected String[] processKeys( String[] keys )
  {
    String[] ret = new String[keys.length];
    for( int i = 0; i < keys.length; i++ )
    {
      ret[i] = keys[i].trim( );
    }
    return ret;
  }

  protected String getPropertyValue( final PropertyContext context, final FlowFile flowFile,
      PropertyDescriptor propertyName )
  {
    return flowFile == null
        ? context.getProperty( propertyName ).evaluateAttributeExpressions( ).getValue( )
        : context.getProperty( propertyName ).evaluateAttributeExpressions( flowFile ).getValue( );
  }

}
