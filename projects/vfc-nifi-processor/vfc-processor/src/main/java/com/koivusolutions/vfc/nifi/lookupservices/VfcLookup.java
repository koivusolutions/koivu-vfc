package com.koivusolutions.vfc.nifi.lookupservices;

import java.util.*;

import org.apache.nifi.lookup.LookupFailureException;
import org.apache.nifi.lookup.LookupService;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public interface VfcLookup extends LookupService<String>
{

  String getRowValue( CloudTableRow cloudTableRow, String key );

  List<String[]> getKeys( ) throws LookupFailureException;

  List<String> getAll( String coordinate ) throws LookupFailureException;

  Optional<String> lookup( Map<String, Object> coordinates, String retExpression,
      String defaultValue ) throws LookupFailureException;

}