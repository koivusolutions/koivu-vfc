package com.koivusolutions.vfc.nifi.processor;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.commons.csv.*;
import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.processor.io.CsvToVfcFileWriterCallback;
import com.koivusolutions.vfc.nifi.validators.*;

@Tags({"json", "csv", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Converts flow file containing CSV format to VFC JSON format")
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@DynamicProperty(name = "The VFC path name for the VFC record.", value = "A CSV column name that will be evaluated against each CSV record. The result of the CSV column will be the value of the "
    + "VFC field whose name is the same as the property name.", description = "User-defined properties identify how to extract specific fields from a CSV record in order to create a VFC Record", expressionLanguageScope = ExpressionLanguageScope.NONE)
public class CsvToVfcJson extends AbstractProcessor implements VfcReadProcessor
{
  private static final String OUTPUT_NAMED = "named";
  private static final String OUTPUT_ALL = "all";
  private static final String OUTPUT_ALL_PARTIALLY_NAME = "all_partially_named";
  private static final String SKIP_EMPTY_NO = "ALL_VALUES";
  private static final String SKIP_EMPTY_YES = "NON_EMPTY_VALUES";

  static final PropertyDescriptor PROPERTY_KEY_EXPANDING_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "header.separator" ) //
      .displayName( "Key Separator" ) //
      .defaultValue( "." ) //
      .required( true ) //
      .description( "The separator character used for expanding json field names." ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  static final PropertyDescriptor PROPERTY_OUTPUT = new PropertyDescriptor.Builder( )
      .name( "fields.output" ) //
      .displayName( "Output Fields" ) //
      .allowableValues( //
          new AllowableValue( OUTPUT_ALL, "All fields", "Output all fields" ), //
          new AllowableValue( OUTPUT_ALL_PARTIALLY_NAME, "All fields, partially named",
              "Output all fields, use name mapping, if present" ), //
          new AllowableValue( OUTPUT_NAMED, "Named fields", "Output ONLY named fields" ) ) //
      .defaultValue( OUTPUT_ALL ) //
      .required( true ) //
      .description( "Define fields included in the VFC output." ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  protected static final PropertyDescriptor PROPERTY_CLOUD_TABLE = new PropertyDescriptor.Builder( )
      .name( "Cloud Table Name" ) //
      .displayName( "Cloud Table Name" ) //
      .description( "Name of the Cloud Table" ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  public static final PropertyDescriptor SKIP_EMPTY = new PropertyDescriptor.Builder( )
      .name( "skipempty" ) //
      .displayName( "Skip empty fields" ) //
      .allowableValues( //
          new AllowableValue( SKIP_EMPTY_NO, "Include all fields" ), //
          new AllowableValue( SKIP_EMPTY_YES, "Skip fields with empty value" ) ) //
      .defaultValue( SKIP_EMPTY_NO ) //
      .required( true ) //
      .description( "Define if empty values in csv columns are not generating property values." ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public CsvToVfcJson( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_CLOUD_TABLE );
    props.add( PROPERTY_OUTPUT );
    props.add( PROPERTY_CSV_SEPARATOR );
    props.add( PROPERTY_CSV_HEADER );
    props.add( PROPERTY_KEY_EXPANDING_SEPARATOR );
    props.add( SKIP_EMPTY );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_FAILURE );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile flowFile = session.get( );
    if( flowFile == null )
    {
      return;
    }
    try
    {
      final char csvSeparator = context.getProperty( PROPERTY_CSV_SEPARATOR )
          .evaluateAttributeExpressions( flowFile ).getValue( ).charAt( 0 );
      final String csvHeader = context.getProperty( PROPERTY_CSV_HEADER )
          .evaluateAttributeExpressions( flowFile ).getValue( );
      final String cloudTable = context.getProperty( PROPERTY_CLOUD_TABLE )
          .evaluateAttributeExpressions( flowFile ).getValue( );
      final boolean skipEmpty = SKIP_EMPTY_YES.equals(
          context.getProperty( SKIP_EMPTY ).evaluateAttributeExpressions( flowFile ).getValue( ) );

      CSVFormat format;
      if( CSV_HEADER_YES.equals( csvHeader ) )
      {
        format = CSVFormat.DEFAULT.builder( ).setAllowMissingColumnNames( true )
            .setDelimiter( csvSeparator ).setHeader( ).setSkipHeaderRecord( true ).build( );
      }
      else
      {
        format = CSVFormat.DEFAULT.builder( ).setAllowMissingColumnNames( true )
            .setDelimiter( csvSeparator ).build( );
      }

      final String[] activeColumns = getActiveColumns( context, flowFile,
          getColumnNames( session, flowFile, format ) );
      final String[] vfcColumns = getVfcColumns( context, activeColumns );

      CsvToVfcFileWriterCallback csvToVfc = new CsvToVfcFileWriterCallback( cloudTable, format,
          vfcColumns, session, flowFile, skipEmpty );
      session.write( flowFile, csvToVfc );

      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ROW_COUNT,
          String.valueOf( csvToVfc.getCount( ) ) );

      session.getProvenanceReporter( ).modifyContent( flowFile );
      session.transfer( flowFile, REL_SUCCESS );
    }
    catch( final RuntimeException e )
    {
      Throwable t = e.getCause( );
      t = t != null ? t : e;
      getLogger( ).warn( "{} failed to process due to {}", new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
  }

  private String[] getColumnNames( final ProcessSession session, FlowFile flowFile,
      CSVFormat format )
  {
    final List<String> headers = new ArrayList<>( );
    session.read( flowFile, new InputStreamCallback( ) {

      @Override
      public void process( InputStream in ) throws IOException
      {
        BOMInputStream inStream = BOMInputStream.builder( ).setInputStream( in ).setInclude( false )
            .setByteOrderMarks( ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE,
                ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE )
            .get( );
        String charset = inStream.hasBOM( ) ? inStream.getBOMCharsetName( )
            : StandardCharsets.UTF_8.name( );
        BufferedReader reader = new BufferedReader( new InputStreamReader( inStream, charset ) );

        CSVParser parser = format.parse( reader );
        headers.addAll( parser.getHeaderNames( ) );
        if( headers.size( ) == 0 )
        {
          Iterator<CSVRecord> it = parser.iterator( );
          if( it.hasNext( ) )
          {
            int columnCount = it.next( ).size( );
            for( int i = 0; i < columnCount; i++ )
            {
              headers.add( String.valueOf( i + 1 ) );
            }
          }
        }
        parser.close( );
      }
    } );

    final String[] columns = headers.toArray( new String[headers.size( )] );
    return columns;
  }

  private String[] getVfcColumns( final ProcessContext context, final String[] csvColumns )
  {
    final String keySeparator = context.getProperty( PROPERTY_KEY_EXPANDING_SEPARATOR ).getValue( );
    final String output = context.getProperty( PROPERTY_OUTPUT ).getValue( );
    boolean partiallyNamed = OUTPUT_ALL_PARTIALLY_NAME.equals( output );

    Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
    final String[] vfcColumns = new String[csvColumns.length];
    for( int i = 0; i < csvColumns.length; i++ )
    {
      if( csvColumns[i] != null )
      {
        vfcColumns[i] = getColumnName( context, partiallyNamed, props, csvColumns[i],
            csvColumns[i].replaceAll( "\\" + keySeparator, DOT ) );
      }
    }
    return vfcColumns;
  }

  private String getColumnName( final ProcessContext context, boolean partiallyNamed,
      Set<PropertyDescriptor> props, String column, String defaultValue )
  {
    for( PropertyDescriptor propertyDescriptor : props )
    {
      if( propertyDescriptor.isDynamic( ) )
      {
        String value = context.getProperty( propertyDescriptor ).getValue( );
        if( value.equals( column ) )
        {
          return propertyDescriptor.getName( );
        }
      }
    }
    if( partiallyNamed )
    {
      if( !VfcNameValidator.isValidVfcName( defaultValue ) )
      {
        return CloudTableRow.COLUMN_TYPE_PROPERTIES + VfcReadProcessor.DOT + defaultValue;
      }
    }
    return defaultValue;
  }

  private String[] getActiveColumns( ProcessContext context, FlowFile flowFile,
      final String[] allColumns )
  {
    final String output = context.getProperty( PROPERTY_OUTPUT ).getValue( );
    if( OUTPUT_ALL.equals( output ) || OUTPUT_ALL_PARTIALLY_NAME.equals( output ) )
    {
      return allColumns;
    }
    else
    {
      Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
      String[] activeColumns = new String[allColumns.length];
      for( int i = 0; i < allColumns.length; i++ )
      {
        String columnName = allColumns[i];
        for( PropertyDescriptor propertyDescriptor : props )
        {
          if( propertyDescriptor.isDynamic( ) )
          {
            String value = context.getProperty( propertyDescriptor ).getValue( );
            if( value.equals( columnName ) )
            {
              activeColumns[i] = value;
              continue;
            }
          }
        }
      }
      return activeColumns;
    }

  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description(
            "VFC Path that indicates how to retrieve the value from a VFC record for the '"
                + propertyDescriptorName + "' column" )
        .dynamic( true ).required( false ).addValidator( new VfcNameValidator( false ) ).build( );
  }

  @Override
  protected Collection<ValidationResult> customValidate( final ValidationContext validationContext )
  {
    final String output = validationContext.getProperty( PROPERTY_OUTPUT ).getValue( );
    if( OUTPUT_ALL.equals( output ) )
    {
      return Collections.emptyList( );
    }

    boolean pathSpecified = false;
    for( final PropertyDescriptor property : validationContext.getProperties( ).keySet( ) )
    {
      if( property.isDynamic( ) )
      {
        pathSpecified = true;
        break;
      }
    }

    if( pathSpecified )
    {
      return Collections.emptyList( );
    }

    return Collections.singleton( new ValidationResult.Builder( ).subject( "CSV columns" )
        .valid( false ).explanation( "No CSV Columns are defined" ).build( ) );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
