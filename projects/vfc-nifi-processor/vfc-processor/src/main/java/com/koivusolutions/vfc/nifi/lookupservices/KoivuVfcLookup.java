package com.koivusolutions.vfc.nifi.lookupservices;

import java.util.*;
import java.util.Map.Entry;

import org.apache.nifi.annotation.behavior.DynamicProperties;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnDisabled;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.*;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.lookup.LookupFailureException;
import org.apache.nifi.lookup.StringLookupService;
import org.apache.nifi.reporting.InitializationException;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.logging.Logger;
import com.koivusolutions.vfc.nifi.logging.Logging;
import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadExactKeysWrapper;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.validators.NumberValidator;
import com.koivusolutions.vfc.nifi.validators.VfcNameValidator;

@Tags({"lookup", "read", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Use Koivu.Cloud service to look up values from CloudTable. "
    + "Use dynamic variable $ to define return value and default value to use value what is returned when no lookup row is found. "
    + "Default value can be overridden by defining $default search coordinate.")
@DynamicProperties({
    @DynamicProperty(name = "*", value = "*", description = "All dynamic properties are added as query to the Cloud Table"),
    @DynamicProperty(name = "$", value = "*", description = "Return value from found CloudTable row")})
public class KoivuVfcLookup extends AbstractControllerService implements StringLookupService, VfcLookup
{
  private static final String DEFAULT_EXPR = "$default";

  public static final PropertyDescriptor PROPERTY_CACHE_TIME = new PropertyDescriptor.Builder( )
      .name( "LookupCacheTime" ) //
      .displayName( "Caching time in seconds" ) //
      .description(
          "How long read table rows are cached in seconds. 0 mean no caching and -1 forever." ) //
      .required( false ) //
      .defaultValue( "3600" ).addValidator( new NumberValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  public static final PropertyDescriptor PROPERTY_DEFAULT_VALUE = new PropertyDescriptor.Builder( )
      .name( "Default" ) //
      .displayName( "Default value" ) //
      .description( "Default value to return in case no lookup match" ) //
      .required( false ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  private final VfcReadExactKeysWrapper delegate = new VfcReadExactKeysWrapper( false );
  private Logging logger;
  private CloudTable ct = null;
  private List<CloudTableRow> rows = null;
  private long expireTime = -1;
  private long lastRead = -1;
  private String retExpression;
  private String defaultValue = null;

  @Override
  protected List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    List<PropertyDescriptor> props = delegate.initProperties( );
    props.add( PROPERTY_CACHE_TIME );
    props.add( PROPERTY_DEFAULT_VALUE );

    return Collections.unmodifiableList( props );
  }

  @OnEnabled
  public void enabled( final ConfigurationContext context ) throws InitializationException
  {
    logger = new Logging( getLogger( ) );
    Logger.setExternalLogger( logger );

    logger.logInfo( "Enabling lookup" + this.getClass( ).getSimpleName( ) );

    try
    {
      logger.logInfo( "-- Starting VFC Lookup" );
      ct = delegate.getCloudTable( context );

      expireTime = context.getProperty( PROPERTY_CACHE_TIME ).asLong( ).longValue( );
      defaultValue = context.getProperty( PROPERTY_DEFAULT_VALUE ).getValue( );
      retExpression = context.getAllProperties( ).get( "$" );

      logger.logInfo( "-- Successfully started VFC Lookup for table:" + ct.getCategory( ) );
    }
    catch( NotInitialized vfcStartError )
    {
      StringBuilder error = new StringBuilder( );
      Throwable t = vfcStartError;
      while( t != null )
      {
        error.append( t.getClass( ).getSimpleName( ) ).append( ":'" );
        error.append( t.getMessage( ) ).append( "'. " );
        t = t.getCause( );
      }
      logger.logError( error.toString( ) );
      throw new InitializationException( vfcStartError );
    }
  }

  @OnDisabled
  public void disabled( final ConfigurationContext context )
  {
    if( logger == null )
    {
      logger = new Logging( getLogger( ) );
      Logger.setExternalLogger( logger );
    }

    logger.logInfo( "Disabling lookup" + this.getClass( ).getSimpleName( ) );

    logger.logInfo( "-- Stopping VFC Lookup for table:" + ( ct != null ? ct.getCategory( ) : "" ) );
    ct = null;
    rows = null;
    logger.logInfo( "-- Successfully stopped VFC Lookup" );
  }

  @Override
  public Optional<String> lookup( Map<String, Object> coordinates ) throws LookupFailureException
  {
    String defaultValueToUse = coordinates.get( DEFAULT_EXPR ) != null
        ? coordinates.get( DEFAULT_EXPR ).toString( )
        : defaultValue;

    return lookup( coordinates, this.retExpression, defaultValueToUse );
  }

  @Override
  public Optional<String> lookup( Map<String, Object> coordinates, String retExpressionToUse,
      String defaultValueToUse ) throws LookupFailureException
  {
    try
    {
      long now = System.currentTimeMillis( );
      if( rows == null || now > lastRead + expireTime * 1000 )
      {
        lastRead = now;
        rows = readLookupValues( );
      }

      if( rows != null )
      {
        for( CloudTableRow cloudTableRow : rows )
        {
          boolean match = true;
          for( Entry<String, Object> coordinate : coordinates.entrySet( ) )
          {
            String key = coordinate.getKey( );
            if( !DEFAULT_EXPR.equals( key ) )
            {
              String value = coordinate.getValue( ).toString( );
              String propValue = getRowValue( cloudTableRow, key );
              if( !value.equals( propValue ) )
              {
                match = false;
                break;
              }
            }
          }
          if( match )
          {
            String retValue = getRowValue( cloudTableRow, retExpressionToUse );
            return getReturnValue( retValue, defaultValueToUse );
          }
        }
      }

      return getReturnValue( null, defaultValueToUse );

    }
    catch( VfcClientException e )
    {
      throw new LookupFailureException( "VfcLookup error:" + e.getMessage( ), e );
    }

  }

  public Optional<String> getReturnValue( String retValue, String defaulValueToUse )
  {
    retValue = retValue != null ? retValue : defaulValueToUse;
    return Optional.ofNullable( retValue );
  }

  @Override
  public String getRowValue( CloudTableRow cloudTableRow, String key )
  {
    String[] parts = key.split( "\\.", 2 );
    {
      if( parts.length == 1 )
      {
        parts = new String[]{CloudTableRow.COLUMN_TYPE_SYSTEM, parts[0]};
      }
    }

    String propValue = cloudTableRow.getAsString( parts[0], parts[1], null );
    return propValue;
  }

  @Override
  public List<String[]> getKeys( ) throws LookupFailureException
  {
    try
    {
      long now = System.currentTimeMillis( );
      if( rows == null || now > lastRead + expireTime * 1000 )
      {
        lastRead = now;
        rows = readLookupValues( );
      }

      if( rows != null )
      {
        List<String[]> keys = new ArrayList<>( rows.size( ) );
        for( CloudTableRow cloudTableRow : rows )
        {
          keys.add( cloudTableRow.getKeys( ) );
        }
        return keys;
      }
      else
      {
        return new ArrayList<String[]>( 0 );
      }
    }
    catch( VfcClientException e )
    {
      throw new LookupFailureException( "VfcLookup error:" + e.getMessage( ), e );
    }
  }

  @Override
  public List<String> getAll( String coordinate ) throws LookupFailureException
  {
    try
    {
      long now = System.currentTimeMillis( );
      if( rows == null || now > lastRead + expireTime * 1000 )
      {
        lastRead = now;
        rows = readLookupValues( );
      }

      if( rows != null )
      {
        List<String> values = new ArrayList<>( rows.size( ) );
        for( CloudTableRow cloudTableRow : rows )
        {
          values.add( getRowValue( cloudTableRow, coordinate ) );
        }
        return values;
      }
      else
      {
        return new ArrayList<String>( 0 );
      }
    }
    catch( VfcClientException e )
    {
      throw new LookupFailureException( "VfcLookup error:" + e.getMessage( ), e );
    }
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    if( "$".equals( propertyDescriptorName ) )
    {
      return new PropertyDescriptor.Builder( ).name( propertyDescriptorName ).description(
          "The value from a VFC record which is returned from the lookup. Example: keys.key1 or properties.foo" )
          .dynamic( true ).required( false ).addValidator( new VfcNameValidator( true ) ).build( );
    }
    else
    {
      return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
          .description( "The value from a VFC record for the '" + propertyDescriptorName
              + "' column which must match in lookup" )
          .dynamic( true ).required( false ).addValidator( new VfcNameValidator( true ) ).build( );
    }
  }

  @Override
  protected Collection<ValidationResult> customValidate( final ValidationContext validationContext )
  {

    boolean retSpecified = false;
    for( final PropertyDescriptor property : validationContext.getProperties( ).keySet( ) )
    {
      if( property.isDynamic( ) && property.getName( ).equals( "$" ) )
      {
        retSpecified = true;
        break;
      }
    }

    if( retSpecified )
    {
      return Collections.emptyList( );
    }

    return Collections.singleton( new ValidationResult.Builder( ).subject( "Return value" )
        .valid( false )
        .explanation(
            "No return variable defined. Add dynamic variable; named $ and put path expression in value (eg. properties.foo)" )
        .build( ) );
  }

  private List<CloudTableRow> readLookupValues( ) throws NoSuchElementException,
      VfcClientException
  {
    List<CloudTableRow> newRows = new ArrayList<>( );
    ct.reset( );
    VfcIterator<CloudTableRow> cursor = ct.iteratorEx( );
    while( cursor.hasNext( ) )
    {
      CloudTableRow currentRow = cursor.next( );
      newRows.add( currentRow );
    }
    return newRows;
  }

  @Override
  public Set<String> getRequiredKeys( )
  {
    return Collections.emptySet( );
  }

}
