package com.koivusolutions.vfc.nifi.processor;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.state.Scope;
import org.apache.nifi.components.state.StateMap;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.vfc.VfcReaderWrapper;
import com.koivusolutions.vfc.nifi.services.NotInitialized;

@InputRequirement(InputRequirement.Requirement.INPUT_FORBIDDEN)
@Stateful(description = "Stores the newest data timestamp returned by VFC and fetch only newer data in next invoke", scopes = Scope.CLUSTER)
@WritesAttributes({

    @WritesAttribute(attribute = "vfc.response.status", description = "This is the VFC response status. Possible values are OK or ERROR."),
    @WritesAttribute(attribute = "vfc.response.httpStatus", description = "This is the HTTP status code of VFC request."),

    @WritesAttribute(attribute = "vfc.error.errorMessage", description = "Clear text error message."),
    @WritesAttribute(attribute = "vfc.error.errorCode", description = "Error number."),
    @WritesAttribute(attribute = "vfc.error.stackTrace", description = "Error stack trace in VFC server. Only returned from VFC development instances."),

    @WritesAttribute(attribute = "vfc.meta", description = "VFC request meta information in JSON format. Content is request dependant"),

    @WritesAttribute(attribute = "vfc.pagination.responseCount", description = "Row count of returned data"),
    @WritesAttribute(attribute = "vfc.pagination.hasMoreData", description = "Still more data to fetch?"),
    @WritesAttribute(attribute = "vfc.pagination.continueToken", description = "Token to use for next data fetch"),

    @WritesAttribute(attribute = "vfc.response.batch.no", description = "When VFC response is split to multiple flowfiles, tells sequence number of split"),
    @WritesAttribute(attribute = "vfc.cloudTable", description = "Cloud table name"),
    @WritesAttribute(attribute = "vfc.row.count", description = "Count of rows in flowfile"),

    @WritesAttribute(attribute = VfcReadProcessor.ATTRIBUTE_ERROR, description = "Error message in case fo exception")})
abstract class VfcReadIncremental extends AbstractSessionFactoryProcessor
    implements VfcReadProcessor
{
  private static final String LAST_DATA_TIME_STAMP = "LastDataTimeStamp";
  private static final String LAST_READ_TIME_STAMP = "LastReadTimeStamp";

  static SimpleDateFormat dateFormatGmt = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS z" );
  static
  {
    dateFormatGmt.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
  }

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  private final VfcReaderWrapper reader;

  public VfcReadIncremental( VfcReaderWrapper reader )
  {
    super( );
    this.reader = reader;
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> originalProps = reader.initProperties( );
    List<PropertyDescriptor> props = new ArrayList<>( originalProps.size( ) + 2 );
    props.addAll( originalProps );
    props.add( reader.PROPERTY_NEWER_THAN );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_FAILURE );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSessionFactory sessionFactory )
  {
    try
    {
      StateMap state = context.getStateManager( ).getState( Scope.CLUSTER );
      String newerThanTimeStamp = state.get( LAST_DATA_TIME_STAMP );
      if( StringUtils.isBlank( newerThanTimeStamp ) )
      {
        Integer catchUpHours = context.getProperty( reader.PROPERTY_NEWER_THAN )
            .evaluateAttributeExpressions( ).asInteger( );
        if( catchUpHours != null )
        {
          Instant currentStartTime = Instant.now( );
          currentStartTime = currentStartTime.minus( catchUpHours.intValue( ), ChronoUnit.HOURS );
          ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant( currentStartTime, ZoneOffset.UTC );
          newerThanTimeStamp = zonedDateTime.toString( );
          setLastDataTimeStamp( context, newerThanTimeStamp );
        }
      }

      CloudTable t = reader.getCloudTable( context, null, newerThanTimeStamp );

      VfcIterator<CloudTableRow> cursor = t.iteratorEx( );
      if( !cursor.hasNext( ) )
      {
        setLastNotFoundTimeStamp( context, t );
        getLogger( ).debug( "Data not found; {}",
            new Object[]{cursor.getResponseStatus( ).toString( )} );
        context.yield( );
      }
      else
      {
        resetLastNotFoundTimeStamp( context );

        Integer splitSizeInteger = context.getProperty( reader.PROPERTY_SPLIT_SIZE )
            .evaluateAttributeExpressions( ).asInteger( );
        int splitSize = splitSizeInteger != null ? splitSizeInteger.intValue( ) : -1;

        ProcessSession session = sessionFactory.createSession( );
        FlowFile flowFile = null;
        int batch = 1;

        while( cursor.hasNext( ) )
        {
          if( flowFile == null )
          {
            flowFile = session.create( );
            setVariables( session, flowFile, cursor.getResponseStatus( ) );
          }
          VfcReadProcessor.setBatchNumberVariable( session, flowFile, batch++ );
          String lastDataTimeStamp = writeFlowFile( t, cursor, splitSize, session, flowFile );

          getLogger( ).info( "{} processed ok", new Object[]{cursor.getResponseStatus( )} );

          String transitUri = cursor.getResponseStatus( ).getMeta( ) != null
              ? cursor.getResponseStatus( ).getMeta( ).get( "request" )
              : cursor.getResponseStatus( ).toString( );

          session.getProvenanceReporter( ).receive( flowFile, transitUri );
          session.transfer( flowFile, REL_SUCCESS );
          session.commit( );
          setLastDataTimeStamp( context, lastDataTimeStamp );
          flowFile = null;
        }
      }
    }
    catch( final FlowFileAccessException | IOException | NotInitialized t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      throw new ProcessException( t );
    }
    catch( final VfcClientException t )
    {
      getLogger( ).warn( "VFC status: {}, {} failed to process due to {}",
          new Object[]{t.getResponseStatus( ).toString( ), this, t} );

      String transitUri = t.getResponseStatus( ).getMeta( ) != null
          ? t.getResponseStatus( ).getMeta( ).get( "request" )
          : t.getResponseStatus( ).toString( );
      ProcessSession session = sessionFactory.createSession( );
      FlowFile flowFile = session.create( );
      session.getProvenanceReporter( ).create( flowFile, transitUri );
      setVariables( session, flowFile, t.getResponseStatus( ) );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      session.transfer( flowFile, REL_FAILURE );
      session.commit( );
    }
  }

  protected void setLastDataTimeStamp( final ProcessContext context, String newerThanTimeStamp )
      throws IOException
  {
    setStateTimeStamp( context, LAST_DATA_TIME_STAMP, newerThanTimeStamp );
  }

  protected void resetLastNotFoundTimeStamp( final ProcessContext context ) throws IOException
  {
    setStateVariable( context, LAST_READ_TIME_STAMP, null );
  }

  protected void setLastNotFoundTimeStamp( final ProcessContext context, CloudTable table )
      throws IOException
  {
    String lastRead = getStateVariable( context, LAST_READ_TIME_STAMP );
    if( StringUtils.isNotBlank( lastRead ) )
    {
      // Advance data time stamp
      setLastDataTimeStamp( context, lastRead );
    }

    Instant currentStartTime = Instant.now( );
    if( table.getResponseStatus( ) != null && table.getResponseStatus( ).getMeta( ) != null
        && table.getResponseStatus( ).getMeta( ).get( "requestTime" ) != null )
    {
      // Use server time stamp when available
      String serverRequestTime = table.getResponseStatus( ).getMeta( ).get( "requestTime" );
      synchronized ( dateFormatGmt )
      {
        try
        {
          Date requestDateTime = dateFormatGmt.parse( serverRequestTime );
          currentStartTime = requestDateTime.toInstant( );
        }
        catch( ParseException e )
        {
          // Skip
        }
      }
    }

    ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant( currentStartTime, ZoneOffset.UTC );
    String now = zonedDateTime.toString( );
    setStateTimeStamp( context, LAST_READ_TIME_STAMP, now );
  }

  private void setStateTimeStamp( final ProcessContext context, String stateVariable,
      String newerThanTimeStamp ) throws IOException
  {
    String oldNewerThanTimeStamp = getStateVariable( context, stateVariable );
    if( oldNewerThanTimeStamp == null || oldNewerThanTimeStamp.compareTo( newerThanTimeStamp ) < 0 )
    {
      setStateVariable( context, stateVariable, newerThanTimeStamp );
    }
  }

  protected String getStateVariable( final ProcessContext context, String name ) throws IOException
  {
    StateMap state = context.getStateManager( ).getState( Scope.CLUSTER );
    return state.get( name );
  }

  protected void setStateVariable( final ProcessContext context, String name, String value )
      throws IOException
  {
    StateMap state = context.getStateManager( ).getState( Scope.CLUSTER );
    Map<String, String> stateMap = new HashMap<>( );
    stateMap.putAll( state.toMap( ) );
    if( value != null )
    {
      stateMap.put( name, value );
    }
    else
    {
      stateMap.remove( name );
    }
    context.getStateManager( ).setState( stateMap, Scope.CLUSTER );
  }

  private String writeFlowFile( CloudTable t, VfcIterator<CloudTableRow> cursor, int splitSize,
      final ProcessSession session, final FlowFile flowFile )
  {
    return VfcReadProcessor.writeFlowFile( t, cursor, splitSize, session, flowFile );
  }

  protected void setVariables( ProcessSession session, FlowFile flowFile,
      VfcResponseStatus responseStatus )
  {
    VfcReadProcessor.setVariables( session, flowFile, responseStatus );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
