package com.koivusolutions.vfc.nifi.validators;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.nifi.components.ValidationResult;

public class PositiveNumberValidator extends StringValidator
{

  @Override
  protected ValidationResult validate( String subject, String input )
  {
    Integer value = NumberUtils.createInteger( input );
    boolean valid = value != null && value.compareTo( NumberUtils.INTEGER_ZERO ) > 0;
    String message = !valid ? "the value must be a positive integer" : "";

    return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( valid )
        .explanation( message ).build( );
  }

}
