package com.koivusolutions.vfc.nifi.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;

import com.koivusolutions.vfc.client.cloudtable.*;

@Tags({"replace", "post", "write", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("REPLACE VFC data collections. If data does not exist, it is inserted. If data exists already, it is updated.")
public class VfcReplace extends VfcWrite
{

  public VfcReplace( )
  {
  }

  @Override
  protected CloudTableUpdater<CloudTableRow> getUpdater( CloudTables cloudTables )
  {
    return cloudTables.replaceInCloudTable( );
  }

  @Override
  protected List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_SKIP_IDENTICAL );
    return props;
  }

}
