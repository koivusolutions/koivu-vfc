package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadKeysWrapper;

@Tags({"get", "read", "full query", "keys", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Streams VFC data collections search by keys.")
public class VfcReadKeys extends VfcRead
{

  public VfcReadKeys( )
  {
    super( new VfcReadKeysWrapper( ) );
  }

}
