package com.koivusolutions.vfc.nifi.services;

import static com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AUTH_TYPE;

import java.io.IOException;
import java.net.URL;
import java.util.*;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnDisabled;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.reporting.InitializationException;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.authentication.Authentication;
import com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AuthType;
import com.koivusolutions.vfc.authentication.PreAuthenticated;
import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcServerConnection;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@Tags({"Service", "connection", "VFC", "Koivu.Cloud", "Koivu", "ValueFlow", "Token"})
@CapabilityDescription("Koivu.Cloud Connection service for pre-authenticated access token.")
public class KoivuVfcServicePreAuthenticated extends KoivuVfcService
{
  private final static List<PropertyDescriptor> propertyDescriptors;

  private static final PropertyDescriptor PROPERTY_VFC_CLIENT_TOKEN = new PropertyDescriptor.Builder( )
      .name( "clienttoken" ) //
      .displayName( "Client Token" ) //
      .description( "VFC client access token" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( false ).build( );

  private static final PropertyDescriptor PROPERTY_VFC_CLIENT_TOKEN_TYPE = new PropertyDescriptor.Builder( )
      .name( "clienttokentype" ) //
      .displayName( "Client Token Type" ) //
      .description( "VFC client access token type" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .defaultValue( "Bearer" ) //
      .required( true ).sensitive( false ).build( );

  static
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_VFC_CLIENT_TOKEN );
    props.add( PROPERTY_VFC_CLIENT_TOKEN_TYPE );
    KoivuVfcService.addPropertyDescriptors( props );

    propertyDescriptors = Collections.unmodifiableList( props );
  }

  private VfcServerConnection connection = null;

  public KoivuVfcServicePreAuthenticated( )
  {
  }

  @Override
  protected List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @Override
  protected AuthType getAuthenticationType( )
  {
    return AuthType.firebase;
  }

  @Override
  @OnEnabled
  public void enabled( final ConfigurationContext context ) throws InitializationException
  {
    enableLogger( );

    String strVfcURL = context.getProperty( PROPERTY_VFC_URL ).evaluateAttributeExpressions( )
        .getValue( );

    logger.logInfo( "Enabling" + this.getClass( ).getSimpleName( ) + ": " + strVfcURL );

    try
    {
      logger.logInfo( "-- Starting VFC server connection" );
      VfcProperties vfcProperties = new VfcProperties( );
      for( PropertyDescriptor key : context.getProperties( ).keySet( ) )
      {
        vfcProperties.put( key.getName( ), context.getProperty( key ).getValue( ) == null ? ""
            : context.getProperty( key ).evaluateAttributeExpressions( ).getValue( ) );
      }
      vfcProperties.put( AUTH_TYPE, getAuthenticationType( ).toString( ) );

      URL vfcURL = new URL( strVfcURL );
      logger.logInfo( "  VFC URL:" + vfcURL );
      connection = new VfcServerConnection( vfcURL, vfcProperties );
      logger.logInfo( "-- Successfully started VFC server connection" );
    }
    catch( IOException vfcStartError )
    {
      StringBuilder error = new StringBuilder( );
      Throwable t = vfcStartError;
      while( t != null )
      {
        error.append( t.getClass( ).getSimpleName( ) ).append( ":'" );
        error.append( t.getMessage( ) ).append( "'. " );
        t = t.getCause( );
      }
      logger.logError( error.toString( ) );
      throw new InitializationException( vfcStartError );
    }
  }

  @Override
  @OnDisabled
  public void disabled( final ConfigurationContext context )
  {
    enableLogger( );

    logger.logInfo( "Disabling " + this.getClass( ).getSimpleName( ) + ": "
        + context.getAllProperties( ).get( PROPERTY_VFC_URL.getName( ) ) );

    logger.logInfo( "-- Stopping VFC server connection" );
    if( this.connection != null )
    {
      connection.close( );
      connection = null;
    }
    logger.logInfo( "-- Successfully stopped VFC server connection" );
  }

  @Override
  public VfcClient getVfcClient( FlowFile flowfile ) throws NotInitialized
  {
    try
    {
      if( connection == null )
      {
        throw new NotInitialized( this.getClass( ).getSimpleName( ) + " is not active." );
      }
      VfcProperties vfcProperties = new VfcProperties( );
      for( PropertyDescriptor desc : getSupportedPropertyDescriptors( ) )
      {
        String propValue = getProperty( desc ).evaluateAttributeExpressions( flowfile ).getValue( );
        vfcProperties.put( desc.getName( ), propValue == null ? "" : propValue );
      }

      String vfcClientToken = getProperty( PROPERTY_VFC_CLIENT_TOKEN )
          .evaluateAttributeExpressions( flowfile ).getValue( );
      String vfcClientTokenType = getProperty( PROPERTY_VFC_CLIENT_TOKEN_TYPE )
          .evaluateAttributeExpressions( flowfile ).getValue( );
      Authentication authentication = new PreAuthenticated( vfcClientToken, vfcClientTokenType );

      return new VfcClient( connection, vfcProperties, authentication );
    }
    catch( Exception e )
    {
      throw new NotInitialized(
          this.getClass( ).getSimpleName( ) + " cannot get Vfc Client:" + e.getMessage( ), e );
    }
  }

}
