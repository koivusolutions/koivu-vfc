package com.koivusolutions.vfc.nifi.processor.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

class FlowFileWriter
{
  private final int splitSize;
  private final HashSet<String> propertyKeys = new HashSet<>( );
  private final HashSet<String> expressionKeys = new HashSet<>( );
  protected final ProcessSession session;
  protected FlowFile flowFile;

  private String lastDataTimeStamp = null;
  private int maxKeyLength = 0;

  private boolean first = true;
  private OutputStreamWriter writer = null;
  private int batchSize = 0;
  private int count = 0;

  FlowFileWriter( String cloudTable, int splitSize, final ProcessSession session,
      final FlowFile flowFile )
  {
    this.splitSize = splitSize;
    this.session = session;
    this.flowFile = flowFile;

    setAttribute( "mime.type", "application/json" );
    setAttribute( "vfc.cloudTable", cloudTable );
    setAttribute( "path", cloudTable );
  }

  protected void setAttribute( String name, String value )
  {
    this.flowFile = session.putAttribute( flowFile, name, value != null ? value : "" );
  }

  public void open( OutputStream out ) throws IOException
  {
    first = true;
    writer = new OutputStreamWriter( new BufferedOutputStream( out ), StandardCharsets.UTF_8 );
    batchSize = splitSize > 0 ? splitSize : Integer.MAX_VALUE;
    writer.write( "[\n" );
  }

  public boolean write( CloudTableRow currentRow ) throws IOException
  {
    try
    {
      if( !first )
      {
        writer.write( ",\n" );
      }
      JSONObject asJson = currentRow.getAsJson( );
      writer.write( asJson.toString( ) );
      count++;

      int keyLenght = currentRow.getKeys( ).length;
      maxKeyLength = keyLenght > maxKeyLength ? keyLenght : maxKeyLength;

      Map<String, String> properties = currentRow.getProperties( );
      if( properties != null )
      {
        propertyKeys.addAll( properties.keySet( ) );
      }
      Map<String, String> expressions = currentRow.getExpressions( );
      if( expressions != null )
      {
        expressionKeys.addAll( expressions.keySet( ) );
      }

      if( lastDataTimeStamp == null
          || lastDataTimeStamp.compareTo( currentRow.getLastModifiedOn( ) ) < 0 )
      {
        lastDataTimeStamp = currentRow.getLastModifiedOn( );
      }
      first = false;

      return --batchSize > 0 ? true : false;
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
  }

  public void close( ) throws IOException
  {
    writer.write( "\n]" );
    writer.flush( );
    writer.close( );
    writer = null;
  }

  public String getLastDataTimeStamp( )
  {
    return lastDataTimeStamp;
  }

  public String getFields( )
  {
    List<String> fields = new ArrayList<>( );

    fields.add( CloudTableRow.SYSTEM_CATEGORY );

    for( int i = 1; i <= this.maxKeyLength; i++ )
    {
      fields.add( CloudTableRow.COLUMN_TYPE_KEYS + VfcReadProcessor.DOT
          + CloudTableRow.KEY_COLUMN_PREFIX + i );
    }

    for( String property : this.propertyKeys )
    {
      fields.add( CloudTableRow.COLUMN_TYPE_PROPERTIES + VfcReadProcessor.DOT + property );
    }
    for( String expression : this.expressionKeys )
    {
      fields.add( CloudTableRow.COLUMN_TYPE_EXPRESSIONS + VfcReadProcessor.DOT + expression );
    }
    fields.add( CloudTableRow.SYSTEM_LAST_MODIFIED_BY );
    fields.add( CloudTableRow.SYSTEM_LAST_MODIFIED_TIME );
    return StringUtils.join( fields, '|' );
  }

  public int getCount( )
  {
    return count;
  }

}
