package com.koivusolutions.vfc.nifi.processor.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.NoSuchElementException;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class VfcJsonFileWriterCallback implements OutputStreamCallback
{
  private final ProcessSession session;
  private FlowFile flowFile;
  private boolean first = true;
  private OutputStreamWriter writer = null;
  private int count = 0;
  private final VfcIterator<JSONObject> cursor;

  public VfcJsonFileWriterCallback( String URL, VfcIterator<JSONObject> cursor,
      final ProcessSession session, final FlowFile flowFile )
  {
    this.session = session;
    this.flowFile = flowFile;
    this.cursor = cursor;

    setAttribute( "mime.type", "application/json" );
    setAttribute( "path", URL );
  }

  protected void setAttribute( String name, String value )
  {
    this.flowFile = session.putAttribute( flowFile, name, value != null ? value : "" );
  }

  public void open( OutputStream out ) throws IOException
  {
    first = true;
    writer = new OutputStreamWriter( new BufferedOutputStream( out ), StandardCharsets.UTF_8 );
    writer.write( "[\n" );
  }

  @Override
  public void process( OutputStream out ) throws IOException
  {
    try
    {
      open( out );
      while( cursor.hasNext( ) )
      {
        JSONObject currentRow = cursor.next( );
        write( currentRow );
      }
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
    catch( VfcClientException e )
    {
      VfcReadProcessor.setVariables( session, flowFile, e.getResponseStatus( ) );
      throw new IOException( e );
    }
    finally
    {
      close( );
    }
  }

  public void write( JSONObject currentRow ) throws IOException
  {
    try
    {
      if( !first )
      {
        writer.write( ",\n" );
      }
      writer.write( currentRow.toString( ) );
      count++;

      first = false;
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
  }

  public void close( ) throws IOException
  {
    writer.write( "\n]" );
    writer.flush( );
    writer.close( );
    writer = null;
  }

  public int getCount( )
  {
    return count;
  }

}
