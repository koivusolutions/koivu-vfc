package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadKeyRangeWrapper;

@Tags({"get", "read", "full query", "key range", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Streams VFC data collections search by keys range.")
public class VfcReadKeyRange extends VfcRead
{

  public VfcReadKeyRange( )
  {
    super( new VfcReadKeyRangeWrapper( ) );
  }

}
