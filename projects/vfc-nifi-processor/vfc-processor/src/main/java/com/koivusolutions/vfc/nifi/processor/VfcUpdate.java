package com.koivusolutions.vfc.nifi.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;

import com.koivusolutions.vfc.client.cloudtable.*;

@Tags({"put", "write", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("PUT VFC data collections")
public class VfcUpdate extends VfcWrite
{

  public VfcUpdate( )
  {
  }

  @Override
  protected CloudTableUpdater<CloudTableRow> getUpdater( CloudTables cloudTables )
  {
    return cloudTables.updateCloudTable( );
  }

  @Override
  protected List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_SKIP_IDENTICAL );
    return props;
  }

}
