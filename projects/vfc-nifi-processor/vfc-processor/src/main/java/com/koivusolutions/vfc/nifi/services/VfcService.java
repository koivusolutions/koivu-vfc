package com.koivusolutions.vfc.nifi.services;

import org.apache.nifi.controller.ControllerService;
import org.apache.nifi.flowfile.FlowFile;

import com.koivusolutions.vfc.client.VfcClient;

public interface VfcService extends ControllerService
{
  public VfcClient getVfcClient( FlowFile flowfile ) throws NotInitialized;
}
