package com.koivusolutions.vfc.nifi.processor.vfc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.components.PropertyDescriptor.Builder;
import org.apache.nifi.context.PropertyContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessContext;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.services.VfcService;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

public class VfcReadExactKeysWrapper extends VfcReaderWrapper
{
  private static final Builder keyBuilder = new PropertyDescriptor.Builder( ).name( "Key Array" ) //
      .displayName( "Key Array for searching multiple rows" ) //
      .description(
          "Separator character separated list of Cloud Table keys for data query. List separator character is defined with 'Key Array Separator' " ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ); //

  private static final PropertyDescriptor PROPERTY_EXACT_MANDATORY_KEYS = keyBuilder
      .required( true ).build( );
  private static final PropertyDescriptor PROPERTY_EXACT_KEYS = keyBuilder.required( false )
      .build( );

  protected static final PropertyDescriptor PROPERTY_KEY_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "Key Array Separator" ) //
      .displayName( "Key Array Separator" ) //
      .description( "List separator character for Cloud Table 'Key Array'" ) //
      .required( true ) //
      .defaultValue( "," ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  private final boolean mandatoryKeys;

  public VfcReadExactKeysWrapper( )
  {
    super( );
    mandatoryKeys = true;
  }

  public VfcReadExactKeysWrapper( boolean mandatoryKeys )
  {
    super( );
    this.mandatoryKeys = mandatoryKeys;
  }

  @Override
  public List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_VFC_SERVICE );
    props.add( PROPERTY_CLOUD_TABLE );
    props.add( mandatoryKeys ? PROPERTY_EXACT_MANDATORY_KEYS : PROPERTY_EXACT_KEYS );
    props.add( PROPERTY_KEY_SEPARATOR );
    props.add( PROPERTY_FILTER );
    props.add( PROPERTY_FILTER_BY_LEVELS );
    props.add( PROPERTY_EXPRESSION );
    props.add( PROPERTY_SPLIT_SIZE );
    props.add( PROPERTY_INCLUDE_DELETED );
    props.add( PROPERTY_IMPERSONATE_BUSINESS );
    return props;
  }

  @Override
  public CloudTable getCloudTable( final ProcessContext context, final FlowFile flowFile,
      String newerThan ) throws IOException,
      NotInitialized
  {
    return getCloudTable( (PropertyContext)context, flowFile, newerThan );
  }

  public CloudTable getCloudTable( final PropertyContext context ) throws NotInitialized
  {
    return getCloudTable( context, null, null );
  }

  public CloudTable getCloudTable( final PropertyContext context, final FlowFile flowFile,
      String newerThan ) throws NotInitialized
  {
    String cloudTable = getPropertyValue( context, flowFile, PROPERTY_CLOUD_TABLE );

    String stringSeparator = getPropertyValue( context, flowFile, PROPERTY_KEY_SEPARATOR );
    String keysString = getPropertyValue( context, flowFile, PROPERTY_EXACT_KEYS );
    String[] keys = StringUtils.isNotBlank( keysString )
        ? processKeys( keysString.split( Pattern.quote( stringSeparator ) ) )
        : new String[0];
    String expressions = getPropertyValue( context, flowFile, PROPERTY_EXPRESSION );
    String filter = getPropertyValue( context, flowFile, PROPERTY_FILTER );
    boolean filterByLevels = Boolean
        .valueOf( getPropertyValue( context, flowFile, PROPERTY_FILTER_BY_LEVELS ) )
        .booleanValue( );
    String impersonateBusiness = getPropertyValue( context, flowFile,
        PROPERTY_IMPERSONATE_BUSINESS );
    boolean includeDeleted = Boolean
        .valueOf( getPropertyValue( context, flowFile, PROPERTY_INCLUDE_DELETED ) ).booleanValue( );

    VfcService vfcService = context.getProperty( PROPERTY_VFC_SERVICE )
        .asControllerService( VfcService.class );
    VfcClient vfcClient = vfcService.getVfcClient( flowFile );

    CloudTable t = vfcClient.getCloudTables( ).getCloudTableExactKeys( cloudTable, keys, filter,
        filterByLevels, newerThan, expressions, impersonateBusiness, includeDeleted );
    return t;
  }

}
