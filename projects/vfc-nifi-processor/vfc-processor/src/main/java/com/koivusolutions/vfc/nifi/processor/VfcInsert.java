package com.koivusolutions.vfc.nifi.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;

import com.koivusolutions.vfc.client.cloudtable.*;

@Tags({"post", "write", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("POST VFC data collections")
public class VfcInsert extends VfcWrite
{

  public VfcInsert( )
  {
  }

  @Override
  protected CloudTableUpdater<CloudTableRow> getUpdater( CloudTables cloudTables )
  {
    return cloudTables.insertToTable( );
  }

  @Override
  protected List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_SKIP_IDENTICAL );
    return props;
  }

}
