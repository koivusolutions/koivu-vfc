package com.koivusolutions.vfc.nifi.validators;

import org.apache.nifi.components.ValidationResult;

public class SingleCharacterValidator extends StringValidator
{

  @Override
  protected ValidationResult validate( String subject, String input )
  {
    boolean valid = input != null && input.length( ) == 1;
    String message = !valid ? "the value must be a single character in length." : "";

    return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( valid )
        .explanation( message ).build( );
  }

}
