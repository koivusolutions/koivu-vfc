package com.koivusolutions.vfc.nifi.validators;

import java.util.regex.Pattern;

import org.apache.nifi.components.*;

import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class VfcNameValidator implements Validator
{
  private static Pattern valid = Pattern.compile(
      "^((properties|expressions)\\.[^\\&\\#\\|\\s]+|keys.key(\\d+)\\s*|category|description|level|lastModifiedTime|lastModifiedBy)$" );

  private final boolean VfcInInput;

  public VfcNameValidator( boolean VfcInInput )
  {
    this.VfcInInput = VfcInInput;
  }

  @Override
  public ValidationResult validate( String subject, String input, ValidationContext context )
  {
    String vfcString = VfcInInput ? input : subject;
    String fieldString = VfcInInput ? subject : input;

    String separator = context.getProperty( VfcReadProcessor.PROPERTY_CSV_SEPARATOR ).getValue( );
    if( fieldString.contains( separator ) )
    {
      return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( false )
          .explanation( "column name contains CSV field separator character '" + separator + "'" )
          .build( );
    }

    boolean isValid = isValidVfcName( vfcString );
    if( isValid )
    {
      return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( true )
          .build( );
    }
    else
    {
      return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( false )
          .explanation( "the VFC data path is not a valid VFC path expression. "
              + "Valid ones are; keys.key<key_no>, properties.<property_name>, "
              + "expressions.<expression_name>, category, description, "
              + "lastModifiedTime or lastModifiedBy" )
          .build( );
    }
  }

  static public boolean isValidVfcName( String vfcString )
  {
    return valid.matcher( vfcString ).find( );
  }
}
