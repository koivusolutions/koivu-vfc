package com.koivusolutions.vfc.nifi.processor.vfc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessContext;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.services.VfcService;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

public class VfcReadKeyRangeWrapper extends VfcReaderWrapper
{
  private static final String FIRST_KEY_MARKER = "_FIRST";
  private static final String LAST_KEY_MARKER = "_LAST";
  private static final String FIRST_KEY_CHAR_SEQUENCE = "\u0000";
  private static final String LAST_KEY_CHAR_SEQUENCE = "\uffff";

  protected static final PropertyDescriptor PROPERTY_KEYS_FROM = new PropertyDescriptor.Builder( )
      .name( "Key Range From Array" ) //
      .displayName( "Key Range From Array" ) //
      .description(
          "Separator character separated list of Cloud Table keys for data query. List separator character is defined with 'Key Array Separator'.  "
              + "Inclusive. Constant '" + FIRST_KEY_MARKER
              + "' can be used as placeholde for indicating first possible key." ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_KEYS_TO = new PropertyDescriptor.Builder( )
      .name( "Key Range To Array" ) //
      .displayName( "Key Range To Array" ) //
      .description(
          "Separator character separated list of Cloud Table keys for data query. List separator character is defined with 'Key Array Separator'.  "
              + "Exclusive. Constant '" + LAST_KEY_MARKER
              + "' can be used as placeholde for indicating last possible key." ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_KEY_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "Key Array Separator" ) //
      .displayName( "Key Array Separator" ) //
      .description(
          "List separator character for Cloud Table 'Key Range Start Array' and 'Key Range End Array'" ) //
      .required( true ) //
      .defaultValue( "," ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  public VfcReadKeyRangeWrapper( )
  {
    super( );
  }

  @Override
  public List<PropertyDescriptor> initProperties( )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_VFC_SERVICE );
    props.add( PROPERTY_CLOUD_TABLE );
    props.add( PROPERTY_KEYS_FROM );
    props.add( PROPERTY_KEYS_TO );
    props.add( PROPERTY_KEY_SEPARATOR );
    props.add( PROPERTY_FILTER );
    props.add( PROPERTY_FILTER_BY_LEVELS );
    props.add( PROPERTY_EXPRESSION );
    props.add( PROPERTY_SPLIT_SIZE );
    props.add( PROPERTY_INCLUDE_DELETED );
    props.add( PROPERTY_IMPERSONATE_BUSINESS );
    return props;
  }

  @Override
  public CloudTable getCloudTable( final ProcessContext context, final FlowFile flowFile,
      String newerThan ) throws IOException,
      NotInitialized
  {
    String cloudTable = getPropertyValue( context, flowFile, PROPERTY_CLOUD_TABLE );

    String stringSeparator = getPropertyValue( context, flowFile, PROPERTY_KEY_SEPARATOR );
    String keysFromString = getPropertyValue( context, flowFile, PROPERTY_KEYS_FROM );
    String keysToString = getPropertyValue( context, flowFile, PROPERTY_KEYS_TO );
    String[] keysFrom = processKeys( keysFromString.split( Pattern.quote( stringSeparator ) ) );
    String[] keysTo = processKeys( keysToString.split( Pattern.quote( stringSeparator ) ) );
    String expressions = getPropertyValue( context, flowFile, PROPERTY_EXPRESSION );
    String filter = getPropertyValue( context, flowFile, PROPERTY_FILTER );
    boolean filterByLevels = Boolean
        .valueOf( getPropertyValue( context, flowFile, PROPERTY_FILTER_BY_LEVELS ) )
        .booleanValue( );

    String impersonateBusiness = getPropertyValue( context, flowFile,
        PROPERTY_IMPERSONATE_BUSINESS );
    boolean includeDeleted = Boolean
        .valueOf( getPropertyValue( context, flowFile, PROPERTY_INCLUDE_DELETED ) ).booleanValue( );

    VfcService vfcService = context.getProperty( PROPERTY_VFC_SERVICE )
        .asControllerService( VfcService.class );
    VfcClient vfcClient = vfcService.getVfcClient( flowFile );

    CloudTable t = vfcClient.getCloudTables( ).getCloudTableWithKeyRange( cloudTable, keysFrom,
        keysTo, filter, filterByLevels, newerThan, expressions, impersonateBusiness,
        includeDeleted );
    return t;
  }

  @Override
  protected String[] processKeys( String[] keys )
  {
    String[] ret = new String[keys.length];
    for( int i = 0; i < keys.length; i++ )
    {
      String key = keys[i].trim( );
      if( FIRST_KEY_MARKER.equals( key ) )
      {
        ret[i] = FIRST_KEY_CHAR_SEQUENCE;
      }
      else if( LAST_KEY_MARKER.equals( key ) )
      {
        ret[i] = LAST_KEY_CHAR_SEQUENCE;
      }
      else
      {
        ret[i] = key;
      }
    }
    return ret;
  }

}
