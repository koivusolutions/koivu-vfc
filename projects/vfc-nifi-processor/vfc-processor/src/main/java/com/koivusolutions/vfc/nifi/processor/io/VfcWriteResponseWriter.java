package com.koivusolutions.vfc.nifi.processor.io;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.Relationship;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcRowUpdateStatus;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class VfcWriteResponseWriter
{
  private final FlowFile originalFlowFile;
  private final ProcessSession session;

  private FlowFile flowFile;
  private OutputStreamWriter writer;
  private int count = 0;
  private String errorMessage;

  public VfcWriteResponseWriter( ProcessSession session, FlowFile originalFlowFile )
  {
    this.session = session;
    this.originalFlowFile = originalFlowFile;
  }

  public void write( VfcRowUpdateStatus<CloudTableRow> result, VfcResponseStatus vfcResponseStatus )
      throws IOException
  {
    if( writer == null )
    {
      flowFile = session.create( originalFlowFile );
      VfcReadProcessor.setVariables( session, flowFile, vfcResponseStatus );
      writer = new OutputStreamWriter( new BufferedOutputStream( session.write( flowFile ) ),
          StandardCharsets.UTF_8 );
      writer.write( "[\n" );
    }
    else
    {
      writer.write( ",\n" );
    }
    JSONObject json = result.getRow( ).getAsJson( );
    if( json != null )
    {
      JSONObject writeResponse = new JSONObject( );
      writeResponse.put( "Response", result.getResponse( ).toString( ) );
      writeResponse.put( "Message", result.getResponseMessage( ) );
      json.put( "writeResponse", writeResponse );
      writer.write( json.toString( ) );
      count++;
    }
  }

  public FlowFile commit( Relationship rel ) throws IOException
  {
    if( writer != null )
    {
      writer.write( "\n]" );
      writer.flush( );
      writer.close( );
      writer = null;
      VfcReadProcessor.setSessionAttribute( session, flowFile, VfcReadProcessor.ATTRIBUTE_ROW_COUNT,
          String.valueOf( count ) );
      if( StringUtils.isNotBlank( errorMessage ) )
      {
        VfcReadProcessor.setSessionAttribute( session, flowFile, VfcReadProcessor.ATTRIBUTE_ERROR,
            errorMessage );
      }
      session.transfer( flowFile, rel );
      session.adjustCounter( "VFC '" + rel.getName( ) + "' rows written", count, false );
      session.getProvenanceReporter( ).modifyContent( flowFile );
      return flowFile;
    }
    return null;
  }

  public void setErrorMessage( String message )
  {
    this.errorMessage = message;
  }

}
