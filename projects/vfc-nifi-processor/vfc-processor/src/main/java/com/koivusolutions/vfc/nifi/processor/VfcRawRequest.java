package com.koivusolutions.vfc.nifi.processor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;
import org.json.JSONException;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;
import com.koivusolutions.vfc.client.requests.VfcRequest;
import com.koivusolutions.vfc.client.requests.VfcRequests;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.io.VfcJsonFileWriterCallback;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.services.VfcService;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@Tags({"get", "post", "patch", "put", "delete", "write", "read", "raw", "Koivu", "ValueFlow", "VFC",
    "Koivu.Cloud"})
@CapabilityDescription("Makes REST request to VFC instance")
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@DynamicProperty(name = "HTTP query parameters", value = "HTTP query parameter name value pairs.", description = "User-defined properties identify HTTP query parameters used when making the request to VFC", expressionLanguageScope = ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
public class VfcRawRequest extends AbstractProcessor implements VfcWriteProcessor
{
  private final static String TRUE = "true";
  private final static String FALSE = "false";

  private final static String METHOD_GET = "GET";
  private final static String METHOD_POST = "POST";
  private final static String METHOD_PUT = "PUT";
  private final static String METHOD_PATCH = "PATCH";
  private final static String METHOD_DELETE = "DELETE";

  protected static final PropertyDescriptor PROPERTY_VFC_SERVICE = new PropertyDescriptor.Builder( )
      .name( "Value Flow Cloud Service" ) //
      .displayName( "Koivu.Cloud Service" ) //
      .description( "Koivu.Cloud Service to define Koivu.Cloud instance connection info" ) //
      .required( true ) //
      .addValidator( Validator.VALID ) //
      .identifiesControllerService( VfcService.class ).build( );

  protected static final PropertyDescriptor PROPERTY_METHOD = new PropertyDescriptor.Builder( )
      .name( "http method" ) //
      .displayName( "HTTP method" ) //
      .description( "HTTP method used to make the request" ) //
      .required( true ) //
      .allowableValues( //
          new AllowableValue( METHOD_GET, METHOD_GET ), //
          new AllowableValue( METHOD_POST, METHOD_POST ), //
          new AllowableValue( METHOD_PUT, METHOD_PUT ), //
          new AllowableValue( METHOD_PATCH, METHOD_PATCH ), //
          new AllowableValue( METHOD_DELETE, METHOD_DELETE ) ) //
      .addValidator( Validator.VALID ) //
      .defaultValue( METHOD_POST ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_URL = new PropertyDescriptor.Builder( )
      .name( "URL" ) //
      .displayName( "URL" ) //
      .description( "URL inside VFC for example; publish" ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_SEND_FLOWFILE = new PropertyDescriptor.Builder( )
      .name( "send data" ) //
      .displayName( "Send Data" ) //
      .description(
          "Send flow file content to VFC with the request. Flow file content needs to be valid JSON." ) //
      .required( true ) //
      .allowableValues( //
          new AllowableValue( TRUE, "Send flow file content" ), //
          new AllowableValue( FALSE, "Do not send data" ) ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  protected static final PropertyDescriptor PROPERTY_QUERY_PARAM_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "qyery param value separator" ) //
      .displayName( "Query parameter list separator" ) //
      .description( "Query parameter value can be a specified character separated list of values. "
          + "For example, if this is defined as pipe character (|) then any query parameter having a value a|b|c,"
          + " is send as a list of three values a, b and c" ) //
      .required( false ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public VfcRawRequest( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_VFC_SERVICE );
    props.add( PROPERTY_METHOD );
    props.add( PROPERTY_URL );
    props.add( PROPERTY_SEND_FLOWFILE );
    props.add( PROPERTY_QUERY_PARAM_SEPARATOR );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_DATA_NOT_FOUND );
    rels.add( REL_FAILURE );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile originalFlowFile = session.get( );
    if( originalFlowFile == null )
    {
      return;
    }

    FlowFile flowFile = originalFlowFile;
    try
    {
      VfcService vfcService = context.getProperty( PROPERTY_VFC_SERVICE )
          .asControllerService( VfcService.class );
      VfcClient vfcClient = vfcService.getVfcClient( flowFile );

      final VfcRequest request = getRequestPerType( context, session, flowFile,
          vfcClient.getVfcRequest( ) );

      VfcIterator<JSONObject> results = request.getReponseBody( );

      if( results != null )
      {
        if( !results.hasNext( ) )
        {
          getLogger( ).debug( "Data not found; {}",
              new Object[]{results.getResponseStatus( ).toString( )} );
          setVariables( session, flowFile, results.getResponseStatus( ) );
          session.transfer( flowFile, REL_DATA_NOT_FOUND );
        }
        else
        {
          VfcJsonFileWriterCallback out = new VfcJsonFileWriterCallback( request.getUrl( ), results,
              session, flowFile );
          session.write( flowFile, out );

          getLogger( ).info( "{} processed ok", new Object[]{results.getResponseStatus( )} );
          String transitUri = getTransitURI( request );
          session.getProvenanceReporter( ).send( flowFile, transitUri );
          session.transfer( flowFile, REL_SUCCESS );
        }
      }
    }
    catch( final FlowFileAccessException | IOException | NotInitialized t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      throw new ProcessException( t );
    }
    catch( final VfcClientException t )
    {
      getLogger( ).warn( "VFC status: {}, {} failed to process due to {}",
          new Object[]{t.getResponseStatus( ).toString( ), this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      setVariables( session, flowFile, t.getResponseStatus( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
    catch( final JSONException t )
    {
      getLogger( ).warn( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR,
          "Flow file is not a valid JSON: " + t.getMessage( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
  }

  private VfcRequest getRequestPerType( ProcessContext context, ProcessSession session,
      FlowFile flowFile, VfcRequests vfcRequests ) throws IOException
  {
    String method = getPropertyValue( context, flowFile, PROPERTY_METHOD );
    String URL = getPropertyValue( context, flowFile, PROPERTY_URL );
    Map<String, String[]> queryParams = getQueryParams( context, flowFile );

    String sendData = getPropertyValue( context, flowFile, PROPERTY_SEND_FLOWFILE );
    String data = null;
    if( TRUE.equals( sendData ) )
    {
      ByteArrayOutputStream content = new ByteArrayOutputStream( (int)flowFile.getSize( ) );
      session.exportTo( flowFile, content );
      content.close( );
      data = new String( content.toByteArray( ), "UTF-8" );
    }

    switch( method )
    {
      case METHOD_POST:
        return vfcRequests.makePostRequests( URL, queryParams, data );
      case METHOD_PUT:
        return vfcRequests.makePutRequests( URL, queryParams, data );
      case METHOD_PATCH:
        return vfcRequests.makePatchRequests( URL, queryParams, data );
      case METHOD_DELETE:
        return vfcRequests.makeDeleteRequests( URL, queryParams, data );
      default:
        return vfcRequests.makeGetRequests( URL, queryParams );
    }
  }

  private Map<String, String[]> getQueryParams( ProcessContext context, FlowFile flowFile )
  {
    String separator = getPropertyValue( context, flowFile, PROPERTY_QUERY_PARAM_SEPARATOR );

    Map<String, String[]> params = new HashMap<>( );

    Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
    for( PropertyDescriptor propertyDescriptor : props )
    {
      if( propertyDescriptor.isDynamic( ) )
      {
        String values = getPropertyValue( context, flowFile, propertyDescriptor );
        String[] value;

        if( StringUtils.isNotBlank( separator ) )
        {
          value = values.split( Pattern.quote( separator ) );
        }
        else
        {
          value = new String[]{values};
        }
        params.put( propertyDescriptor.getName( ), value );
      }
    }
    return params;
  }

  protected String getTransitURI( VfcRequest request )
  {
    String transitUri = request.getResponseStatus( ) != null
        ? ( ( request.getResponseStatus( ).getMeta( ) != null )
            ? request.getResponseStatus( ).getMeta( ).get( "request" )
            : request.getResponseStatus( ).toString( ) )
        : "";
    return transitUri;
  }

  private void setVariables( ProcessSession session, FlowFile flowFile,
      VfcResponseStatus responseStatus )
  {
    VfcReadProcessor.setVariables( session, flowFile, responseStatus );
  }

  private String getPropertyValue( final ProcessContext context, final FlowFile flowFile,
      PropertyDescriptor propertyName )
  {
    return flowFile == null
        ? context.getProperty( propertyName ).evaluateAttributeExpressions( ).getValue( )
        : context.getProperty( propertyName ).evaluateAttributeExpressions( flowFile ).getValue( );
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description( "HTTP query parameter named:" + propertyDescriptorName ).dynamic( true )
        .required( false ).addValidator( new StringValidator( ) ).build( );
  }

  @Override
  protected Collection<ValidationResult> customValidate( final ValidationContext validationContext )
  {
    return Collections.emptyList( );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
