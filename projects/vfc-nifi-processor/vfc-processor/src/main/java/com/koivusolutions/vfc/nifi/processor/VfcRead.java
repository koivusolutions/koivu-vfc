package com.koivusolutions.vfc.nifi.processor;

import java.io.IOException;
import java.util.*;

import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.vfc.VfcReaderWrapper;
import com.koivusolutions.vfc.nifi.services.NotInitialized;

@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@WritesAttributes({

    @WritesAttribute(attribute = "vfc.response.status", description = "This is the VFC response status. Possible values are OK or ERROR."),
    @WritesAttribute(attribute = "vfc.response.httpStatus", description = "This is the HTTP status code of VFC request."),

    @WritesAttribute(attribute = "vfc.error.errorMessage", description = "Clear text error message."),
    @WritesAttribute(attribute = "vfc.error.errorCode", description = "Error number."),
    @WritesAttribute(attribute = "vfc.error.stackTrace", description = "Error stack trace in VFC server. Only returned from VFC development instances."),

    @WritesAttribute(attribute = "vfc.meta", description = "VFC request meta information in JSON format. Content is request dependant"),

    @WritesAttribute(attribute = "vfc.pagination.responseCount", description = "Row count of returned data"),
    @WritesAttribute(attribute = "vfc.pagination.hasMoreData", description = "Still more data to fetch?"),
    @WritesAttribute(attribute = "vfc.pagination.continueToken", description = "Token to use for next data fetch"),

    @WritesAttribute(attribute = "vfc.response.batch.no", description = "When VFC response is split to multiple flowfiles, tells sequence number of split"),
    @WritesAttribute(attribute = "vfc.cloudTable", description = "Cloud table name"),
    @WritesAttribute(attribute = "vfc.row.count", description = "Count of rows in flowfile"),

    @WritesAttribute(attribute = VfcReadProcessor.ATTRIBUTE_ERROR, description = "Error message in case fo exception")})

abstract class VfcRead extends AbstractProcessor implements VfcReadProcessor
{
  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;
  private final VfcReaderWrapper reader;

  public VfcRead( VfcReaderWrapper reader )
  {
    super( );
    this.reader = reader;
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = reader.initProperties( );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    rels.add( REL_DATA_NOT_FOUND );
    rels.add( REL_FAILURE );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile originalFlowFile = session.get( );
    if( originalFlowFile == null )
    {
      return;
    }

    FlowFile flowFile = originalFlowFile;
    try
    {
      CloudTable t = reader.getCloudTable( context, flowFile, null );

      VfcIterator<CloudTableRow> cursor = t.iteratorEx( );
      if( !cursor.hasNext( ) )
      {
        getLogger( ).debug( "Data not found; {}",
            new Object[]{cursor.getResponseStatus( ).toString( )} );
        session.transfer( flowFile, REL_DATA_NOT_FOUND );
      }
      else
      {
        Integer splitSizeInteger = context.getProperty( reader.PROPERTY_SPLIT_SIZE )
            .evaluateAttributeExpressions( originalFlowFile ).asInteger( );
        int splitSize = splitSizeInteger != null ? splitSizeInteger.intValue( ) : -1;

        int batch = 1;
        while( cursor.hasNext( ) )
        {
          if( batch != 1 )
          {
            flowFile = session.create( originalFlowFile );
            setVariables( session, flowFile, cursor.getResponseStatus( ) );
          }
          VfcReadProcessor.setBatchNumberVariable( session, flowFile, batch++ );

          writeFlowFile( t, cursor, splitSize, session, flowFile );
          getLogger( ).info( "{} processed ok", new Object[]{cursor.getResponseStatus( )} );

          String transitUri = cursor.getResponseStatus( ).getMeta( ) != null
              ? cursor.getResponseStatus( ).getMeta( ).get( "request" )
              : cursor.getResponseStatus( ).toString( );
          session.getProvenanceReporter( ).receive( flowFile, transitUri );
          session.transfer( flowFile, REL_SUCCESS );
        }
      }
    }
    catch( final FlowFileAccessException | IOException | NotInitialized t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      throw new ProcessException( t );
    }
    catch( final VfcClientException t )
    {
      getLogger( ).warn( "VFC status: {}, {} failed to process due to {}",
          new Object[]{t.getResponseStatus( ).toString( ), this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      setVariables( session, flowFile, t.getResponseStatus( ) );
      session.transfer( flowFile, REL_FAILURE );
    }
  }

  private void writeFlowFile( CloudTable t, VfcIterator<CloudTableRow> cursor, int splitSize,
      final ProcessSession session, final FlowFile flowFile )
  {
    VfcReadProcessor.writeFlowFile( t, cursor, splitSize, session, flowFile );
  }

  protected void setVariables( ProcessSession session, FlowFile flowFile,
      VfcResponseStatus responseStatus )
  {
    VfcReadProcessor.setVariables( session, flowFile, responseStatus );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
