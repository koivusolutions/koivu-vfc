package com.koivusolutions.vfc.nifi.logging;

import org.apache.nifi.logging.ComponentLog;

import com.koivusolutions.vfc.logging.Log;

public class Logging implements Log
{
  private final ComponentLog nifiLogger;

  public Logging( ComponentLog nifiLogger )
  {
    this.nifiLogger = nifiLogger;
  }

  @Override
  public void logError( String msg, Throwable t )
  {
    nifiLogger.error( msg, t );
  }

  @Override
  public void logError( String msg )
  {
    nifiLogger.error( msg );
  }

  @Override
  public void logWarn( String msg )
  {
    nifiLogger.warn( msg );
  }

  @Override
  public void logInfo( String msg )
  {
    nifiLogger.info( msg );
  }

  @Override
  public void logDebug( String msg )
  {
    nifiLogger.debug( msg );
  }

}
