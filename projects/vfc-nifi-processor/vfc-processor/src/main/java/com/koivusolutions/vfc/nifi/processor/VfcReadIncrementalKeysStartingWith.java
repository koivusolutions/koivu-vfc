package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadKeysStartingWithWrapper;

@Tags({"get", "read", "streaming query", "keys starting with", "Koivu", "ValueFlow", "VFC",
    "Koivu.Cloud"})
@CapabilityDescription("Streams VFC data collections search by keys starting with.")
public class VfcReadIncrementalKeysStartingWith extends VfcReadIncremental
{

  public VfcReadIncrementalKeysStartingWith( )
  {
    super( new VfcReadKeysStartingWithWrapper( ) );
  }

}
