package com.koivusolutions.vfc.nifi.lookupservices;

import java.util.*;
import java.util.Map.Entry;

import org.apache.nifi.annotation.behavior.DynamicProperties;
import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnDisabled;
import org.apache.nifi.annotation.lifecycle.OnEnabled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.controller.AbstractControllerService;
import org.apache.nifi.controller.ConfigurationContext;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.lookup.LookupFailureException;
import org.apache.nifi.lookup.LookupService;
import org.apache.nifi.reporting.InitializationException;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.*;
import com.koivusolutions.vfc.logging.Logger;
import com.koivusolutions.vfc.nifi.logging.Logging;
import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadExactKeysWrapper;
import com.koivusolutions.vfc.nifi.services.NotInitialized;
import com.koivusolutions.vfc.nifi.validators.NumberValidator;
import com.koivusolutions.vfc.nifi.validators.VfcNameValidator;

@Tags({"lookup", "read", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Use Koivu.Cloud service to look up values from CloudTable.")
@DynamicProperties({
    @DynamicProperty(name = "*", value = "*", description = "All dynamic properties are added as query to the Cloud Table")})
public class KoivuVfcMultiRowLookup extends AbstractControllerService
    implements LookupService<List<ImmutableCloudTableRow>>
{
  public static final PropertyDescriptor PROPERTY_CACHE_TIME = new PropertyDescriptor.Builder( )
      .name( "LookupCacheTime" ) //
      .displayName( "Caching time in seconds" ) //
      .description(
          "How long read table rows are cached in seconds. 0 mean no caching and -1 forever." ) //
      .required( false ) //
      .defaultValue( "3600" ).addValidator( new NumberValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ) //
      .build( );

  private final VfcReadExactKeysWrapper delegate = new VfcReadExactKeysWrapper( );
  private Logging logger;
  private CloudTable ct = null;
  private List<CloudTableRow> rows = null;
  private long expireTime = -1;
  private long lastRead = -1;

  @Override
  protected List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    List<PropertyDescriptor> props = delegate.initProperties( );
    props.add( PROPERTY_CACHE_TIME );

    return Collections.unmodifiableList( props );
  }

  @OnEnabled
  public void enabled( final ConfigurationContext context ) throws InitializationException
  {
    logger = new Logging( getLogger( ) );
    Logger.setExternalLogger( logger );

    logger.logInfo( "Enabling lookup" + this.getClass( ).getSimpleName( ) );

    try
    {
      logger.logInfo( "-- Starting VFC Lookup" );
      ct = delegate.getCloudTable( context );

      expireTime = context.getProperty( PROPERTY_CACHE_TIME ).asLong( ).longValue( );

      logger.logInfo( "-- Successfully started VFC Lookup for table:" + ct.getCategory( ) );
    }
    catch( NotInitialized vfcStartError )
    {
      StringBuilder error = new StringBuilder( );
      Throwable t = vfcStartError;
      while( t != null )
      {
        error.append( t.getClass( ).getSimpleName( ) ).append( ":'" );
        error.append( t.getMessage( ) ).append( "'. " );
        t = t.getCause( );
      }
      logger.logError( error.toString( ) );
      throw new InitializationException( vfcStartError );
    }
  }

  @OnDisabled
  public void disabled( final ConfigurationContext context )
  {
    if( logger == null )
    {
      logger = new Logging( getLogger( ) );
      Logger.setExternalLogger( logger );
    }

    logger.logInfo( "Disabling lookup" + this.getClass( ).getSimpleName( ) );

    logger.logInfo( "-- Stopping VFC Lookup for table:" + ( ct != null ? ct.getCategory( ) : "" ) );
    ct = null;
    logger.logInfo( "-- Successfully stopped VFC Lookup" );
  }

  @Override
  public Optional<List<ImmutableCloudTableRow>> lookup( Map<String, Object> coordinates )
      throws LookupFailureException
  {
    try
    {
      List<ImmutableCloudTableRow> ret = new ArrayList<>( );
      long now = System.currentTimeMillis( );
      if( rows == null || now > lastRead + expireTime * 1000 )
      {
        lastRead = now;
        rows = readLookupValues( );
      }

      if( rows != null )
      {
        for( CloudTableRow cloudTableRow : rows )
        {
          boolean match = true;
          for( Entry<String, Object> coordinate : coordinates.entrySet( ) )
          {
            String key = coordinate.getKey( );
            String value = coordinate.getValue( ).toString( );
            String propValue = getRowValue( cloudTableRow, key );
            if( !value.equals( propValue ) )
            {
              match = false;
              break;
            }
          }
          if( match )
          {
            ret.add( cloudTableRow );
          }
        }
      }

      return Optional.ofNullable( ret.size( ) == 0 ? null : ret );

    }
    catch( VfcClientException e )
    {
      throw new LookupFailureException( "VfcLookup error:" + e.getMessage( ), e );
    }

  }

  public String getRowValue( CloudTableRow cloudTableRow, String key )
  {
    String[] parts = key.split( "\\.", 2 );
    {
      if( parts.length == 1 )
      {
        parts = new String[]{CloudTableRow.COLUMN_TYPE_SYSTEM, parts[0]};
      }
    }

    String propValue = cloudTableRow.getAsString( parts[0], parts[1], null );
    return propValue;
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description( "The value from a VFC record for the '" + propertyDescriptorName
            + "' column which must match in lookup" )
        .dynamic( true ).required( false ).addValidator( new VfcNameValidator( false ) ).build( );
  }

  private List<CloudTableRow> readLookupValues( ) throws NoSuchElementException,
      VfcClientException
  {
    List<CloudTableRow> newRows = new ArrayList<>( );
    ct.reset( );
    VfcIterator<CloudTableRow> cursor = ct.iteratorEx( );
    while( cursor.hasNext( ) )
    {
      CloudTableRow currentRow = cursor.next( );
      newRows.add( currentRow );
    }
    return newRows;
  }

  @Override
  public Set<String> getRequiredKeys( )
  {
    return Collections.emptySet( );
  }

  @Override
  public Class<?> getValueType( )
  {
    return List.class;
  }

}
