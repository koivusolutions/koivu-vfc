package com.koivusolutions.vfc.nifi.services;

import java.util.*;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;

import com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AuthType;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@Tags({"Service", "connection", "VFC", "Koivu.Cloud", "Koivu", "ValueFlow", "Auth0"})
@CapabilityDescription("Koivu.Cloud Connection service for Auth0.")
public class KoivuVfcServiceAuth0 extends KoivuVfcService
{
  private final static List<PropertyDescriptor> propertyDescriptors;

  // Authentication
  private static final PropertyDescriptor PROPERTY_AUTH0_AUDIENCE = new PropertyDescriptor.Builder( )
      .name( "authentication.auth0.url.audience" ) //
      .displayName( "Audience" ) //
      .description( "Auth0 API audience" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .defaultValue( "https://koivuapi.koivusolutions.com" ).build( );

  private static final PropertyDescriptor PROPERTY_AUTH0_TOKEN_URL = new PropertyDescriptor.Builder( )
      .name( "authentication.auth0.url.token" ) //
      .displayName( "Token URL" ) //
      .description( "Auth0 URL to retrieve access token" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( false ) //
      .addValidator( new StringValidator( ) ) //
      .defaultValue( "https://%s.eu.auth0.com/oauth/token" ).build( );

  private static final PropertyDescriptor PROPERTY_AUTH0_TENANT = new PropertyDescriptor.Builder( )
      .name( "authentication.auth0.tenant" ) //
      .displayName( "Authentication Tenant" ) //
      .description( "Auth0 tenant name" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .addValidator( new StringValidator( ) ) //
      .required( true ).sensitive( false ).build( );

  static
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_AUTH0_TENANT );
    props.add( PROPERTY_AUTH0_TOKEN_URL );
    props.add( PROPERTY_AUTH0_AUDIENCE );
    props.add( PROPERTY_VFC_CLIENT_ID );
    props.add( PROPERTY_VFC_CLIENT_SECRET );
    KoivuVfcService.addPropertyDescriptors( props );
    propertyDescriptors = Collections.unmodifiableList( props );
  }

  @Override
  protected List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @Override
  protected AuthType getAuthenticationType( )
  {
    return AuthType.auth0;
  }

}
