package com.koivusolutions.vfc.nifi.services;

import java.util.*;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.expression.ExpressionLanguageScope;

import com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication.AuthType;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@Tags({"Service", "connection", "VFC", "Koivu.Cloud", "Koivu", "ValueFlow", "Firebase"})
@CapabilityDescription("Koivu.Cloud Connection service for Firebase.")
public class KoivuVfcServiceFirebase extends KoivuVfcService
{
  private final static List<PropertyDescriptor> propertyDescriptors;

  // Authentication
  private static final PropertyDescriptor PROPERTY_FIREBASE_API_KEY = new PropertyDescriptor.Builder( )
      .name( "authentication.firebase.apiKey" ) //
      .displayName( "API Key" ) //
      .description( "Firebase API key" ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .required( true ).sensitive( true ) //
      .addValidator( new StringValidator( ) ) //
      .build( );

  static
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_FIREBASE_API_KEY );
    props.add( PROPERTY_VFC_CLIENT_ID );
    props.add( PROPERTY_VFC_CLIENT_SECRET );
    KoivuVfcService.addPropertyDescriptors( props );
    propertyDescriptors = Collections.unmodifiableList( props );
  }

  public KoivuVfcServiceFirebase( )
  {
  }

  @Override
  protected List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @Override
  protected AuthType getAuthenticationType( )
  {
    return AuthType.firebase;
  }

}
