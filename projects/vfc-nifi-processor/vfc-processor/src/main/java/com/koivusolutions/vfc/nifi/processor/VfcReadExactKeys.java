package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadExactKeysWrapper;

@Tags({"get", "read", "full query", "exact keys", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Streams VFC data collections search by exact keys.")
public class VfcReadExactKeys extends VfcRead
{

  public VfcReadExactKeys( )
  {
    super( new VfcReadExactKeysWrapper( ) );
  }

}
