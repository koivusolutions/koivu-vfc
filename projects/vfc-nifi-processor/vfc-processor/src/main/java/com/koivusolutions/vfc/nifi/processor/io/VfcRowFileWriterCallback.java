package com.koivusolutions.vfc.nifi.processor.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.NoSuchElementException;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.OutputStreamCallback;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public class VfcRowFileWriterCallback extends FlowFileWriter implements OutputStreamCallback
{
  private final CloudTableRow row;

  public VfcRowFileWriterCallback( CloudTableRow row, final ProcessSession session,
      final FlowFile flowFile )
  {
    super( row.getCategory( ), -1, session, flowFile );
    this.row = row;
  }

  @Override
  public void process( OutputStream out ) throws IOException
  {
    try
    {
      super.open( out );
      super.write( row );
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
    finally
    {
      super.close( );
    }
  }

}
