package com.koivusolutions.vfc.nifi.services;

public class NotInitialized extends Exception
{
  public NotInitialized( String message )
  {
    super( message );
  }

  public NotInitialized( String message, Throwable cause )
  {
    super( message, cause );
  }

}
