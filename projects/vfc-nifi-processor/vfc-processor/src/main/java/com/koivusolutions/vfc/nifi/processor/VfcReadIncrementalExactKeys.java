package com.koivusolutions.vfc.nifi.processor;

import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;

import com.koivusolutions.vfc.nifi.processor.vfc.VfcReadExactKeysWrapper;

@Tags({"get", "read", "streaming query", "exact keys", "Koivu", "ValueFlow", "VFC", "Koivu.Cloud"})
@CapabilityDescription("Streams VFC data collections search by exact keys.")
public class VfcReadIncrementalExactKeys extends VfcReadIncremental
{

  public VfcReadIncrementalExactKeys( )
  {
    super( new VfcReadExactKeysWrapper( ) );
  }

}
