package com.koivusolutions.vfc.nifi.validators;

import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageCompiler;

public class StringValidator implements Validator
{

  @Override
  public ValidationResult validate( String subject, String input, ValidationContext context )
  {
    if( context.isExpressionLanguagePresent( input ) )
    {
      ExpressionLanguageCompiler elc = context.newExpressionLanguageCompiler( );
      final String msg = elc.validateExpression( input, true );
      final boolean validExpression = msg == null;
      return new ValidationResult.Builder( ).subject( subject ).input( input )
          .valid( validExpression )
          .explanation( validExpression ? ""
              : "the value is not a valid Nifi Expression. " + ( msg != null ? msg : "" ) )
          .build( );
    }

    return validate( subject, input );
  }

  protected ValidationResult validate( String subject, String input )
  {
    return new ValidationResult.Builder( ).subject( subject ).input( input ).valid( true ).build( );
  }
}
