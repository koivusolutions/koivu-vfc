package com.koivusolutions.vfc.nifi.processor.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.NoSuchElementException;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.io.OutputStreamCallback;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.nifi.processor.VfcReadProcessor;

public class VfcCloudTableFileWriterCallback extends FlowFileWriter implements OutputStreamCallback
{
  private final VfcIterator<CloudTableRow> cursor;

  public VfcCloudTableFileWriterCallback( CloudTable t, VfcIterator<CloudTableRow> cursor, int splitSize,
      final ProcessSession session, final FlowFile flowFile )
  {
    super( t.getCategory( ), splitSize, session, flowFile );
    this.cursor = cursor;
  }

  public VfcCloudTableFileWriterCallback( String category, VfcIterator<CloudTableRow> cursor,
      int splitSize, final ProcessSession session, final FlowFile flowFile )
  {
    super( category, splitSize, session, flowFile );
    this.cursor = cursor;
  }

  @Override
  public void process( OutputStream out ) throws IOException
  {
    try
    {
      super.open( out );
      boolean cont = true;
      while( cont && cursor.hasNext( ) )
      {
        CloudTableRow currentRow = cursor.next( );
        cont = super.write( currentRow );
      }
    }
    catch( NoSuchElementException e )
    {
      throw new IOException( e );
    }
    catch( VfcClientException e )
    {
      VfcReadProcessor.setVariables( session, flowFile, e.getResponseStatus( ) );
      throw new IOException( e );
    }
    finally
    {
      super.close( );
    }
  }

}
