package com.koivusolutions.vfc.nifi.processor;

import java.util.*;
import java.util.regex.Pattern;

import org.apache.nifi.annotation.behavior.DynamicProperty;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.*;
import org.apache.nifi.expression.ExpressionLanguageScope;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.nifi.processor.io.VfcCloudTableFlowfileAppenderCallback;
import com.koivusolutions.vfc.nifi.validators.SingleCharacterValidator;
import com.koivusolutions.vfc.nifi.validators.StringValidator;

@Tags({"Koivu", "ValueFlow", "VFC", "Koivu.Cloud", "Row", "Create"})
@CapabilityDescription("Create a Koivu.Cloud data row and writes that to flowfile")
@InputRequirement(InputRequirement.Requirement.INPUT_ALLOWED)
@DynamicProperty(name = "The property name for the row.", value = "Property value", description = "User-defined properties identify properties to add to the row. Dynamic values of ${uuid} and ${reverseTimeStamp} are also available", expressionLanguageScope = ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
public class VfcCreateRow extends AbstractProcessor implements VfcReadProcessor
{
  private static final String OUTPUT_REPLACE = "replace";
  private static final String OUTPUT_APPEND = "append";

  static final PropertyDescriptor PROPERTY_KEYS_SEPARATOR = new PropertyDescriptor.Builder( )
      .name( "keys.separator" ) //
      .displayName( "Key Separator" ) //
      .defaultValue( "," ) //
      .required( true ) //
      .description( "The separator character used for key values." ) //
      .addValidator( new SingleCharacterValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  static final PropertyDescriptor PROPERTY_KEYS = new PropertyDescriptor.Builder( ).name( "keys" ) //
      .displayName( "Key values" ) //
      .defaultValue( "" ) //
      .required( true ) //
      .description( "The key values separated by separator character used for key values." ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ).build( );

  static final PropertyDescriptor PROPERTY_OUTPUT = new PropertyDescriptor.Builder( )
      .name( "output" ) //
      .displayName( "Output Strategy" ) //
      .allowableValues( //
          new AllowableValue( OUTPUT_REPLACE, "Replace",
              "Replace flowfile content with a new row" ), //
          new AllowableValue( OUTPUT_APPEND, "Append", "Appends a new row to the flow file" ) ) //
      .defaultValue( OUTPUT_REPLACE ) //
      .required( true ) //
      .description(
          "Defines if flowfile is rewritten by a new row or new row is appended to the flowfile." ) //
      .addValidator( Validator.VALID ) //
      .expressionLanguageSupported( ExpressionLanguageScope.NONE ).build( );

  protected static final PropertyDescriptor PROPERTY_CLOUD_TABLE = new PropertyDescriptor.Builder( )
      .name( "Cloud Table Name" ) //
      .displayName( "Cloud Table Name" ) //
      .description( "Name of the Cloud Table" ) //
      .required( true ) //
      .addValidator( new StringValidator( ) ) //
      .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES ) //
      .build( );

  private Set<Relationship> relationships;
  private List<PropertyDescriptor> propertyDescriptors;

  public VfcCreateRow( )
  {
    super( );
  }

  @Override
  protected void init( ProcessorInitializationContext context )
  {
    List<PropertyDescriptor> props = new ArrayList<>( );
    props.add( PROPERTY_KEYS );
    props.add( PROPERTY_KEYS_SEPARATOR );
    props.add( PROPERTY_OUTPUT );
    props.add( PROPERTY_CLOUD_TABLE );
    propertyDescriptors = Collections.unmodifiableList( props );

    final Set<Relationship> rels = new HashSet<>( );
    rels.add( REL_SUCCESS );
    relationships = Collections.unmodifiableSet( rels );

    super.init( context );
  }

  @Override
  public void onTrigger( final ProcessContext context, final ProcessSession session )
      throws ProcessException
  {
    FlowFile originalFlowFile = session.get( );
    if( originalFlowFile == null )
    {
      return;
    }

    final Map<String, String> fieldVariables = new HashMap<>( );
    fieldVariables.put( "uuid", UUID.randomUUID( ).toString( ) );
    fieldVariables.put( "reverseTimeStamp",
        Long.valueOf( Long.MAX_VALUE - System.currentTimeMillis( ) ).toString( ) );

    FlowFile flowFile = originalFlowFile;
    try
    {
      CloudTableRow r = getCloudTableRow( fieldVariables, context, flowFile );
      List<CloudTableRow> rowList = new ArrayList<>( 1 );
      rowList.add( r );
      final Iterator<CloudTableRow> parentIt = rowList.iterator( );
      VfcIterator<CloudTableRow> it = new VfcIterator<CloudTableRow>( ) {

        @Override
        public VfcResponseStatus getResponseStatus( ) throws VfcClientException
        {
          return null;
        }

        @Override
        public boolean hasNext( ) throws VfcClientException
        {
          return parentIt.hasNext( );
        }

        @Override
        public CloudTableRow next( ) throws NoSuchElementException,
            VfcClientException
        {
          return parentIt.next( );
        }
      };

      String outputStrategy = context.getProperty( PROPERTY_OUTPUT )
          .evaluateAttributeExpressions( flowFile ).getValue( );

      VfcCloudTableFlowfileAppenderCallback flowFileWriter = new VfcCloudTableFlowfileAppenderCallback(
          r.getCategory( ), it, OUTPUT_APPEND.equals( outputStrategy ), session, flowFile );
      session.write( flowFile, flowFileWriter );

      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ROW_COUNT,
          String.valueOf( flowFileWriter.getCount( ) ) );
      session.adjustCounter( "VFC rows written", flowFileWriter.getCount( ), false );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_RESPONSE_COLUMNS,
          flowFileWriter.getFields( ) );
      session.getProvenanceReporter( ).modifyContent( flowFile );
      session.transfer( flowFile, REL_SUCCESS );
    }
    catch( final FlowFileAccessException t )
    {
      getLogger( ).error( "{} failed to process due to {}; rolling back session",
          new Object[]{this, t} );
      VfcReadProcessor.setSessionAttribute( session, flowFile, ATTRIBUTE_ERROR, t.getMessage( ) );
      throw new ProcessException( t );
    }
  }

  private CloudTableRow getCloudTableRow( Map<String, String> fieldVariables,
      ProcessContext context, FlowFile flowFile )
  {
    CloudTableRow row = new CloudTableRow( );
    String cloudTable = context.getProperty( PROPERTY_CLOUD_TABLE )
        .evaluateAttributeExpressions( flowFile, fieldVariables ).getValue( );
    row.setCategory( cloudTable );

    String keyStr = context.getProperty( PROPERTY_KEYS )
        .evaluateAttributeExpressions( flowFile, fieldVariables ).getValue( );
    String keySeparator = context.getProperty( PROPERTY_KEYS_SEPARATOR )
        .evaluateAttributeExpressions( flowFile ).getValue( );
    String[] keys = keyStr.split( Pattern.quote( keySeparator ), -1 );
    row.setKeys( keys );

    Set<PropertyDescriptor> props = context.getProperties( ).keySet( );
    for( PropertyDescriptor propertyDescriptor : props )
    {
      if( propertyDescriptor.isDynamic( ) )
      {
        String name = propertyDescriptor.getName( );
        String value = context.getProperty( name )
            .evaluateAttributeExpressions( flowFile, fieldVariables ).getValue( );
        row.addProperty( name, value );
      }
    }

    return row;
  }

  @Override
  protected PropertyDescriptor getSupportedDynamicPropertyDescriptor(
      final String propertyDescriptorName )
  {
    return new PropertyDescriptor.Builder( ).name( propertyDescriptorName )
        .description( "Properties to add to the row. Use" ).dynamic( true ).required( false )
        .expressionLanguageSupported( ExpressionLanguageScope.FLOWFILE_ATTRIBUTES )
        .addValidator( new StringValidator( ) ).build( );
  }

  @Override
  public Set<Relationship> getRelationships( )
  {
    return relationships;
  }

  @Override
  public List<PropertyDescriptor> getSupportedPropertyDescriptors( )
  {
    return propertyDescriptors;
  }

  @OnScheduled
  public void initSchedule( ProcessContext context )
  {
  }

  @OnStopped
  public void close( )
  {
  }

}
