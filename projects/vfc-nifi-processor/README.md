# Koivu.Cloud

## Koivu.Cloud NiFi Processors

This NiFi extension adds a Koivu.Cloud client processor and a controller service extension to Apache NiFi. Provided features are:

* **_Koivu.Cloud controller service_**
  * Authentication (Auth0, Firebase)
  * Client to Server communication batching
  * Client to Server communication compression

* **_Koivu.Cloud Processors_**
  * **Retrieving data with**
    * keys
    * exact keys
    * keys starting with
    * key range
  * **Retrieving data *incrementally since last retrieval* with**
    * keys
    * exact keys
    * keys starting with
    * key range
  * **Inserting data**
  * **Updating data**
  * **Deleting data**
  * **Replacing data**
  * **Creating data rows**
  * **Data format conversions**
    * from Koivu.Cloud JSON format to CSV format
    * from CSV format to Koivu.Cloud JSON format

### Creating a connection to Koivu.Cloud

#### Prerequisites

* Basic knowledge about Apache NiFi. Required NiFi version 1.13.0 or newer.
* Basic knowledge about Koivu.Cloud
* Installation
  * Go to <https://mvnrepository.com/artifact/com.koivusolutions.vfc/vfc-nifi-processor>
  * Click the newest version
  * Download the ``nar`` file (for example, ``vfc-nifi-processor-1.4.20210102.nar``)
  * Copy downloaded nar file to ``<nifi_installdir>/extensions``
  * Start NiFi
* You will also need a running ``Koivu.Cloud instance``
  * URL to Koivu.Cloud server instance
  * Koivu.Cloud solution id
* And authentication information from ``Auth0``
  * Authentication tenant
  * Client ID
  * Client Secret

#### Finding Koivu.Cloud processors when working in NiFi

In the **Add Processor** window, to list only the Koivu.Cloud processors while adding a new processor to the NiFi canvas, select ``com.koivu.vfc.nifi`` from the **Source** list, or use tags, such as ``vfc`` or ``Koivu.Cloud`` to filter the displayed processors.

#### Configuring processors

When configuring a processor, you can use the NiFi expression language to evaluate values for the properties. For more information, see <https://nifi.apache.org/docs/nifi-docs/html/expression-language-guide.html>.

#### Creating a client connection

When starting a NiFi flow you should use incremental read processors (e.g. VFCRead**Incremental**...). All other Koivu.Cloud processors are expected to be used in the middle of the NiFi flow.

Processors that require a connection to a Koivu.Cloud instance have the ``Koivu.Cloud Service`` property, which defines the Koivu.Cloud instance to be used for the data reads and writes. You need to select a one that is already configured or create a new service of type ``KoivuVfcService``.

##### Configuring Koivu.Cloud service

There are two Koivu.Cloud services at the moment:

* ``KoivuVfcServiceFirebase`` - does authentication on Firebase and connects to a Koivu.Cloud instance.
* ``KoivuVfcServiceAuth0`` - does authentication on Auth0 and connects to a Koivu.Cloud instance.

You can create as many Koivu.Cloud service instances as required, and multiple processors can share the same service instance.

Important properties:

* ``Koivu.Cloud URL`` defines a URL for the Koivu.Cloud instance where connection is made
* ``Client ID``, ``Client Secret`` are authentication information that you can get from the Integrations page of the Koivu.Cloud Web application. When working with Auth0 you can find the same information together with ``Authentication Tenant`` and ``Audience`` from Auth0 by clicking 'Applications' from the Dashboard menu and selecting a dedicated 'Machine to Machine' application. All values can be easily copied when you click the name of the application and open the 'Settings' view. **Domain** from Auth0 is divided into two properties: ``Authentication Tenant`` and ``Token URL``.
* ``HTTP Client ID`` identifies the HTTP traffic made from this service. Change it to something that you will recognize later when inspecting the HTTP access logs from the Koivu.Cloud server.
* ``Tenant``, ``Solution``, ``Branch`` and ``Domain`` are your implementation specific values. Usually you just set the correct tenant and solution or tenant, solution and branch in some cases.
* ``Batch Size`` defines how many row at most is read or written to/from the Koivu.Cloud server. This comes into play when data amouts are high. Retrieving data in smaller batches allows NiFi to start working on data earlier and at the same time as more data is retrieved from the Koivu.Cloud instance. Also, writing a huge amount of data in smaller batches makes it more reliable when sent over HTTP. If you set this value to negative or zero, then all data is always handled as one batch. Usually it does not make sense to use batch sizes smaller than 20 000, but it depends on your environment.
* ``Connection Pool Size`` defines how many parallel connections the Koivu.Cloud service makes to the Koivu.Cloud server. All processors that use this service share this pool. If the amount of concurrent reads or writes is higher than the pool size, the reads or writes that go over the limit will queue/wait for the previous to complete.

There are also other properties that mainly set HTTP traffic time outs and proxy servers. For more information, see the documention for each property in NiFi.

##### Read / Write balancing

Create one Koivu.Cloud service for read operations with the ``Connection Pool Size`` property. Use this Koivu.Cloud service instance for all of your read processors. Create another Koivu.Cloud service for write operations with the ``Connection Pool Size`` property. Use this Koivu.Cloud service instance for all of your write processors.

##### Transferring data from one Koivu.Cloud instance to another

Create two Koivu.Cloud services that each connect to a different Koivu.Cloud instance, and use these services to read data from one Koivu.Cloud instance and write to another.
The same strategy can be used to move data between different Koivu.Cloud solutions inside the same Koivu.Cloud instance.

#### Reading Data from Koivu.Cloud server

Processors that read data from a Koivu.Cloud server require the ``Cloud Table Name`` property to be set. Depending on the processor, you also need to define search keys. The keys are given as a string with the ``Key Array``,``Key Starting With Array`` or ``Key Range From Array`` and ``Key Range To Array`` properties, depending on the processor. Key arrays are character separated strings. The separating character is specified with the ``Key Array Separator`` property. For example, if the ``Key Array Separator`` property is a | character, then a ``Key Array`` consisting of first|second|third is interpreted as a key array with values first, second and third. When you use key ranges, constant strings ``_FIRST`` and ``_LAST`` can be used in place of a key to mark the first possible key and the last possible key. For example, when the ``Key Range From Array`` equals first|second|_FIRST and the ``Key Range To Array`` equals first|second|_LAST the system returns all rows where key 1 equals 'first' and key 2 equals 'second' and key 3 can be anything. Note that the previous is different from ``Key Starting With Array`` with values first|second, because this would also match cases where key 2 is second_and_something_more!

``Filter`` and ``Expressions`` properties are Koivu.Cloud standard features where a filter further filters a result set returned by key matching. Expressions create dynamic columns during the data read time. Both values must be in the JEXL script syntax.

``Include deleted rows`` is used if you need to read rows from the Koivu.Cloud server that have already been deleted. By specifying this property as true, Koivu.Cloud server also returns rows that have already been deleted. The deleted rows have their own ``deleted`` field set to true, which separates them from the existing rows.

``Impersonate to business`` allows a signed in user to impersonate a user that belongs to another business than their assigned business. This requires that specific roles and access rights are set in your solution. To use this feature, set the impersonated target business id in this property.  

``Result Split Size`` splits read data into multiple FlowFiles. The maximum number of rows in each file is given in this property. This allows further row by row processing in NiFi, if required. Also, it is easier to handle large datasets because the system can begin to process FlowFiles while more data is read from Koivu.Cloud and FlowFiles are written. Additionally, this reduces memory pressure when processing extremely large datasets because the data handling is subdivided into smaller sized datasets.

Koivu.Cloud returns data in JSON format. The format of the JSON is listed at the end of this document.

##### Koivu.Cloud incremental processors state

Koivu.Cloud incremental processors remember their state and only retrieve data that has been added/changed since the last data retrieval.
In case you need to reset the state, stop the processor, right-click it and select 'View state' and then click 'Clear state' in the Component State dialog.

Incremental read processors have a ``Catch Up Hours`` property, which is used when the processor has not yet done its first read operation and has no state saved. The value of this property is the number of passed hours that the first query is limited to retrieve, instead of retrieving all possible rows.

#### Writing Data to Koivu.Cloud server

You can write data with the ``VfcInsert``,``VfcUpdate``, ``VfcReplace``, ``VfcPatch``, ``VfcPatchReplace`` and ``VfcDelete`` processors. These processors expect data in the flow file to be in Koivu.Cloud's JSON format. The format of JSON can be seen at the end of this document. ``VfcInsert`` checks that no previous data exists with the same keys. ``VfcUpdate`` checks that data really exists before editing it. If the previous state of the data does not matter, you should use ``VfcReplace`` which does not perform any checks for existing data states and is also the quickest to execute. The difference between ``VfcPatch`` and ``VfcPatchReplace`` is that ``VfcPatch`` expects a row with the given keys already to exist while ``VfcPatchReplace`` inserts a row, if it does not exist.

Writer processors have 5 different out relationships:

* ``success`` All successfully written rows are routed to this relationship.
* ``rejected`` All rows that cannot be written are routed to this relationship with rejection reason added to a row in the JSON file. This FlowFile can be re-routed back to any write processors.
* ``failure`` Operation failed because of an error. Example: no access rights to write into a specific Cloud table.
* ``not found`` No content in the FlowFile that could be written to the Koivu.Cloud server.
* ``original`` Original input passthrough.

#### Generating Koivu.Cloud data in NiFi FlowFile

You can use ``VfcCreateRow`` to form data rows for Koivu.Cloud on the fly. The generated rows need to be written normally to Koivu.Cloud by using the ``VfcInsert``,``VfcUpdate``, ``VfcReplace``, ``VfcPatch``, ``VfcPatchReplace`` or ``VfcDelete`` processors.

#### Transforming Koivu.Cloud data to and from CSV format

You can use ``VfcJsonToCsv`` and ``CsvToVfcJson`` processors to convert Koivu.Cloud data from and to CSV format.

Important properties:

* ``CSV Header`` defines whether the CSV header line is written when writing the CSV. During the process the column headers are read from the CSV file. If you define that the CSV does not have a header, the column headings are replaced with numbers starting from 1, i.e. "1", "2", "3", and so on.
* ``CSV field separator`` defines the CSV field separator during the read and write operations.
* ``Key Separator`` defines how nested fields in the KoivuCloud JSON are flattened to CSV column headers.
  * For example, if the value is \_ then Koivu.Cloud key fields are written as keys_key1, keys_key2, and so on. If the value is . then Koivu.Cloud key fields are written as keys.key1, keys.key2, and so on. The same applies to all nested fields, such as properties and expressions.
  * This is also used when converting a flat CSV into a Koivu.Cloud JSON. If the value is . then CSV columns keys.key1, keys.key2 are converted into a key array in the Koivu.Cloud JSON. If the value is \_ then CSV columns keys_key1, keys_key2 are converted into a key array in the Koivu.Cloud JSON. Also, the same applies to all nested fields, such as properties and expressions.

#### Mapping Koivu.Cloud data fields to and from CSV columns

You can add dynamic properties to the ``VfcJsonToCsv`` and ``CsvToVfcJson`` processors. By adding dynamic properties you can do name mapping from CSV columns to Koivu.Cloud JSON fields, and vice versa.

* Example mapping in ``VfcJsonToCsv`` direction:
  * Property _'Animal'_ Value:_'properties.cat'_ writes Koivu.Cloud property _'cat'_ to a CSV column _'Animal'_.

* Example mapping in ``CsvToVfcJson`` direction:
  * Property _'properties.car'_ Value:_'vehicle'_ writes CSV column  _'vehicle'_ to a Koivu.Cloud property _'car'_.

You can add as many name mappings as required to these processors.
``Output Fields`` property defines whether the data conversion uses only the name mapped fields in the defined order. Another option is to allow all fields to be part of the data conversion, and the mapping is applied only to fields that have a name mapping. Other fields go through the normal field renaming process.

When you create a Koivu.Cloud JSON format from a CSV file, the Cloud Table name needs to be defined either as a _'category'_ column in the CSV directly or through name mapping. If it is not defined in the CSV or it is required to be overridden, this can be done with the ``Cloud Table Name`` property in the ``CsvToVfcJson`` processor.  

### Appendix: Koivu.Cloud JSON format as AVRO schema definition

* Note 1! Fields in ``properties`` depend on properties in the Koivu.Cloud Cloud Table in question.
* Note 2! Fields in ``expressions`` depend on read expressions defined in column definitions or expressions given when doing the data query.
* Note 3! Length of ``keys`` array depends on Koivu.Cloud Cloud Table in question.

```json
{
  "name": "VFC schema",
  "type": "record",
  "namespace": "com.koivu.vfc.avro",
  "fields": [
    {
      "name": "keys",
      "type": {
        "type": "array",
        "items": "string"
      }
    },
    {
      "name": "category",
      "type": "string"
    },
    {
      "name": "properties",
      "type": {
        "name": "properties",
        "type": "record",
        "fields": [
          {
            "name": "property_1",
            "type": "string"
          },
          {
            "name": "property_2",
            "type": "string"
          },
          {
            "name": "property_n",
            "type": "string"
          }
        ]
      }
    },
    {
      "name": "expressions",
      "type": {
        "name": "expressions",
        "type": "record",
        "fields": [
          {
            "name": "expression_1",
            "type": "string"
          },
          {
            "name": "expression_2",
            "type": "string"
          },
          {
            "name": "expression_n",
            "type": "string"
          }
        ]
      }
    },
   {
      "name": "lastModifiedTime",
      "type": "string",
      "logicalType": "date"
    },
    {
      "name": "lastModifiedBy",
      "type": "string"
    }
  ]
}
```
