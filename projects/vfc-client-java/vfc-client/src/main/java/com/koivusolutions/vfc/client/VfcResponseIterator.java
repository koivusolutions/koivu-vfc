package com.koivusolutions.vfc.client;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.http.HttpResponseExceptionWithEntity;

public class VfcResponseIterator<T extends VfcObject> implements VfcIterator<VfcRowUpdateStatus<T>>
{
  private final LinkedList<Pair<List<? extends VfcRowUpdateStatus<T>>, Future<JSONObject>>> responses;
  private final boolean all;
  private Iterator<? extends VfcRowUpdateStatus<T>> currentStatuses = null;
  private VfcResponseStatus responseStatus = null;

  public VfcResponseIterator(
      LinkedList<Pair<List<? extends VfcRowUpdateStatus<T>>, Future<JSONObject>>> responses,
      boolean all )
  {
    this.all = all;
    this.responses = responses;
  }

  @Override
  public boolean hasNext( ) throws VfcClientException
  {
    if( currentStatuses == null || currentStatuses.hasNext( ) == false )
    {
      readNextBatch( );
    }
    if( currentStatuses != null )
    {
      return currentStatuses.hasNext( );
    }
    return false;
  }

  @Override
  public VfcRowUpdateStatus<T> next( ) throws VfcClientException
  {
    if( hasNext( ) == true )
    {
      return currentStatuses.next( );
    }
    throw new NoSuchElementException( );
  }

  @Override
  public VfcResponseStatus getResponseStatus( ) throws VfcClientException
  {
    if( responseStatus != null || hasNext( ) )
    {
      return responseStatus;
    }
    return new VfcResponseStatus( );
  }

  private void readNextBatch( ) throws VfcClientException
  {
    synchronized ( responses )
    {
      if( responses.size( ) > 0 )
      {
        Pair<List<? extends VfcRowUpdateStatus<T>>, Future<JSONObject>> batch = responses
            .getFirst( );
        List<? extends VfcRowUpdateStatus<T>> statuses = batch.getLeft( );
        Future<JSONObject> response = batch.getRight( );
        if( response != null )
        {
          if( !all && !response.isDone( ) )
          {
            // Do not wait for task to finish
            return;
          }
          responses.removeFirst( );
          parse( statuses, response );
          currentStatuses = statuses.iterator( );
        }
      }
    }
  }

  private void parse( List<? extends VfcRowUpdateStatus<T>> statuses, Future<JSONObject> future )
      throws VfcClientException
  {
    try
    {
      JSONObject serverResponse = future.get( );
      responseStatus = new VfcResponseStatus( serverResponse );
      Iterator<? extends VfcRowUpdateStatus<T>> rowStatus = statuses.iterator( );
      JSONArray rowsArray = serverResponse.getJSONArray( "data" );
      if( rowsArray != null )
      {
        for( int i = 0; i < rowsArray.length( ); i++ )
        {
          rowStatus.next( ).setStatus( rowsArray.getJSONObject( i ) );
        }
      }
    }
    catch( InterruptedException | ExecutionException e )
    {
      Throwable cause = e.getCause( );
      JSONObject responseEntity = null;
      if( cause instanceof HttpResponseExceptionWithEntity )
      {
        HttpResponseExceptionWithEntity httpException = (HttpResponseExceptionWithEntity)cause;
        responseEntity = httpException.getResponseEntity( );
        responseStatus = new VfcResponseStatus( responseEntity );
      }
      throw new VfcClientException( "Data update error:" + e.getMessage( ), cause, responseEntity );
    }
  }

  @Override
  public String toString( )
  {
    return "VfcResponseIterator [all=" + all + ", currentStatuses=" + currentStatuses
        + ", responseStatus=" + responseStatus + "]";
  }
}
