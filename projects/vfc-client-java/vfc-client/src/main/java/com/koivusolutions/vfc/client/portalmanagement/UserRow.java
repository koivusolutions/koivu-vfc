package com.koivusolutions.vfc.client.portalmanagement;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class UserRow extends ImmutableUserRow implements VfcObject
{
  public UserRow( )
  {
    super( );
  }

  public UserRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setApproved( boolean approved )
  {
    setFieldAsBoolean( SYSTEM_APPROVED, approved );
  }

  public void setExternal( boolean external )
  {
    setFieldAsBoolean( SYSTEM_EXTERNAL, external );
  }

  public void setAssignedBusiness( String asignedBusinessId )
  {
    setFieldAsString( SYSTEM_ASSIGNED_BUSINESS, asignedBusinessId );
  }

  public void setRoleGroup( String roleGroupId )
  {
    setFieldAsString( SYSTEM_ROLE_GROUP, roleGroupId );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_APPROVED, column ) )
      {
        setApproved( Boolean.valueOf( newValue ).booleanValue( ) );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_EXTERNAL, column ) )
      {
        setExternal( Boolean.valueOf( newValue ).booleanValue( ) );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_ASSIGNED_BUSINESS, column ) )
      {
        setAssignedBusiness( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_ROLE_GROUP, column ) )
      {
        setRoleGroup( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableUserRow asImmutableUserRow( )
  {
    return this;
  }
}
