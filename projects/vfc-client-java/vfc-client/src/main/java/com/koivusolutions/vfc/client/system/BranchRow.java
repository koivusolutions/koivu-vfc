package com.koivusolutions.vfc.client.system;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class BranchRow extends ImmutableBranchRow implements VfcObject
{
  public BranchRow( )
  {
    super( );
  }

  public BranchRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setId( String id )
  {
    super.setId( id );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setName( String name )
  {
    setFieldAsString( SYSTEM_NAME, name );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_NAME, column ) )
      {
        setName( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableBranchRow asImmutableBranchRow( )
  {
    return this;
  }
}
