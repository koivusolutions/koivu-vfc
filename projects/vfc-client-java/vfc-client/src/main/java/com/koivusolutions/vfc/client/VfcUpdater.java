package com.koivusolutions.vfc.client;

public interface VfcUpdater<T extends VfcObject>
{

  VfcUpdater<T> addUpdate( T row );

  VfcUpdater<T> flush( );

  VfcIterator<VfcRowUpdateStatus<T>> getResponses( );

  VfcIterator<VfcRowUpdateStatus<T>> getAllResponses( );

  void checkErrors( ) throws VfcClientException;

  void checkAllErrors( ) throws VfcClientException;

  void setSkipIdentical( boolean skipIdentical );
}