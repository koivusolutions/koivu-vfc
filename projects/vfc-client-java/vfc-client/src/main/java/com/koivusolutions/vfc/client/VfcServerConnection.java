package com.koivusolutions.vfc.client;

import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.math.NumberUtils;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.http.HttpClient;

public class VfcServerConnection
{

  private final ExecutorService executor;
  private final HttpClient httpClient;
  private final String vfcURL;
  private final String versionURL;
  private final int batchsize;

  public VfcServerConnection( URL vfcURL, VfcProperties vfcProperties )
  {
    this.httpClient = new HttpClient( vfcProperties );
    String url = vfcURL.toString( );
    this.vfcURL = url.endsWith( "/" ) ? url : ( url + "/" );
    this.versionURL = vfcProperties.getProperty( "vfc.api.version", "valueflow/v1/" );
    this.batchsize = NumberUtils.toInt( vfcProperties.getProperty( "vfc.api.batchsize" ), 2048 );
    this.executor = Executors.newFixedThreadPool(
        vfcProperties.getPropertyAsInt( "http.connection.poolsize", 20 ),
        new JavaThreadFactory( ) );
  }

  ExecutorService getExecutor( )
  {
    return executor;
  }

  HttpClient getHttpClient( )
  {
    return httpClient;
  }

  String getVfcURL( )
  {
    return vfcURL;
  }

  String getVersionURL( )
  {
    return versionURL;
  }

  int getBatchSize( )
  {
    return batchsize;
  }

  URI getResourceURI( String resourceURL ) throws URISyntaxException
  {
    return new URI( getVfcURL( ) + getVersionURL( ) + resourceURL );
  }

  public void close( )
  {
    executor.shutdown( );
  }

}