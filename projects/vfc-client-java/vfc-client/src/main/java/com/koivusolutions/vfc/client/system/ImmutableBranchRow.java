package com.koivusolutions.vfc.client.system;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.IdObject;

public class ImmutableBranchRow extends IdObject
{
  public static final String SYSTEM_NAME = "name";

  public ImmutableBranchRow( )
  {
    super( new JSONObject( ) );
  }

  public ImmutableBranchRow( JSONObject obj )
  {
    super( obj );
  }

  public String getName( )
  {
    return getFieldAsString( SYSTEM_NAME );
  }

}
