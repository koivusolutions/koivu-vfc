package com.koivusolutions.vfc.client.cloudtable;

import com.koivusolutions.vfc.client.VfcClient;

public class CloudTables extends CloudTablesBase
{

  public CloudTables( VfcClient vfcClient )
  {
    super( vfcClient, "CloudTable", "properties" );
  }
}
