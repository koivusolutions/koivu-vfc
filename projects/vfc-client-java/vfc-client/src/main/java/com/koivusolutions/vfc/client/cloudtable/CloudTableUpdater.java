package com.koivusolutions.vfc.client.cloudtable;

import com.koivusolutions.vfc.client.*;

public class CloudTableUpdater<T extends VfcObject> extends VfcUpdateResponse<T>
{
  private final String URL;

  public CloudTableUpdater( VfcClient vfcClient, UpdateStrategy updateStrategy, String URL )
  {
    super( vfcClient, updateStrategy );
    this.URL = URL;
  }

  @Override
  public VfcUpdater<T> addUpdate( T row )
  {
    return super.addUpdate( row );
  }

  @Override
  protected String getUrl( )
  {
    return URL;
  }

}
