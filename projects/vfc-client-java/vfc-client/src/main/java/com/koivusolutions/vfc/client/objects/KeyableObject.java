package com.koivusolutions.vfc.client.objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class KeyableObject extends BaseObject
{
  public static final String COLUMN_TYPE_KEYS = "keys";
  public static final String KEY_COLUMN_PREFIX = "key";

  public KeyableObject( JSONObject obj )
  {
    super( obj );
  }

  public String[] getKeys( )
  {
    JSONArray keysJSON = getAsJson( ).optJSONArray( COLUMN_TYPE_KEYS );
    if( keysJSON != null )
    {
      String[] keysArray = new String[keysJSON.length( )];
      for( int i = 0; i < keysArray.length; i++ )
      {
        keysArray[i] = keysJSON.optString( i );
      }
      return keysArray;
    }
    return null;
  }

  protected void setKeys( String... keys )
  {
    JSONArray keysJSON = new JSONArray( keys );
    setFieldAsJsonObject( COLUMN_TYPE_KEYS, keysJSON );
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_KEYS, columnType ) )
    {
      if( column.startsWith( KEY_COLUMN_PREFIX ) )
      {
        int keyIndex = NumberUtils.toInt( column.substring( KEY_COLUMN_PREFIX.length( ) ), -1 );
        String[] keys = getKeys( );
        if( keys != null && keyIndex > 0 && keyIndex <= keys.length )
        {
          return keys[keyIndex - 1];
        }
      }
      return defaultValue;
    }
    return super.getAsString( columnType, column, defaultValue );
  }

  @Override
  protected void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_KEYS, columnType ) )
    {
      if( column.startsWith( KEY_COLUMN_PREFIX ) )
      {
        int keyIndex = NumberUtils.toInt( column.substring( KEY_COLUMN_PREFIX.length( ) ), -1 );
        String[] keys = getKeys( );
        if( keys != null && keyIndex > 0 && keyIndex <= keys.length )
        {
          keys[keyIndex] = newValue;
          setKeys( keys );
        }
      }
      return;
    }
    super.setAsString( columnType, column, newValue );
  }
}