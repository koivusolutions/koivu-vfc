package com.koivusolutions.vfc.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.codec.Charsets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.authentication.Authentication;
import com.koivusolutions.vfc.client.VfcClientException;

public class HttpClient
{
  private final PoolingHttpClientConnectionManager cm;
  private final RequestConfig requestConfig;
  private final String vfcClientId;
  private final HttpHost proxy;
  private final int gzipLimit;

  public HttpClient( VfcProperties vfcProperties )
  {
    cm = new PoolingHttpClientConnectionManager( );
    cm.setMaxTotal( vfcProperties.getPropertyAsInt( "http.connection.poolsize", 20 ) );
    cm.setDefaultMaxPerRoute( vfcProperties.getPropertyAsInt( "http.connection.poolsize", 20 ) );
    cm.setValidateAfterInactivity(
        vfcProperties.getPropertyAsInt( "http.connection.validateafterinactivity", 5000 ) );

    requestConfig = RequestConfig.custom( )
        .setSocketTimeout( vfcProperties.getPropertyAsInt( "http.connection.sockettimeout", 5000 ) )
        .setConnectTimeout(
            vfcProperties.getPropertyAsInt( "http.connection.connectiontimeout", 5000 ) )
        .setConnectionRequestTimeout(
            vfcProperties.getPropertyAsInt( "http.connection.connectionrequesttimeout", 5000 ) )
        .build( );

    vfcClientId = vfcProperties.getProperty( "vfc.client.id", "Koivu Value Flow Client" );

    String proxyHost = vfcProperties.getProperty( "http.proxy.host" );
    int proxyPort = vfcProperties.getPropertyAsInt( "http.proxy.port", 0 );
    String proxyScheme = vfcProperties.getProperty( "http.proxy.scheme" );
    if( StringUtils.isNotBlank( proxyHost ) && proxyPort > 0
        && StringUtils.isNotBlank( proxyScheme ) )
    {
      proxy = new HttpHost( proxyHost, proxyPort, proxyScheme );
    }
    else
    {
      proxy = null;
    }

    gzipLimit = vfcProperties.getPropertyAsInt( "http.gzip.limit", 1024 );
  }

  private CloseableHttpClient getClient( )
  {
    return HttpClients.custom( ) //
        .setConnectionManager( cm ) //
        .setDefaultRequestConfig( requestConfig ) //
        .setProxy( proxy ) //
        .setRetryHandler( new DefaultHttpRequestRetryHandler( 3, true ) ) //
        .build( );
  }

  public JSONObject postJson( Authentication auth, URI query,
      List<Pair<String, String>> queryParams, JSONObject postParams ) throws ParseException,
      IOException,
      VfcClientException,
      URISyntaxException,
      HttpResponseExceptionWithEntity
  {
    HttpPost postRequest = new HttpPost( );
    buildRequest( postRequest, auth, query, queryParams );
    HttpEntity entity = createJsonEntity( query, postParams, true );
    postRequest.setEntity( entity );
    return processResponse( postRequest );
  }

  public JSONObject putJson( Authentication auth, URI query, List<Pair<String, String>> queryParams,
      JSONObject putParams ) throws ParseException,
      IOException,
      VfcClientException,
      URISyntaxException,
      HttpResponseExceptionWithEntity
  {
    HttpPut putRequest = new HttpPut( );
    buildRequest( putRequest, auth, query, queryParams );
    HttpEntity entity = createJsonEntity( query, putParams, true );
    putRequest.setEntity( entity );
    return processResponse( putRequest );
  }

  public JSONObject patchJson( Authentication auth, URI query,
      List<Pair<String, String>> queryParams, JSONObject putParams ) throws ParseException,
      IOException,
      VfcClientException,
      URISyntaxException,
      HttpResponseExceptionWithEntity
  {
    HttpPatch putRequest = new HttpPatch( );
    buildRequest( putRequest, auth, query, queryParams );
    HttpEntity entity = createJsonEntity( query, putParams, true );
    putRequest.setEntity( entity );
    return processResponse( putRequest );
  }

  public JSONObject deleteJson( Authentication auth, URI query,
      List<Pair<String, String>> queryParams, JSONObject deleteParams ) throws ParseException,
      IOException,
      VfcClientException,
      URISyntaxException
  {
    HttpDeleteWithBody deleteRequest = new HttpDeleteWithBody( );
    buildRequest( deleteRequest, auth, query, queryParams );
    HttpEntity entity = createJsonEntity( query, deleteParams, false );
    deleteRequest.setEntity( entity );
    return processResponse( deleteRequest );
  }

  public JSONObject getJson( Authentication auth, URI query,
      List<Pair<String, String>> queryParams ) throws ParseException,
      IOException,
      URISyntaxException,
      VfcClientException,
      HttpResponseExceptionWithEntity
  {
    HttpGet getRequest = new HttpGet( );
    buildRequest( getRequest, auth, query, queryParams );
    return processResponse( getRequest );
  }

  private HttpEntity createJsonEntity( URI query, JSONObject jsonParam, boolean allowZip )
  {
    String contextAsString = jsonParam != null ? jsonParam.toString( ) : "";
    EntityBuilder entityBuilder = EntityBuilder.create( ).setText( contextAsString )
        .setContentType( ContentType.APPLICATION_JSON );
    boolean isLocalHost = StringUtils.equalsAnyIgnoreCase( query.getHost( ), "localhost",
        "127.0.0.1" );
    if( !isLocalHost && allowZip && contextAsString.length( ) > gzipLimit )
    {
      entityBuilder.gzipCompress( );
    }
    return entityBuilder.build( );
  }

  private void buildRequest( HttpRequestBase request, Authentication auth, URI query,
      List<Pair<String, String>> queryParams ) throws ParseException,
      URISyntaxException,
      VfcClientException
  {
    URIBuilder uriBuilder = new URIBuilder( query );
    if( queryParams != null )
    {
      for( Pair<String, String> queryParam : queryParams )
      {
        uriBuilder.addParameter( queryParam.getLeft( ), queryParam.getRight( ) );
      }
    }
    request.setURI( uriBuilder.build( ) );
    if( auth != null )
    {
      request.setHeader( "authorization", auth.getTokenType( ) + " " + auth.getAccessToken( ) );
    }
    request.setHeader( "User-Agent", vfcClientId + ", gzip" );
  }

  private JSONObject processResponse( HttpRequestBase getRequest )
      throws HttpResponseExceptionWithEntity,
      IOException,
      ClientProtocolException
  {
    CloseableHttpClient client = getClient( );
    {
      try (CloseableHttpResponse response = client.execute( getRequest ))
      {
        StatusLine status = response.getStatusLine( );
        HttpEntity responseEntity = response.getEntity( );
        Header encodingHeader = responseEntity.getContentEncoding( );

        Charset encoding = encodingHeader == null ? StandardCharsets.UTF_8
            : Charsets.toCharset( encodingHeader.getValue( ) );

        int statusCode = status.getStatusCode( );
        if( statusCode == HttpStatus.SC_UNAUTHORIZED )
        {
          throw new HttpResponseExceptionWithEntity( status.getStatusCode( ),
              status.getReasonPhrase( ), null );
        }

        String json = EntityUtils.toString( responseEntity, encoding );
        if( StringUtils.isBlank( json ) )
        {
          throw new HttpResponseContentException( status.getStatusCode( ),
              status.getReasonPhrase( ), buildErrorResponse( "No content" ) );
        }
        if( !responseEntity.getContentType( ).getValue( )
            .contains( ContentType.APPLICATION_JSON.getMimeType( ) ) )
        {
          throw new HttpResponseContentException( status.getStatusCode( ),
              status.getReasonPhrase( ), buildErrorResponse(
                  "Not JSON content: " + response.toString( ) + " content:" + json ) );
        }
        if( statusCode != HttpStatus.SC_OK )
        {
          throw new HttpResponseExceptionWithEntity( status.getStatusCode( ),
              status.getReasonPhrase( ), new JSONObject( json ) );
        }
        return new JSONObject( json );
      }
    }
  }

  private JSONObject buildErrorResponse( String errorMsg )
  {
    JSONObject response = new JSONObject( );
    JSONObject error = new JSONObject( );
    error.put( "errorMessage", StringUtils.abbreviate( errorMsg, 256 ) );
    response.put( "error", error );
    return response;
  }

  private class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase
  {
    public static final String METHOD_NAME = "DELETE";

    @Override
    public String getMethod( )
    {
      return METHOD_NAME;
    }

    public HttpDeleteWithBody( )
    {
      super( );
    }
  }

}
