package com.koivusolutions.vfc.client.integrations;

public class PropertyKey
{
  private final String id;
  private final String property;

  public PropertyKey( String id, String property )
  {
    super( );
    this.id = id;
    this.property = property;
  }

  public PropertyKey( String[] keys )
  {
    super( );
    this.id = keys[0];
    this.property = keys[1];
  }

  public String getId( )
  {
    return id;
  }

  public String getProperty( )
  {
    return property;
  }

  @Override
  public int hashCode( )
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ( ( id == null ) ? 0 : id.hashCode( ) );
    result = prime * result + ( ( property == null ) ? 0 : property.hashCode( ) );
    return result;
  }

  @Override
  public boolean equals( Object obj )
  {
    if( this == obj )
    {
      return true;
    }
    if( obj == null )
    {
      return false;
    }
    if( getClass( ) != obj.getClass( ) )
    {
      return false;
    }
    PropertyKey other = (PropertyKey)obj;
    if( id == null )
    {
      if( other.id != null )
      {
        return false;
      }
    }
    else if( !id.equals( other.id ) )
    {
      return false;
    }
    if( property == null )
    {
      if( other.property != null )
      {
        return false;
      }
    }
    else if( !property.equals( other.property ) )
    {
      return false;
    }
    return true;
  }

}
