package com.koivusolutions.vfc.client.system;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcObject;

public abstract class SystemTableWithFilter<T extends VfcObject> extends SystemTable<T>
{

  public SystemTableWithFilter( VfcClient vfcClient, String URL )
  {
    super( vfcClient, URL );
  }

  public SystemTableRows<T> getWithFilter( String filter )
  {
    List<Pair<String, String>> queryParams = addQueryParams( null, null, filter, null );
    return new SystemTableRows<T>( this, vfcClient, queryParams );
  }
}
