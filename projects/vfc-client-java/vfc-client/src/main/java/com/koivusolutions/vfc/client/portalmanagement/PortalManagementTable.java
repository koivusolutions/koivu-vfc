package com.koivusolutions.vfc.client.portalmanagement;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;

public abstract class PortalManagementTable<T extends VfcObject> implements VfcTableUpdates<T>
{
  private final VfcClient vfcClient;
  private final String URL;
  
  public PortalManagementTable( VfcClient vfcClient, String URL)
  {
    this.vfcClient = vfcClient;
    this.URL=URL;
  }

  protected String getUrl( )
  {
    return URL;
  }

  abstract protected T build( JSONObject obj );

  public PortalManagementRows<T> getStartingWith( String startswith )
  {
    return getStartingWith( startswith, null );
  }

  public PortalManagementRows<T> getStartingWith( String[] startswith )
  {
    return getStartingWith( startswith, null );
  }

  public PortalManagementRows<T> getStartingWith( String startswith, String filter )
  {
    List<Pair<String, String>> queryParams = addQueryParams( new String[]{startswith}, filter );
    return new PortalManagementRows<T>( this, vfcClient, queryParams );
  }

  public PortalManagementRows<T> getStartingWith( String startswith[], String filter )
  {
    List<Pair<String, String>> queryParams = addQueryParams( startswith, filter );
    return new PortalManagementRows<T>( this, vfcClient, queryParams );
  }

  private void addParam( List<Pair<String, String>> queryParams, String name, String value )
  {
    if( StringUtils.isNotBlank( value ) )
    {
      queryParams.add( new ImmutablePair<String, String>( name, value ) );
    }
  }

  @Override
  public PortalManagementUpdater<T> deleteFrom( )
  {
    return new PortalManagementUpdater<T>( vfcClient, URL, UpdateStrategy.DELETE );
  }

  @Override
  public PortalManagementUpdater<T> insertTo( )
  {
    return new PortalManagementUpdater<T>( vfcClient, URL, UpdateStrategy.INSERT );
  }

  @Override
  public PortalManagementUpdater<T> update( )
  {
    return new PortalManagementUpdater<T>( vfcClient, URL, UpdateStrategy.UPDATE );
  }

  @Override
  public PortalManagementUpdater<T> patch( )
  {
    return new PortalManagementUpdater<T>( vfcClient, URL, UpdateStrategy.PATCH );
  }

  private List<Pair<String, String>> addQueryParams( String[] startswith, String filter )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( startswith != null )
    {
      for( String key : startswith )
      {
        addParam( queryParams, "startswith", key );
      }
    }
    addParam( queryParams, "filter", filter );
    return queryParams;
  }

}
