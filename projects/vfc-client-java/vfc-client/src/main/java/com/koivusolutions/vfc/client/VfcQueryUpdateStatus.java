package com.koivusolutions.vfc.client;

import org.json.JSONObject;

public class VfcQueryUpdateStatus
{
  public enum Response
  {
    OK, ERROR;

    boolean isError( )
    {
      return this == ERROR;
    }
  }

  private Response responseCode;
  private String responseMessage;

  public VfcQueryUpdateStatus( )
  {
    super( );
  }

  public Response getResponse( )
  {
    return responseCode;
  }

  public String getResponseMessage( )
  {
    return responseMessage;
  }

  public boolean isError( )
  {
    return responseCode != null ? responseCode.isError( ) : true;
  }

  VfcQueryUpdateStatus setStatus( JSONObject jsonObject )
  {
    responseCode = Response.valueOf( jsonObject.optString( "status", "ERROR" ) );
    responseMessage = jsonObject.optString( "message", "" );
    return this;
  }

  @Override
  public String toString( )
  {
    return "VfcQueryUpdateStatus [responseCode=" + responseCode + ", responseMessage="
        + responseMessage + "]";
  }

}