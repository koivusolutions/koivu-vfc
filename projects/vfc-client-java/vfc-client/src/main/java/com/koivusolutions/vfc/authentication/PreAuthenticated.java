package com.koivusolutions.vfc.authentication;

import com.koivusolutions.vfc.client.VfcClientException;

public class PreAuthenticated implements Authentication
{
  private final String accessToken;
  private final String tokenType;

  public PreAuthenticated( String accessToken, String tokenType )
  {
    super( );
    this.accessToken = accessToken;
    this.tokenType = tokenType;
  }

  @Override
  public String getAccessToken( ) throws VfcClientException
  {
    return accessToken;
  }

  @Override
  public String getTokenType( )
  {
    return tokenType;
  }

}
