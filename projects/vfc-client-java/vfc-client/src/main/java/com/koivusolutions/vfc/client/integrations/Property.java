package com.koivusolutions.vfc.client.integrations;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public class Property
{
  private static final String VALUE = "value";

  private final PropertyKey key;
  private final String value;

  public Property( PropertyKey key, CloudTableRow properties )
  {
    super( );
    this.key = key;
    this.value = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, VALUE, "" );
  }

  public PropertyKey getKey( )
  {
    return key;
  }

  public String getValue( )
  {
    return value;
  }
}
