package com.koivusolutions.vfc.client.cloudtable;

import com.koivusolutions.vfc.client.*;

public class CloudTableQueryUpdater extends VfcQueryUpdateResponse<CloudTableRow>
{
  private final String URL;
  private final String category;

  public CloudTableQueryUpdater( VfcClient vfcClient, String category,
      UpdateStrategy updateStrategy, String URL )
  {
    super( vfcClient, updateStrategy );
    this.category = category;
    this.URL = URL;
  }

  @Override
  protected String getUrl( )
  {
    return URL + "/" + category;
  }

}
