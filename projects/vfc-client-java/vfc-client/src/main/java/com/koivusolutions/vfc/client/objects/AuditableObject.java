package com.koivusolutions.vfc.client.objects;

import java.text.*;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

abstract class AuditableObject
{

  public static final String COLUMN_TYPE_SYSTEM = "system";
  public static final String SYSTEM_LAST_MODIFIED_BY = "lastModifiedBy";
  public static final String SYSTEM_LAST_MODIFIED_TIME = "lastModifiedTime";
  public static final DateFormat DATE_FORMAT = new SimpleDateFormat( "yyyy-MM-dd" );

  protected final JSONObject row;

  public AuditableObject( JSONObject obj )
  {
    super( );
    this.row = obj;
  }

  protected String getFieldAsString( String name )
  {
    return row.optString( name );
  }

  protected int getFieldAsInt( String name )
  {
    return row.optInt( name );
  }

  protected boolean getFieldAsBoolean( String name )
  {
    return row.optBoolean( name );
  }

  protected JSONObject getFieldAsJsonObject( String name )
  {
    return row.optJSONObject( name );
  }

  protected void setFieldAsString( String name, String value )
  {
    row.putOpt( name, value );
  }

  protected void setFieldAsJsonObject( String name, JSONObject value )
  {
    row.putOpt( name, value );
  }

  protected void setFieldAsJsonObject( String name, JSONArray value )
  {
    row.putOpt( name, value );
  }

  protected void setFieldAsBoolean( String name, boolean value )
  {
    row.putOpt( name, Boolean.valueOf( value ) );
  }

  protected void setFieldAsInt( String name, int value )
  {
    row.putOpt( name, Integer.valueOf( value ) );
  }

  protected JSONObject getAsJson( )
  {
    return row;
  }

  public String getLastModifiedOn( )
  {
    return getFieldAsString( SYSTEM_LAST_MODIFIED_TIME );
  }

  public String getLastModifiedBy( )
  {
    return getFieldAsString( SYSTEM_LAST_MODIFIED_BY );
  }

  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equalsIgnoreCase( SYSTEM_LAST_MODIFIED_BY, column ) )
    {
      return Objects.toString( getLastModifiedBy( ), defaultValue );
    }
    if( StringUtils.equalsIgnoreCase( SYSTEM_LAST_MODIFIED_TIME, column ) )
    {
      return Objects.toString( getLastModifiedOn( ), defaultValue );
    }
    return defaultValue;
  }

  public Date getAsDate( String type, String field, Date defaultValue )
  {
    String value = getAsString( type, field, null );
    if( StringUtils.isBlank( value ) )
    {
      return defaultValue;
    }
    try
    {
      return DATE_FORMAT.parse( value );
    }
    catch( ParseException e )
    {
      return defaultValue;
    }
  }

  public Date getAsDate( String type, String field, String defaultValue )
  {
    Date defaultDate = null;
    try
    {
      if( StringUtils.isNotBlank( defaultValue ) )
      {
        defaultDate = DATE_FORMAT.parse( defaultValue );
      }
    }
    catch( ParseException e )
    {
    }
    return getAsDate( type, field, defaultDate );
  }

  public Integer getAsInteger( String type, String field, Integer defaultValue )
  {
    String value = getAsString( type, field, null );
    if( StringUtils.isBlank( value ) )
    {
      return defaultValue;
    }
    try
    {
      return Integer.valueOf( Integer.parseInt( value ) );
    }
    catch( NumberFormatException e )
    {
      return defaultValue;
    }
  }

  public Integer getAsInteger( String type, String field, String defaultValue )
  {
    Integer defaultInteger = null;
    try
    {
      if( StringUtils.isNotBlank( defaultValue ) )
      {
        defaultInteger = Integer.valueOf( Integer.parseInt( defaultValue ) );
      }
    }
    catch( NumberFormatException e )
    {
    }
    return getAsInteger( type, field, defaultInteger );
  }

  public int getAsInteger( String type, String field, int defaultValue )
  {
    Integer defaultInteger = null;
    Integer value = getAsInteger( type, field, defaultInteger );
    return value != null ? value.intValue( ) : defaultValue;
  }

  public Double getAsDouble( String type, String field, Double defaultValue )
  {
    String value = getAsString( type, field, null );
    if( StringUtils.isBlank( value ) )
    {
      return defaultValue;
    }
    try
    {
      return Double.valueOf( Double.parseDouble( value ) );
    }
    catch( NumberFormatException e )
    {
      return defaultValue;
    }
  }

  public Double getAsDouble( String type, String field, String defaultValue )
  {
    Double defaultDouble = null;
    try
    {
      if( StringUtils.isNotBlank( defaultValue ) )
      {
        defaultDouble = Double.valueOf( Double.parseDouble( defaultValue ) );
      }
    }
    catch( NumberFormatException e )
    {
    }
    return getAsDouble( type, field, defaultDouble );
  }

  public double getAsDouble( String type, String field, double defaultValue )
  {
    Double defaultDouble = null;
    Double value = getAsDouble( type, field, defaultDouble );
    return value != null ? value.doubleValue( ) : defaultValue;
  }

  public boolean getAsBoolean( String type, String field, boolean defaultValue )
  {
    String value = getAsString( type, field, null );
    if( StringUtils.isBlank( value ) )
    {
      return defaultValue;
    }
    return Boolean.parseBoolean( value );
  }

  public boolean getAsBoolean( String type, String field, String defaultValue )
  {
    boolean defaultBoolean = Boolean.parseBoolean( defaultValue );
    return getAsBoolean( type, field, defaultBoolean );
  }

  protected void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      setFieldAsString( column, newValue );
      return;
    }
  }

  protected void setAsDate( String columnType, String column, Date newValue )
  {
    String newStringValue = newValue != null ? DATE_FORMAT.format( newValue ) : null;
    setAsString( columnType, column, newStringValue );
  }

  protected void setAsInteger( String columnType, String column, int newValue )
  {
    setAsString( columnType, column, Integer.toString( newValue ) );
  }

  protected void setAsDouble( String columnType, String column, double newValue )
  {
    setAsString( columnType, column, Double.toString( newValue ) );
  }

  protected void setAsBoolean( String columnType, String column, boolean newValue )
  {
    setAsString( columnType, column, Boolean.toString( newValue ) );
  }

  @Override
  public int hashCode( )
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ( ( row == null ) ? 0 : row.toString( ).hashCode( ) );
    return result;
  }

  @Override
  public boolean equals( Object obj )
  {
    if( this == obj )
      return true;
    if( obj == null )
      return false;
    if( getClass( ) != obj.getClass( ) )
      return false;
    AuditableObject other = (AuditableObject)obj;
    if( row == null )
    {
      if( other.row != null )
        return false;
    }
    else if( !row.toString( ).equals( other.row.toString( ) ) )
      return false;
    return true;
  }

}