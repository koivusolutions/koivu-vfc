package com.koivusolutions.vfc.client;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.http.HttpResponseExceptionWithEntity;

public abstract class VfcQueryUpdateResponse<T extends VfcObject> extends VfcUpdateResponseBase<T>
{
  Future<JSONObject> current = null;
  private final List<Pair<String, String>> queryParams = new ArrayList<>( );
  VfcResponseStatus responseStatus = null;

  public VfcQueryUpdateResponse( VfcClient vfcClient, UpdateStrategy updateStrategy )
  {
    super( vfcClient, updateStrategy );

  }

  public VfcQueryUpdateResponse<T> byKeys( String keys[] )
  {
    return byKeys( keys, null );
  }

  public VfcQueryUpdateResponse<T> byKeys( String keys[], String filter )
  {
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( "key", key );
      }
    }
    addParam( "filter", filter );
    flush( );
    return this;
  }

  public VfcQueryUpdateResponse<T> byKeysStartingWith( String keys[] )
  {
    return byKeysStartingWith( keys, null );
  }

  public VfcQueryUpdateResponse<T> byKeysStartingWith( String keys[], String filter )
  {
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( "startswith", key );
      }
    }
    addParam( "filter", filter );
    flush( );
    return this;
  }

  public VfcQueryUpdateResponse<T> byKeyRange( String keysFrom[], String[] keysTo )
  {
    return byKeyRange( keysFrom, keysTo, null );
  }

  public VfcQueryUpdateResponse<T> byKeyRange( String keysFrom[], String[] keysTo, String filter )
  {
    if( keysFrom != null )
    {
      for( String key : keysFrom )
      {
        addParam( "keyfrom", key );
      }
    }
    if( keysTo != null )
    {
      for( String key : keysTo )
      {
        addParam( "keyto", key );
      }
    }
    addParam( "filter", filter );
    flush( );
    return this;
  }

  private void addParam( String name, String value )
  {
    if( StringUtils.isNotBlank( value ) )
    {
      queryParams.add( new ImmutablePair<String, String>( name, value ) );
    }
  }

  protected VfcIterator<JSONObject> read( )
  {
    return new VfcIterator<JSONObject>( ) {
      private Iterator<JSONObject> rows = null;

      @Override
      public boolean hasNext( ) throws VfcClientException
      {
        try
        {
          if( rows == null || rows.hasNext( ) == false )
          {
            rows = null;
            if( current != null )
            {
              JSONObject serverResponse = current.get( );
              responseStatus = new VfcResponseStatus( serverResponse );
              current = null;
              JSONArray rowsArray = serverResponse.getJSONArray( "data" );
              if( rowsArray != null )
              {
                List<JSONObject> list = new ArrayList<>( rowsArray.length( ) );
                for( int i = 0; i < rowsArray.length( ); i++ )
                {
                  list.add( rowsArray.getJSONObject( i ) );
                }
                rows = list.iterator( );
              }
            }
          }
          return rows != null && rows.hasNext( );
        }
        catch( InterruptedException | ExecutionException e )
        {
          Throwable cause = e.getCause( );
          JSONObject responseEntity = null;
          if( cause instanceof HttpResponseExceptionWithEntity )
          {
            HttpResponseExceptionWithEntity httpException = (HttpResponseExceptionWithEntity)cause;
            responseEntity = httpException.getResponseEntity( );
            responseStatus = new VfcResponseStatus( responseEntity );
            if( httpException.getStatusCode( ) == HttpStatus.SC_NOT_FOUND )
            {
              return false;
            }
          }
          throw new VfcClientException( "Data read error:" + e.getMessage( ), cause,
              responseEntity );
        }
      }

      @Override
      public JSONObject next( ) throws VfcClientException
      {
        if( hasNext( ) )
        {
          return rows.next( );
        }
        else
        {
          throw new NoSuchElementException( );
        }
      }

      @Override
      public VfcResponseStatus getResponseStatus( )
      {
        return responseStatus;
      }
    };
  }

  public VfcQueryUpdateResponse<T> flush( )
  {
    List<Pair<String, String>> requestQueryParams = new ArrayList<>( );
    requestQueryParams.addAll( queryParams );
    requestQueryParams.add( new ImmutablePair<>( "maxcount", "9999999" ) );

    current = vfcClient.sendToVfcAsync( getUrl( ), requestQueryParams, null, updateStrategy );
    return this;
  }

  public VfcIterator<VfcQueryUpdateStatus> iteratorEx( )
  {
    VfcIterator<JSONObject> jsonIt = read( );
    return new VfcIterator<VfcQueryUpdateStatus>( ) {

      @Override
      public boolean hasNext( ) throws VfcClientException
      {
        return jsonIt.hasNext( );
      }

      @Override
      public VfcQueryUpdateStatus next( ) throws NoSuchElementException,
          VfcClientException
      {
        return new VfcQueryUpdateStatus( ).setStatus( jsonIt.next( ) );
      }

      @Override
      public VfcResponseStatus getResponseStatus( ) throws VfcClientException
      {
        return jsonIt.getResponseStatus( );
      }
    };
  }

  public void checkErrors( ) throws VfcClientException
  {
    VfcIterator<VfcQueryUpdateStatus> resp = iteratorEx( );
    while( resp.hasNext( ) )
    {
      VfcQueryUpdateStatus reponse = resp.next( );
      if( reponse.isError( ) )
      {
        throw new VfcClientException( "Data update error:" + reponse.getResponseMessage( ) );
      }
    }
  }

  @Override
  public String toString( )
  {
    return this.getClass( ).getSimpleName( ) + " [ queryParams=" + queryParams + ", responseStatus="
        + responseStatus + "]";
  }

}
