package com.koivusolutions.vfc.logging;


public interface Log
{
  public void logError( String msg, Throwable t );

  public void logError( String msg );

  public void logWarn( String msg );

  public void logInfo( String msg );

  public void logDebug( String msg );
}