package com.koivusolutions.vfc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.math.NumberUtils;

public class VfcProperties extends Properties
{
  private static final String VFC_CLIENT_PROPERTIES = "/vfc-client.properties";

  public VfcProperties( ) throws IOException
  {
    try (InputStream inputStream = getClass( ).getResourceAsStream( VFC_CLIENT_PROPERTIES ))
    {
      this.load( inputStream );
    }
  }

  public VfcProperties( InputStream inputStream ) throws IOException
  {
    this( );
    this.load( inputStream );
  }

  public int getPropertyAsInt( String name, int defaultValue )
  {
    String propertyValue = getProperty( name );
    if( propertyValue != null )
    {
      return NumberUtils.toInt( propertyValue, defaultValue );
    }
    return defaultValue;
  }
}
