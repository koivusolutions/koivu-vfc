package com.koivusolutions.vfc.client.portalmanagement;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class PrivilegeRow extends ImmutablePrivilegeRow implements VfcObject
{
  public PrivilegeRow( )
  {
    super( );
  }

  public PrivilegeRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setCreateRole( String roleId )
  {
    setFieldAsString( SYSTEM_CREATE_ROLE, roleId );
  }

  public void setReadRole( String roleId )
  {
    setFieldAsString( SYSTEM_READ_ROLE, roleId );
  }

  public void setUpdateRole( String roleId )
  {
    setFieldAsString( SYSTEM_UPDATE_ROLE, roleId );
  }

  public void setDeleteRole( String roleId )
  {
    setFieldAsString( SYSTEM_DELETE_ROLE, roleId );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_CREATE_ROLE, column ) )
      {
        setCreateRole( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_READ_ROLE, column ) )
      {
        setReadRole( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_UPDATE_ROLE, column ) )
      {
        setUpdateRole( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_DELETE_ROLE, column ) )
      {
        setDeleteRole( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutablePrivilegeRow asImmutablePrivilegeRow( )
  {
    return this;
  }
}
