package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutablePrivilegeRow extends ImmutablePortalManagementTableRow
{
  public static final String SYSTEM_CREATE_ROLE = "createRole";
  public static final String SYSTEM_READ_ROLE = "readRole";
  public static final String SYSTEM_UPDATE_ROLE = "updateRole";
  public static final String SYSTEM_DELETE_ROLE = "deleteRole";

  public ImmutablePrivilegeRow( )
  {
    super( );
  }

  public ImmutablePrivilegeRow( JSONObject obj )
  {
    super( obj );
  }

  public String getCreateRole( )
  {
    return getFieldAsString( SYSTEM_CREATE_ROLE );
  }

  public String getReadRole( )
  {
    return getFieldAsString( SYSTEM_READ_ROLE );
  }

  public String getUpdateRole( )
  {
    return getFieldAsString( SYSTEM_UPDATE_ROLE );
  }

  public String getDeleteRole( )
  {
    return getFieldAsString( SYSTEM_DELETE_ROLE );
  }

}
