package com.koivusolutions.vfc.client.cloudtable;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.koivusolutions.vfc.client.*;

public class CloudTablesBase implements VfcTableUpdates<CloudTableRow>
{
  private final VfcClient vfcClient;
  private final String URLPrefix;
  private final String name;

  public CloudTablesBase( VfcClient vfcClient, String name, String URLPrefix )
  {
    this.vfcClient = vfcClient;
    this.name = name;
    this.URLPrefix = URLPrefix;
  }

  public CloudTable getCloudTable( String category, String keys[] )
  {
    return getCloudTable( category, keys, null, true, null, null, null, false );
  }

  public CloudTable getCloudTable( String category, String keys[], String filter )
  {
    return getCloudTable( category, keys, filter, true, null, null, null, false );
  }

  public CloudTable getCloudTable( String category, String keys[], String filter,
      String newerThanTime )
  {
    return getCloudTable( category, keys, filter, true, newerThanTime, null, null, false );
  }

  public CloudTable getCloudTable( String category, String keys[], String filter,
      boolean filterByLevels, String newerThanTime, String expressions, String impersonatebusiness,
      boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "key", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, impersonatebusiness,
        includedeleted, queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableKeysStartingWith( String category, String keys[] )
  {
    return getCloudTableKeysStartingWith( category, keys, null, true, null, null, null, false );
  }

  public CloudTable getCloudTableKeysStartingWith( String category, String keys[], String filter )
  {
    return getCloudTableKeysStartingWith( category, keys, filter, true, null, null, null, false );
  }

  public CloudTable getCloudTableKeysStartingWith( String category, String keys[], String filter,
      String newerThanTime )
  {
    return getCloudTableKeysStartingWith( category, keys, filter, true, newerThanTime, null, null,
        false );
  }

  public CloudTable getCloudTableKeysStartingWith( String category, String keys[], String filter,
      boolean filterByLevels, String newerThanTime, String expressions, String impersonatebusiness,
      boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "startswith", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, impersonatebusiness,
        includedeleted, queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableExactKeys( String category, String keys[] )
  {
    return getCloudTableExactKeys( category, keys, null, true, null, null, null, false );
  }

  public CloudTable getCloudTableExactKeys( String category, String keys[], String filter )
  {
    return getCloudTableExactKeys( category, keys, filter, true, null, null, null, false );
  }

  public CloudTable getCloudTableExactKeys( String category, String keys[], String filter,
      String newerThanTime )
  {
    return getCloudTableExactKeys( category, keys, filter, true, newerThanTime, null, null, false );
  }

  public CloudTable getCloudTableExactKeys( String category, String keys[], String filter,
      boolean filterByLevels, String newerThanTime, String expressions, String impersonatebusiness,
      boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "exactkey", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, impersonatebusiness,
        includedeleted, queryParams );

    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableWithKeyRange( String category, String keysFrom[], String[] keysTo )
  {
    return getCloudTableWithKeyRange( category, keysFrom, keysTo, null, true, null, null, null,
        false );
  }

  public CloudTable getCloudTableWithKeyRange( String category, String keysFrom[], String[] keysTo,
      String filter )
  {
    return getCloudTableWithKeyRange( category, keysFrom, keysTo, filter, true, null, null, null,
        false );
  }

  public CloudTable getCloudTableWithKeyRange( String category, String keysFrom[], String[] keysTo,
      String filter, String newerThanTime )
  {
    return getCloudTableWithKeyRange( category, keysFrom, keysTo, filter, true, newerThanTime, null,
        null, false );
  }

  public CloudTable getCloudTableWithKeyRange( String category, String keysFrom[], String[] keysTo,
      String filter, boolean filterByLevels, String newerThanTime, String expressions,
      String impersonatebusiness, boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keysFrom != null )
    {
      for( String key : keysFrom )
      {
        addParam( queryParams, "keyfrom", key );
      }
    }
    if( keysTo != null )
    {
      for( String key : keysTo )
      {
        addParam( queryParams, "keyto", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, impersonatebusiness,
        includedeleted, queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  private void addParam( List<Pair<String, String>> queryParams, String paramName,
      String paramValue )
  {
    if( StringUtils.isNotBlank( paramValue ) )
    {
      queryParams.add( new ImmutablePair<String, String>( paramName, paramValue ) );
    }
  }

  public CloudTableQueryUpdater deleteFromCloudTableByQuery( String category )
  {
    return new CloudTableQueryUpdater( vfcClient, category, UpdateStrategy.DELETE, URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> deleteFromCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.DELETE, URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> insertToTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.INSERT, URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> updateCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.UPDATE, URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> patchCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.PATCH, URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> patchReplaceCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.PATCH_REPLACE,
        URLPrefix );
  }

  public CloudTableUpdater<CloudTableRow> replaceInCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.REPLACE, URLPrefix );
  }

  private void addQueryParams( String filter, boolean filterByLevels, String newerThanTime,
      String expressions, String impersonatebusiness, boolean includedeleted,
      List<Pair<String, String>> queryParams )
  {
    addParam( queryParams, "filter", filter );
    addParam( queryParams, "newerthan", newerThanTime );
    addParam( queryParams, "expression", expressions );
    addParam( queryParams, "impersonatebusiness", impersonatebusiness );
    if( includedeleted )
    {
      addParam( queryParams, "includedeleted", "true" );
    }
    if( !filterByLevels )
    {
      addParam( queryParams, "filterbylevels", "false" );
    }
  }

  @Override
  public VfcUpdater<CloudTableRow> deleteFrom( )
  {
    return this.deleteFromCloudTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> insertTo( )
  {
    return this.insertToTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> update( )
  {
    return this.updateCloudTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> patch( )
  {
    return this.patchCloudTable( );
  }

}
