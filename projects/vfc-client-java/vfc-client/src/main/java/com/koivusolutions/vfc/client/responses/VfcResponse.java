package com.koivusolutions.vfc.client.responses;

import com.koivusolutions.vfc.client.VfcClientException;

public interface VfcResponse
{
  VfcResponseStatus getResponseStatus( ) throws VfcClientException;
}
