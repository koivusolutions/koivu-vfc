package com.koivusolutions.vfc.client.integrations;

import com.koivusolutions.vfc.client.VfcClient;

public class ManagerParams
{
  private VfcClient vfc;
  private String id;
  private String propertiesTable;
  private String columnsTable;
  private String mappingTable;

  public VfcClient getVfc( )
  {
    return vfc;
  }

  public void setVfc( VfcClient vfc )
  {
    this.vfc = vfc;
  }

  public String getId( )
  {
    return id;
  }

  public void setId( String id )
  {
    this.id = id;
  }

  public String getPropertiesTable( )
  {
    return propertiesTable;
  }

  public void setPropertiesTable( String propertiesTable )
  {
    this.propertiesTable = propertiesTable;
  }

  public String getColumnsTable( )
  {
    return columnsTable;
  }

  public void setColumnsTable( String columnsTable )
  {
    this.columnsTable = columnsTable;
  }

  public String getMappingTable( )
  {
    return mappingTable;
  }

  public void setMappingTable( String mappingTable )
  {
    this.mappingTable = mappingTable;
  }

}
