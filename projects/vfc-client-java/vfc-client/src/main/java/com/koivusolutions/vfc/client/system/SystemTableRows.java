package com.koivusolutions.vfc.client.system;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcObject;
import com.koivusolutions.vfc.client.objects.Table;

public class SystemTableRows<T extends VfcObject> extends Table<T>
{
  private final SystemTable<T> rowType;

  SystemTableRows( SystemTable<T> rowType, VfcClient vfcClient,
      List<Pair<String, String>> query )
  {
    super( vfcClient, rowType.getUrl( ), query );
    this.rowType = rowType;
  }

  @Override
  public String toString( )
  {
    return "Rows [ query=" + super.getQueryParams( ) + "]";
  }

  @Override
  protected T build( JSONObject obj )
  {
    return rowType.build( obj );
  }

}
