package com.koivusolutions.vfc.client.system;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.SequenceObject;

public class ImmutableSequenceRow extends SequenceObject
{

  public ImmutableSequenceRow( )
  {
    super( new JSONObject( ) );
  }

  public ImmutableSequenceRow( JSONObject obj )
  {
    super( obj );
  }

}
