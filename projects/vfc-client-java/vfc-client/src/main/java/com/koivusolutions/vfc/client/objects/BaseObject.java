package com.koivusolutions.vfc.client.objects;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

abstract class BaseObject extends AuditableObject
{
  public static final String COLUMN_TYPE_PROPERTIES = "properties";

  public BaseObject( JSONObject obj )
  {
    super( obj );
  }


  public Map<String, String> getProperties( )
  {
    JSONObject propertiesJSON = row.optJSONObject( COLUMN_TYPE_PROPERTIES );
    if( propertiesJSON != null )
    {
      Map<String, String> map = new HashMap<String, String>( );
      for( String key : propertiesJSON.keySet( ) )
      {
        map.put( key, propertiesJSON.optString( key ) );
      }
      return map;
    }
    return null;
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_PROPERTIES, columnType ) )
    {
      JSONObject jsonObject = row.optJSONObject( COLUMN_TYPE_PROPERTIES );
      if( jsonObject != null )
      {
        return jsonObject.optString( column, defaultValue );
      }
      return defaultValue;
    }
    return super.getAsString( columnType, column, defaultValue );
  }

  protected void setProperties( Map<String, String> properties )
  {
    JSONObject propertiesJSON = new JSONObject( properties );
    setFieldAsJsonObject( COLUMN_TYPE_PROPERTIES, propertiesJSON );
  }

  protected void addProperty( String name, String value )
  {
    if( !row.has( COLUMN_TYPE_PROPERTIES ) )
    {
      JSONObject propertiesJSON = new JSONObject( );
      row.putOpt( COLUMN_TYPE_PROPERTIES, propertiesJSON );
    }
    row.getJSONObject( COLUMN_TYPE_PROPERTIES ).putOpt( name, value );
  }

  @Override
  protected void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_PROPERTIES, columnType ) )
    {
      addProperty( column, newValue );
      return;
    }
    super.setAsString( columnType, column, newValue );
  }

}