package com.koivusolutions.vfc.client;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.responses.VfcResponse;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;

public class VfcClientException extends Exception implements VfcResponse
{
  private VfcResponseStatus responseStatus = null;

  public VfcClientException( String message )
  {
    super( message );
  }

  public VfcClientException( String message, Throwable cause )
  {
    super( message, cause );
  }

  public VfcClientException( String message, JSONObject serverResponse )
  {
    super( message );
    this.responseStatus = new VfcResponseStatus( serverResponse );
  }

  public VfcClientException( String message, Throwable cause, JSONObject serverResponse )
  {
    super( message, cause );
    this.responseStatus = new VfcResponseStatus( serverResponse );
  }

  @Override
  public VfcResponseStatus getResponseStatus( )
  {
    if( responseStatus == null )
    {
      responseStatus = new VfcResponseStatus( );
    }
    return responseStatus;
  }

}
