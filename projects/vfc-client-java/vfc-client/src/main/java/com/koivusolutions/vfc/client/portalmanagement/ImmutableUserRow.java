package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutableUserRow extends ImmutablePortalManagementTableRow
{
  public static final String SYSTEM_APPROVED = "approved";
  public static final String SYSTEM_EXTERNAL = "external";
  public static final String SYSTEM_ASSIGNED_BUSINESS = "assignedBusiness";
  public static final String SYSTEM_ROLE_GROUP = "roleGroup";

  public ImmutableUserRow( )
  {
    super( );
  }

  public ImmutableUserRow( JSONObject obj )
  {
    super( obj );
  }

  public boolean isApproved( )
  {
    return getFieldAsBoolean( SYSTEM_APPROVED );
  }

  public boolean isExternal( )
  {
    return getFieldAsBoolean( SYSTEM_EXTERNAL );
  }

  public String getAssignedBusiness( )
  {
    return getFieldAsString( SYSTEM_ASSIGNED_BUSINESS );
  }

  public String getRoleGroup( )
  {
    return getFieldAsString( SYSTEM_ROLE_GROUP );
  }

}
