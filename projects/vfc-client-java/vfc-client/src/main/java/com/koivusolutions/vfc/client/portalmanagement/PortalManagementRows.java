package com.koivusolutions.vfc.client.portalmanagement;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcObject;
import com.koivusolutions.vfc.client.objects.Table;

public class PortalManagementRows<T extends VfcObject> extends Table<T>
{
  private final PortalManagementTable<T> rowType;

  PortalManagementRows( PortalManagementTable<T> rowType, VfcClient vfcClient,
      List<Pair<String, String>> query )
  {
    super( vfcClient, rowType.getUrl( ), query );
    this.rowType = rowType;
  }

  @Override
  public String toString( )
  {
    return "PortalManagementRows [ query=" + super.getQueryParams( ) + "]";
  }

  @Override
  protected T build( JSONObject obj )
  {
    return rowType.build( obj );
  }

}
