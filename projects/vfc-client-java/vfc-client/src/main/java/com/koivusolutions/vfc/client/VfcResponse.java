package com.koivusolutions.vfc.client;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.responses.VfcResponseStatus;

public abstract class VfcResponse
{
  private final VfcClient vfcClient;
  private final String URL;
  private VfcIteratorWithMetaData currentIterator = null;

  public VfcResponse( VfcClient vfcClient, String URL )
  {
    this.vfcClient = vfcClient;
    this.URL = URL;
  }

  public String getUrl( )
  {
    return URL;
  }

  protected abstract List<Pair<String, String>> getQueryParams( );

  protected abstract UpdateStrategy getMethod( );

  protected abstract JSONObject getData( );

  protected VfcClient getVfcClient( )
  {
    return vfcClient;
  }

  protected VfcIteratorWithMetaData read( )
  {
    if( currentIterator == null )
    {
      currentIterator = new VfcIteratorWithMetaData( this );
    }
    return currentIterator;
  }

  protected void initialize( )
  {
    currentIterator = new VfcIteratorWithMetaData( this );
  }

  public VfcResponseStatus getResponseStatus( )
  {
    if( currentIterator != null )
    {
      return currentIterator.getResponseStatus( );
    }
    return new VfcResponseStatus( );
  }

}
