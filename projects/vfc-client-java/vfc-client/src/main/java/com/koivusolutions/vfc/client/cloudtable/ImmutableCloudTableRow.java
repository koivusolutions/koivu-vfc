package com.koivusolutions.vfc.client.cloudtable;

import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.KeyableObject;

public class ImmutableCloudTableRow extends KeyableObject
{
  public static final String COLUMN_TYPE_EXPRESSIONS = "expressions";
  public static final String SYSTEM_LEVEL = "level";
  public static final String SYSTEM_DESCRIPTION = "description";
  public static final String SYSTEM_CATEGORY = "category";

  protected ImmutableCloudTableRow( )
  {
    this( new JSONObject( ) );
  }

  protected ImmutableCloudTableRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public String toString( )
  {
    return "CloudTableRow [ " + getCategory( ) + ", " + Arrays.toString( getKeys( ) )
        + ", Description=" + getDescription( ) + ", Properties=" + getProperties( )
        + ", Expressions=" + getExpressions( ) + ", LastModifiedOn=" + getLastModifiedOn( )
        + ", LastModifiedBy=" + getLastModifiedBy( ) + "]";
  }

  public String getCategory( )
  {
    return getFieldAsString( SYSTEM_CATEGORY );
  }

  public int getLevel( )
  {
    return getFieldAsInt( SYSTEM_LEVEL );
  }

  public Map<String, String> getExpressions( )
  {
    JSONObject expressionsJSON = getFieldAsJsonObject( COLUMN_TYPE_EXPRESSIONS );
    if( expressionsJSON != null )
    {
      Map<String, String> map = new HashMap<String, String>( );
      for( String key : expressionsJSON.keySet( ) )
      {
        map.put( key, expressionsJSON.optString( key ) );
      }
      return map;
    }
    return null;
  }

  public String getDescription( )
  {
    return getFieldAsString( SYSTEM_DESCRIPTION );
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_EXPRESSIONS, columnType ) )
    {
      JSONObject jsonObject = getFieldAsJsonObject( COLUMN_TYPE_EXPRESSIONS );
      if( jsonObject != null )
      {
        return jsonObject.optString( column, defaultValue );
      }
      return defaultValue;
    }
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_CATEGORY, column ) )
      {
        return Objects.toString( getCategory( ), defaultValue );
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_DESCRIPTION, column ) )
      {
        return Objects.toString( getDescription( ), defaultValue );
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_LEVEL, column ) )
      {
        return Objects.toString( String.valueOf( getLevel( ) ), defaultValue );
      }
    }
    return super.getAsString( columnType, column, defaultValue );
  }
}