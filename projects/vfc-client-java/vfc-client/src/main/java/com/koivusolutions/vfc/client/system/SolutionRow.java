package com.koivusolutions.vfc.client.system;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class SolutionRow extends ImmutableSolutionRow implements VfcObject
{
  public SolutionRow( )
  {
    super( );
  }

  public SolutionRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setId( String id )
  {
    super.setId( id );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setDefaultSolution( boolean defaultSolution )
  {
    setFieldAsBoolean( SYSTEM_DEFAULT_SOLUTION, defaultSolution );
  }

  public void setName( String name )
  {
    setFieldAsString( SYSTEM_NAME, name );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_DEFAULT_SOLUTION, column ) )
      {
        setDefaultSolution( Boolean.valueOf( newValue ).booleanValue( ) );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_NAME, column ) )
      {
        setName( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableSolutionRow asImmutableSolutionRow( )
  {
    return this;
  }
}
