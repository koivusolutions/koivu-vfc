package com.koivusolutions.vfc.client.portalmanagement;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class CreationPrivilegeRow extends ImmutableCreationPrivilegeRow implements VfcObject
{
  public CreationPrivilegeRow( )
  {
    super( );
  }

  public CreationPrivilegeRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setCreationRole( String creationRoleId )
  {
    setFieldAsString( SYSTEM_CREATION_ROLE, creationRoleId );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_CREATION_ROLE, column ) )
      {
        setCreationRole( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableCreationPrivilegeRow asImmutableCreationPrivilegeRow( )
  {
    return this;
  }
}
