package com.koivusolutions.vfc.client.cloudtable;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.objects.Table;

public class CloudTable extends Table<CloudTableRow>
{
  private final String name;
  private final String category;

  CloudTable( VfcClient vfcClient, String name, String URLPrefix, String category,
      List<Pair<String, String>> query )
  {
    super( vfcClient, URLPrefix + "/" + category, query );
    this.category = category;
    this.name = name;
  }

  @Override
  public String toString( )
  {
    return name + " [" + category + " query=" + super.getQueryParams( ) + "]";
  }

  public String getCategory( )
  {
    return category;
  }

  @Override
  protected CloudTableRow build( JSONObject obj )
  {
    return new CloudTableRow( obj );
  }

}
