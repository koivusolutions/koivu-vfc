package com.koivusolutions.vfc.client.solutioncloudtables;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.cloudtable.CloudTablesBase;

public class SolutionCloudTables extends CloudTablesBase
{

  public SolutionCloudTables( VfcClient vfcClient )
  {
    super( vfcClient, "SolutionCloudTable", "solutionproperties" );
  }
}
