package com.koivusolutions.vfc.client;

import org.json.JSONObject;

public interface VfcObject
{
  JSONObject getAsJson( );
}
