package com.koivusolutions.vfc.client.system;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.KeyableObject;

public class ImmutableChatRow extends KeyableObject
{
  public static final String SYSTEM_DESCRIPTION = "description";

  public ImmutableChatRow( )
  {
    super( new JSONObject( ) );
  }

  public ImmutableChatRow( JSONObject obj )
  {
    super( obj );
  }

  public String getDescription( )
  {
    return getFieldAsString( SYSTEM_DESCRIPTION );
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_DESCRIPTION, column ) )
      {
        return Objects.toString( getDescription( ), defaultValue );
      }
    }
    return super.getAsString( columnType, column, defaultValue );
  }
}
