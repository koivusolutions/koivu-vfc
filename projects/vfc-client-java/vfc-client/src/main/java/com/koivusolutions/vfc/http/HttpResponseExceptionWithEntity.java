package com.koivusolutions.vfc.http;

import org.apache.http.client.HttpResponseException;
import org.json.JSONObject;

public class HttpResponseExceptionWithEntity extends HttpResponseException
{
  private final JSONObject responseEntity;

  public HttpResponseExceptionWithEntity( int statusCode, String reasonPhrase,
      JSONObject responseEntity )
  {
    super( statusCode, reasonPhrase );
    this.responseEntity = responseEntity;
  }

  public JSONObject getResponseEntity( )
  {
    return responseEntity;
  }

}
