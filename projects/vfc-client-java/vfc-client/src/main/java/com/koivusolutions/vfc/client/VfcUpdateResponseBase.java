package com.koivusolutions.vfc.client;

public abstract class VfcUpdateResponseBase<T extends VfcObject>
{

  protected final VfcClient vfcClient;
  protected final UpdateStrategy updateStrategy;

  public VfcUpdateResponseBase( VfcClient vfcClient, UpdateStrategy updateStrategy )
  {
    this.vfcClient = vfcClient;
    this.updateStrategy = updateStrategy;
  }

  protected abstract String getUrl( );

}