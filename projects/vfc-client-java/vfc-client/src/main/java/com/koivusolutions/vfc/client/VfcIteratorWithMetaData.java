package com.koivusolutions.vfc.client;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.responses.VfcResponseStatus;
import com.koivusolutions.vfc.http.HttpResponseExceptionWithEntity;

public class VfcIteratorWithMetaData implements VfcIterator<JSONObject>
{
  private Future<JSONObject> current = null;
  private final VfcResponse vfcResponse;
  private Iterator<JSONObject> rows = null;
  private VfcResponseStatus responseStatus = null;

  VfcIteratorWithMetaData( VfcResponse vfcResponse )
  {
    this.vfcResponse = vfcResponse;
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    queryParams.addAll( vfcResponse.getQueryParams( ) );
    this.current = readData( vfcResponse, queryParams );
  }

  @Override
  public boolean hasNext( ) throws VfcClientException
  {
    try
    {
      if( rows == null || rows.hasNext( ) == false )
      {
        rows = null;
        if( current != null )
        {
          JSONObject serverResponse = current.get( );
          this.responseStatus = new VfcResponseStatus( serverResponse );
          current = null;
          JSONObject pagination = serverResponse.optJSONObject( "pagination" );

          Object data = serverResponse.opt( "data" );
          if( data != null )
          {
            ArrayList<JSONObject> list = new ArrayList<>( 1 );
            if( data instanceof JSONArray )
            {
              JSONArray rowsArray = (JSONArray)data;
              list.ensureCapacity( rowsArray.length( ) );
              for( int i = 0; i < rowsArray.length( ); i++ )
              {
                list.add( rowsArray.getJSONObject( i ) );
              }
            }
            else if( data instanceof JSONObject )
            {
              list.add( (JSONObject)data );
            }
            else
            {
              JSONObject textResponse = new JSONObject( );
              textResponse.put( "data", data );
              list.add( textResponse );
            }
            rows = list.iterator( );
          }
          if( pagination != null )
          {
            boolean hasMore = pagination.optBoolean( "hasMoreData" );
            String continueToken = pagination.optString( "continueToken" );
            if( hasMore && StringUtils.isNotBlank( continueToken ) )
            {
              List<Pair<String, String>> queryParams = new ArrayList<>( );
              queryParams.addAll( vfcResponse.getQueryParams( ) );
              queryParams
                  .add( new ImmutablePair<String, String>( "continuetoken", continueToken ) );
              this.current = readData( vfcResponse, queryParams );
            }
          }
        }
      }
      return rows != null && rows.hasNext( );
    }
    catch( InterruptedException | ExecutionException e )
    {
      Throwable cause = e.getCause( );
      JSONObject responseEntity = null;
      if( cause instanceof HttpResponseExceptionWithEntity )
      {
        HttpResponseExceptionWithEntity httpException = (HttpResponseExceptionWithEntity)cause;
        responseEntity = httpException.getResponseEntity( );
        responseStatus = new VfcResponseStatus( responseEntity );
        if( httpException.getStatusCode( ) == HttpStatus.SC_NOT_FOUND )
        {
          return false;
        }
      }
      throw new VfcClientException( "Data read error:" + e.getMessage( ), cause, responseEntity );
    }

  }

  @Override
  public JSONObject next( ) throws VfcClientException
  {
    if( hasNext( ) )
    {
      return rows.next( );
    }
    else
    {
      throw new NoSuchElementException( );
    }
  }

  @Override
  public VfcResponseStatus getResponseStatus( )
  {
    return responseStatus;
  }

  private Future<JSONObject> readData( VfcResponse parent,
      List<Pair<String, String>> queryParams )
  {
    if( parent.getMethod( ) == null )
    {
      return parent.getVfcClient( ).readFromVfcAsync( parent.getUrl( ), queryParams );
    }
    else
    {
      return parent.getVfcClient( ).sendToVfcAsync( parent.getUrl( ), queryParams,
          parent.getData( ), parent.getMethod( ) );
    }
  }

}
