package com.koivusolutions.vfc.client;

import java.util.concurrent.Executors;

import com.koivusolutions.vfc.logging.Logger;

public class JavaThreadFactory implements java.util.concurrent.ThreadFactory
{

  public JavaThreadFactory( )
  {
  }

  @Override
  public Thread newThread( Runnable r )
  {
    Logger.logDebug( "Creating thread with default ThreadFactory" );
    Thread t = Executors.defaultThreadFactory( ).newThread( r );
    t.setDaemon( true );
    return t;
  }

}
