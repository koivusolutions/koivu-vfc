package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutableBusinessRow extends ImmutablePortalManagementTableRow
{
  public static final String SYSTEM_OWNER_BUSINESS = "ownerBusiness";
  public static final String SYSTEM_SUB_TENANT = "tenant";
  public static final String SYSTEM_IMPERSONATING_TARGETS = "impersonatingTargets";

  public ImmutableBusinessRow( )
  {
    super( );
  }

  public ImmutableBusinessRow( JSONObject obj )
  {
    super( obj );
  }

  public boolean isOwnerBusiness( )
  {
    return getFieldAsBoolean( SYSTEM_OWNER_BUSINESS );
  }

  public String getSubTenant( )
  {
    return getFieldAsString( SYSTEM_SUB_TENANT );
  }

  public String getImpersonatingTargets( )
  {
    return getFieldAsString( SYSTEM_IMPERSONATING_TARGETS );
  }

}
