package com.koivusolutions.vfc.authentication;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.HttpResponseException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.http.HttpClient;
import com.koivusolutions.vfc.http.HttpResponseExceptionWithEntity;
import com.koivusolutions.vfc.logging.Logger;

public class KoivuCloudIntegrationIdentityAuthentication implements Authentication
{
  public static String AUTH_TYPE = "authentication.type";
  public enum AuthType
  {
    auth0, firebase
  }

  private final String vfcClientId;
  private final String vfcClientSecret;
  private final HttpClient httpClient;
  private final VfcProperties vfcProperties;
  private String vfcAccessToken;
  private String vfcTokenType;
  private long expires;

  public KoivuCloudIntegrationIdentityAuthentication( VfcProperties vfcProperties, HttpClient httpClient, String vfcClientId,
      String vfcClientSecret ) throws VfcClientException
  {
    this.vfcClientId = vfcClientId;
    this.vfcClientSecret = vfcClientSecret;
    this.httpClient = httpClient;
    this.vfcProperties = vfcProperties;

    refreshAccessToken( );
  }

  private void refreshAccessToken( ) throws VfcClientException
  {
    String authType = vfcProperties.getProperty( AUTH_TYPE, AuthType.firebase.toString( ) );
    try
    {
      switch( AuthType.valueOf( authType.toLowerCase( ) ) )
      {
        case auth0:
        {
          refreshAccessTokenAuth0( );
          break;
        }
        case firebase:
        {
          refreshAccessTokenFirebase( );
          break;
        }
      }
    }
    catch( IllegalArgumentException e )
    {
      throw new VfcClientException( "Unknown authentication type: " + authType
          + ", possible values: " + Arrays.asList( AuthType.values( ) ).toString( ) );
    }
  }

  private void refreshAccessTokenAuth0( ) throws VfcClientException
  {
    try
    {
      String audience = vfcProperties.getProperty( "authentication.auth0.audience",
          "https://koivuapi.koivusolutions.com" );
      String tenant = vfcProperties.getProperty( "authentication.auth0.tenant", "koivusolutions" );
      String tokenUrlTemplate = vfcProperties.getProperty( "authentication.auth0.url.token",
          "https://%s.eu.auth0.com/oauth/token" );
      String tokenUri = String.format( tokenUrlTemplate, tenant );

      Logger.logInfo( "Authenticate request to:" + tokenUri );

      JSONObject postParams = new JSONObject( );
      postParams.put( "grant_type", "client_credentials" );
      postParams.put( "client_id", vfcClientId );
      postParams.put( "client_secret", vfcClientSecret );
      postParams.put( "audience", audience );

      JSONObject responseJSON = httpClient.postJson( null, new URI( tokenUri ), null, postParams );
      vfcAccessToken = responseJSON.getString( "access_token" );
      vfcTokenType = responseJSON.getString( "token_type" );
      // Expires, take 10 mins before expiration
      expires = responseJSON.getLong( "expires_in" ) * 1000 + System.currentTimeMillis( )
          - 10 * 60 * 1000;
      Logger.logInfo( "Authenticated ok" );
    }
    catch( HttpResponseException e )
    {
      if( e.getStatusCode( ) == HttpStatus.SC_FORBIDDEN )
      {
        throw new VfcClientException(
            "Unauthorized. Provided clientid and clientsecret are not valid." );
      }
      throw new VfcClientException( "Cannot authenticate:" + e.getReasonPhrase( ), e );
    }
    catch( Exception e )
    {
      throw new VfcClientException( "Cannot authenticate:" + e.getMessage( ), e );
    }

  }

  private void refreshAccessTokenFirebase( ) throws VfcClientException
  {
    try
    {
      String tokenUrl = vfcProperties.getProperty( "authentication.firebase.url.token",
          "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword" );
      String apiKey = vfcProperties.getProperty( "authentication.firebase.apiKey", "" );
      if( StringUtils.isBlank( apiKey ) )
      {
        throw new VfcClientException( "Firebase authentication apiKey cannot be empty." );
      }

      Logger.logInfo( "Authenticate request to:" + tokenUrl );

      List<Pair<String, String>> queryParams = new ArrayList<>( );
      queryParams.add( new ImmutablePair<String, String>( "key", apiKey ) );

      JSONObject postParams = new JSONObject( );
      postParams.put( "email", vfcClientId );
      postParams.put( "password", vfcClientSecret );
      postParams.put( "returnSecureToken", true );

      JSONObject responseJSON = httpClient.postJson( null, new URI( tokenUrl ), queryParams,
          postParams );
      String accessToken = responseJSON.getString( "idToken" );

      verifyEmailVerifiedFirebase( apiKey, accessToken );

      vfcAccessToken = accessToken;
      vfcTokenType = "Bearer";
      // Expires, take 10 mins before expiration
      expires = responseJSON.getLong( "expiresIn" ) * 1000 + System.currentTimeMillis( )
          - 10 * 60 * 1000;
      Logger.logInfo( "Authenticated ok" );
    }
    catch( HttpResponseException e )
    {
      if( e.getStatusCode( ) == HttpStatus.SC_FORBIDDEN )
      {
        throw new VfcClientException(
            "Unauthorized. Provided clientid and clientsecret are not valid." );
      }
      throw new VfcClientException( "Cannot authenticate:" + e.getReasonPhrase( ), e );
    }
    catch( Exception e )
    {
      throw new VfcClientException( "Cannot authenticate:" + e.getMessage( ), e );
    }
  }

  private void verifyEmailVerifiedFirebase( String apiKey, String accessToken )
      throws HttpResponseExceptionWithEntity,
      ParseException,
      IOException,
      VfcClientException,
      URISyntaxException
  {
    String accountUrl = vfcProperties.getProperty( "authentication.firebase.url.account",
        "https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo" );

    List<Pair<String, String>> queryParams = new ArrayList<>( );
    queryParams.add( new ImmutablePair<String, String>( "key", apiKey ) );

    JSONObject postParams = new JSONObject( );
    postParams.put( "idToken", accessToken );

    JSONObject responseJSON = httpClient.postJson( null, new URI( accountUrl ), queryParams,
        postParams );
    JSONArray jsonArray = responseJSON.getJSONArray( "users" );
    JSONObject firstUserJSON = jsonArray.getJSONObject( 0 );
    boolean emailVerified = firstUserJSON.getBoolean( "emailVerified" );
    if( !emailVerified )
    {
      throw new VfcClientException( "Email is not verified" );
    }
  }

  @Override
  public String getAccessToken( ) throws VfcClientException
  {
    if( System.currentTimeMillis( ) > expires )
    {
      refreshAccessToken( );
    }
    return vfcAccessToken;
  }

  @Override
  public String getTokenType( )
  {
    return vfcTokenType;
  }

}
