package com.koivusolutions.vfc.client;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.VfcProperties;
import com.koivusolutions.vfc.authentication.Authentication;
import com.koivusolutions.vfc.authentication.KoivuCloudIntegrationIdentityAuthentication;
import com.koivusolutions.vfc.client.cloudtable.CloudTables;
import com.koivusolutions.vfc.client.globalcloudtables.GlobalCloudTables;
import com.koivusolutions.vfc.client.portalmanagement.PortalManagement;
import com.koivusolutions.vfc.client.requests.VfcRequests;
import com.koivusolutions.vfc.client.solutioncloudtables.SolutionCloudTables;
import com.koivusolutions.vfc.client.system.SystemTables;

public class VfcClient
{
  private final VfcServerConnection connection;
  private final String tenant;
  private final String solution;
  private final String branch;
  private final String impersonateBusiness;

  private Authentication auth;

  public VfcClient( URL vfcURL, VfcProperties vfcProperties, String vfcClientId,
      String vfcClientSecret ) throws VfcClientException
  {
    this( vfcURL, vfcProperties );
    auth = new KoivuCloudIntegrationIdentityAuthentication( vfcProperties,
        connection.getHttpClient( ), vfcClientId, vfcClientSecret );
  }

  public VfcClient( URL vfcURL, VfcProperties vfcProperties, Authentication customAuthentication )
      throws VfcClientException
  {
    this( vfcURL, vfcProperties );
    auth = customAuthentication;
    if( auth == null )
    {
      throw new VfcClientException( "Authentication class is null" );
    }
  }

  public VfcClient( VfcServerConnection connection, VfcProperties vfcProperties,
      Authentication customAuthentication ) throws VfcClientException
  {
    this( connection, vfcProperties );
    auth = customAuthentication;
    if( auth == null )
    {
      throw new VfcClientException( "Authentication class is null" );
    }
  }

  private VfcClient( URL vfcURL, VfcProperties vfcProperties )
  {
    this( new VfcServerConnection( vfcURL, vfcProperties ), vfcProperties );
  }

  private VfcClient( VfcServerConnection connection, VfcProperties vfcProperties )
  {
    this.tenant = vfcProperties.getProperty( "vfc.api.tenant" );
    this.solution = vfcProperties.getProperty( "vfc.api.solution" );
    this.branch = vfcProperties.getProperty( "vfc.api.branch" );
    this.impersonateBusiness = vfcProperties.getProperty( "vfc.api.impersonatebusiness" );
    this.connection = connection;
  }

  public VfcRequests getVfcRequest( )
  {
    return new VfcRequests( this );
  }

  public GlobalCloudTables getGlobalCloudTables( )
  {
    return new GlobalCloudTables( this );
  }

  public SolutionCloudTables getSolutionCloudTables( )
  {
    return new SolutionCloudTables( this );
  }

  public CloudTables getCloudTables( )
  {
    return new CloudTables( this );
  }

  public PortalManagement getPortalManagement( )
  {
    return new PortalManagement( this );
  }

  public SystemTables getSystemTables( )
  {
    return new SystemTables( this );
  }

  private List<Pair<String, String>> setEnvironment( List<Pair<String, String>> queryParams )
  {
    if( queryParams == null )
    {
      queryParams = new ArrayList<>( );
    }

    addQueryParam( queryParams, "maxcount", String.valueOf( getBatchSize( ) ) );
    addQueryParam( queryParams, "tenant", tenant );
    addQueryParam( queryParams, "solution", solution );
    addQueryParam( queryParams, "branch", branch );
    addQueryParam( queryParams, "impersonatebusiness", impersonateBusiness );
    return queryParams;
  }

  Future<JSONObject> readFromVfcAsync( String resourceURL, List<Pair<String, String>> queryParams )
  {
    queryParams = setEnvironment( queryParams );
    return read( resourceURL, queryParams );
  }

  private Future<JSONObject> read( String resourceURL, List<Pair<String, String>> queryParams )
  {
    Callable<JSONObject> request = new Callable<JSONObject>( ) {
      @Override
      public JSONObject call( ) throws Exception
      {
        return connection.getHttpClient( ).getJson( auth, connection.getResourceURI( resourceURL ),
            queryParams );
      }

    };
    return connection.getExecutor( ).submit( request );
  }

  Future<JSONObject> sendToVfcAsync( String resourceURL, List<Pair<String, String>> queryParams,
      JSONObject data, UpdateStrategy strategy )
  {
    queryParams = setEnvironment( queryParams );
    return write( resourceURL, queryParams, data, strategy );
  }

  private Future<JSONObject> write( String resourceURL, List<Pair<String, String>> queryParams,
      JSONObject data, UpdateStrategy strategy )
  {
    Callable<JSONObject> request = new Callable<JSONObject>( ) {
      @Override
      public JSONObject call( ) throws Exception
      {
        switch( strategy )
        {
          case DELETE:
          {
            return connection.getHttpClient( ).deleteJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
          case UPDATE:
          {
            return connection.getHttpClient( ).putJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
          case REPLACE:
          {
            addQueryParam( queryParams, "replace", "true" );
            return connection.getHttpClient( ).postJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
          case PATCH:
          {
            return connection.getHttpClient( ).patchJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
          case PATCH_REPLACE:
          {
            addQueryParam( queryParams, "replace", "true" );
            return connection.getHttpClient( ).patchJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
          case INSERT:
          default:
          {
            return connection.getHttpClient( ).postJson( auth,
                connection.getResourceURI( resourceURL ), queryParams, data );
          }
        }
      }
    };
    return connection.getExecutor( ).submit( request );
  }

  void addQueryParam( List<Pair<String, String>> queryParams, String qpName, String qpValue )
  {
    if( StringUtils.isNotBlank( qpValue ) && !hasParameter( queryParams, qpName ) )
    {
      queryParams.add( new ImmutablePair<String, String>( qpName, qpValue ) );
    }
  }

  private boolean hasParameter( List<Pair<String, String>> queryParams, String name )
  {
    if( queryParams != null )
    {
      for( Pair<String, String> param : queryParams )
      {
        if( name.equals( param.getKey( ) ) )
        {
          return true;
        }
      }
    }
    return false;
  }

  int getBatchSize( )
  {
    return connection.getBatchSize( );
  }

  public void close( )
  {
    connection.close( );
  }

  public String getTenant( )
  {
    return tenant;
  }

  public String getSolution( )
  {
    return solution;
  }

  public String getBranch( )
  {
    return branch;
  }

  public String getImpersonateBusiness( )
  {
    return impersonateBusiness;
  }
}
