package com.koivusolutions.vfc.client.system;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcObject;

public abstract class SystemTableWithAll<T extends VfcObject> extends SystemTable<T>
{

  public SystemTableWithAll( VfcClient vfcClient, String URL )
  {
    super( vfcClient, URL );
  }

  public SystemTableRows<T> get( )
  {
    List<Pair<String, String>> queryParams = addQueryParams( null, null, null, null );
    return new SystemTableRows<T>( this, vfcClient, queryParams );
  }
}
