package com.koivusolutions.vfc.client.portalmanagement;

import java.util.Map;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class RoleGroupRow extends ImmutableRoleRow implements VfcObject
{
  public RoleGroupRow( )
  {
    super( );
  }

  public RoleGroupRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }


  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableRoleRow asImmutableRoleRow( )
  {
    return this;
  }
}
