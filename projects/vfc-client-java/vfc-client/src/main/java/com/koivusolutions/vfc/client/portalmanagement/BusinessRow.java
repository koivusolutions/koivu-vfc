package com.koivusolutions.vfc.client.portalmanagement;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class BusinessRow extends ImmutableBusinessRow implements VfcObject
{
  public BusinessRow( )
  {
    super( );
  }

  public BusinessRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setOwnerBusiness( boolean owner )
  {
    setFieldAsBoolean( SYSTEM_OWNER_BUSINESS, owner );
  }

  public void setSubTenant( String subTenantId )
  {
    setFieldAsString( SYSTEM_SUB_TENANT, subTenantId );
  }

  public void setImpersonatingTargets( String impersonatingTargets )
  {
    setFieldAsString( SYSTEM_IMPERSONATING_TARGETS, impersonatingTargets );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_OWNER_BUSINESS, column ) )
      {
        setOwnerBusiness( Boolean.valueOf( newValue ).booleanValue( ) );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_SUB_TENANT, column ) )
      {
        setSubTenant( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_IMPERSONATING_TARGETS, column ) )
      {
        setImpersonatingTargets( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableBusinessRow asImmutableBusinessRow( )
  {
    return this;
  }
}
