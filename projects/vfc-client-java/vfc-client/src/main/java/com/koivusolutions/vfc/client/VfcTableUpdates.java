package com.koivusolutions.vfc.client;

public interface VfcTableUpdates<T extends VfcObject>
{

  VfcUpdater<T> deleteFrom( );

  VfcUpdater<T> insertTo( );

  VfcUpdater<T> update( );

  VfcUpdater<T> patch( );

}