package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutableCreationPrivilegeRow extends ImmutablePortalManagementTableRow
{
  public static final String SYSTEM_CREATION_ROLE = "creationRole";

  public ImmutableCreationPrivilegeRow( )
  {
    super( );
  }

  public ImmutableCreationPrivilegeRow( JSONObject obj )
  {
    super( obj );
  }

  public String getCreationRole( )
  {
    return getFieldAsString( SYSTEM_CREATION_ROLE );
  }

}
