package com.koivusolutions.vfc.client.requests;

import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.UpdateStrategy;
import com.koivusolutions.vfc.client.VfcClient;

public class VfcRequests
{
  private final VfcClient vfcClient;

  public VfcRequests( VfcClient vfcClient )
  {
    this.vfcClient = vfcClient;
  }

  public VfcRequest makeGetRequests( String URL, Map<String, String[]> queryParams )
  {
    return makeRequest( null, URL, queryParams, null );
  }

  public VfcRequest makePostRequests( String URL, Map<String, String[]> queryParams )
  {
    return makeRequest( UpdateStrategy.INSERT, URL, queryParams, null );
  }

  public VfcRequest makePostRequests( String URL, Map<String, String[]> queryParams, String body )
  {
    return makeRequest( UpdateStrategy.INSERT, URL, queryParams, toJson( body ) );
  }

  public VfcRequest makePostRequests( String URL, Map<String, String[]> queryParams,
      JSONObject body )
  {
    return makeRequest( UpdateStrategy.INSERT, URL, queryParams, body );
  }

  public VfcRequest makePutRequests( String URL, Map<String, String[]> queryParams )
  {
    return makeRequest( UpdateStrategy.UPDATE, URL, queryParams, null );
  }

  public VfcRequest makePutRequests( String URL, Map<String, String[]> queryParams, String body )
  {
    return makeRequest( UpdateStrategy.UPDATE, URL, queryParams, toJson( body ) );
  }

  public VfcRequest makePutRequests( String URL, Map<String, String[]> queryParams,
      JSONObject body )
  {
    return makeRequest( UpdateStrategy.UPDATE, URL, queryParams, body );
  }

  public VfcRequest makePatchRequests( String URL, Map<String, String[]> queryParams )
  {
    return makeRequest( UpdateStrategy.PATCH, URL, queryParams, null );
  }

  public VfcRequest makePatchRequests( String URL, Map<String, String[]> queryParams, String body )
  {
    return makeRequest( UpdateStrategy.PATCH, URL, queryParams, toJson( body ) );
  }

  public VfcRequest makePatchRequests( String URL, Map<String, String[]> queryParams,
      JSONObject body )
  {
    return makeRequest( UpdateStrategy.PATCH, URL, queryParams, body );
  }

  public VfcRequest makeDeleteRequests( String URL, Map<String, String[]> queryParams )
  {
    return makeRequest( UpdateStrategy.DELETE, URL, queryParams, null );
  }

  public VfcRequest makeDeleteRequests( String URL, Map<String, String[]> queryParams, String body )
  {
    return makeRequest( UpdateStrategy.DELETE, URL, queryParams, toJson( body ) );
  }

  public VfcRequest makeDeleteRequests( String URL, Map<String, String[]> queryParams,
      JSONObject body )
  {
    return makeRequest( UpdateStrategy.DELETE, URL, queryParams, body );
  }

  private VfcRequest makeRequest( UpdateStrategy method, String URL,
      Map<String, String[]> queryParams, JSONObject body )
  {
    return new VfcRequest( vfcClient, URL, method, addParams( queryParams ), body );
  }

  private JSONObject toJson( String body )
  {
    if( StringUtils.isNotBlank( body ) )
    {
      return new JSONObject( body );
    }
    return null;
  }

  private List<Pair<String, String>> addParams( Map<String, String[]> queryParamsMap )
  {
    if( queryParamsMap != null )
    {
      List<Pair<String, String>> queryParams = new ArrayList<>( queryParamsMap.size( ) );
      for( Entry<String, String[]> pair : queryParamsMap.entrySet( ) )
      {
        String name = pair.getKey( );
        String[] values = pair.getValue( );
        if( ArrayUtils.isNotEmpty( values ) )
        {
          for( String value : values )
          {
            queryParams
                .add( new ImmutablePair<String, String>( name, value != null ? value : "" ) );
          }
        }
      }
      return queryParams;
    }
    return null;
  }

}
