package com.koivusolutions.vfc.client.cloudtable;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.koivusolutions.vfc.client.*;

public class NamedCloudTable implements VfcTableUpdates<CloudTableRow>
{
  private final VfcClient vfcClient;
  final String URLPrefix;
  private final String name;
  private final String category;

  public NamedCloudTable( VfcClient vfcClient, String name, String URLPrefix, String category )
  {
    this.vfcClient = vfcClient;
    this.name = name;
    this.URLPrefix = URLPrefix;
    this.category = category;
  }

  public CloudTable getCloudTable( String keys[] )
  {
    return getCloudTable( keys, null, true, null, null, false );
  }

  public CloudTable getCloudTable( String keys[], String filter )
  {
    return getCloudTable( keys, filter, true, null, null, false );
  }

  public CloudTable getCloudTable( String keys[], String filter, String newerThanTime )
  {
    return getCloudTable( keys, filter, true, newerThanTime, null, false );
  }

  public CloudTable getCloudTable( String keys[], String filter, boolean filterByLevels,
      String newerThanTime, String expressions, boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "key", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, includedeleted,
        queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableKeysStartingWith( String keys[] )
  {
    return getCloudTableKeysStartingWith( keys, null, true, null, null, false );
  }

  public CloudTable getCloudTableKeysStartingWith( String keys[], String filter )
  {
    return getCloudTableKeysStartingWith( keys, filter, true, null, null, false );
  }

  public CloudTable getCloudTableKeysStartingWith( String keys[], String filter,
      String newerThanTime )
  {
    return getCloudTableKeysStartingWith( keys, filter, true, newerThanTime, null, false );
  }

  public CloudTable getCloudTableKeysStartingWith( String keys[], String filter,
      boolean filterByLevels, String newerThanTime, String expressions, boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "startswith", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, includedeleted,
        queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableExactKeys( String keys[] )
  {
    return getCloudTableExactKeys( keys, null, true, null, null, false );
  }

  public CloudTable getCloudTableExactKeys( String keys[], String filter )
  {
    return getCloudTableExactKeys( keys, filter, true, null, null, false );
  }

  public CloudTable getCloudTableExactKeys( String keys[], String filter, String newerThanTime )
  {
    return getCloudTableExactKeys( keys, filter, true, newerThanTime, null, false );
  }

  public CloudTable getCloudTableExactKeys( String keys[], String filter, boolean filterByLevels,
      String newerThanTime, String expressions, boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keys != null )
    {
      for( String key : keys )
      {
        addParam( queryParams, "exactkey", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, includedeleted,
        queryParams );

    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  public CloudTable getCloudTableWithKeyRange( String keysFrom[], String[] keysTo )
  {
    return getCloudTableWithKeyRange( keysFrom, keysTo, null, true, null, null, false );
  }

  public CloudTable getCloudTableWithKeyRange( String keysFrom[], String[] keysTo, String filter )
  {
    return getCloudTableWithKeyRange( keysFrom, keysTo, filter, true, null, null, false );
  }

  public CloudTable getCloudTableWithKeyRange( String keysFrom[], String[] keysTo, String filter,
      String newerThanTime )
  {
    return getCloudTableWithKeyRange( keysFrom, keysTo, filter, true, newerThanTime, null, false );
  }

  public CloudTable getCloudTableWithKeyRange( String keysFrom[], String[] keysTo, String filter,
      boolean filterByLevels, String newerThanTime, String expressions, boolean includedeleted )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    if( keysFrom != null )
    {
      for( String key : keysFrom )
      {
        addParam( queryParams, "keyfrom", key );
      }
    }
    if( keysTo != null )
    {
      for( String key : keysTo )
      {
        addParam( queryParams, "keyto", key );
      }
    }
    addQueryParams( filter, filterByLevels, newerThanTime, expressions, includedeleted,
        queryParams );
    return new CloudTable( vfcClient, name, URLPrefix, category, queryParams );
  }

  private void addParam( List<Pair<String, String>> queryParams, String paramName,
      String paramValue )
  {
    if( StringUtils.isNotBlank( paramValue ) )
    {
      queryParams.add( new ImmutablePair<String, String>( paramName, paramValue ) );
    }
  }

  public CloudTableUpdater<CloudTableRow> deleteFromCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.DELETE,
        URLPrefix + category );
  }

  public CloudTableUpdater<CloudTableRow> insertToTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.INSERT,
        URLPrefix + category );
  }

  public CloudTableUpdater<CloudTableRow> updateCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.UPDATE,
        URLPrefix + category );
  }

  public CloudTableUpdater<CloudTableRow> patchCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.PATCH,
        URLPrefix + category );
  }

  public CloudTableUpdater<CloudTableRow> patchReplaceCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.PATCH_REPLACE,
        URLPrefix + category );
  }

  public CloudTableUpdater<CloudTableRow> replaceInCloudTable( )
  {
    return new CloudTableUpdater<CloudTableRow>( vfcClient, UpdateStrategy.REPLACE,
        URLPrefix + category );
  }

  private void addQueryParams( String filter, boolean filterByLevels, String newerThanTime,
      String expressions, boolean includedeleted, List<Pair<String, String>> queryParams )
  {
    addParam( queryParams, "filter", filter );
    addParam( queryParams, "newerthan", newerThanTime );
    addParam( queryParams, "expression", expressions );
    if( includedeleted )
    {
      addParam( queryParams, "includedeleted", "true" );
    }
    if( !filterByLevels )
    {
      addParam( queryParams, "filterbylevels", "false" );
    }
  }

  @Override
  public VfcUpdater<CloudTableRow> deleteFrom( )
  {
    return this.deleteFromCloudTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> insertTo( )
  {
    return this.insertToTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> update( )
  {
    return this.updateCloudTable( );
  }

  @Override
  public VfcUpdater<CloudTableRow> patch( )
  {
    return this.patchCloudTable( );
  }
}
