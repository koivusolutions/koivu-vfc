package com.koivusolutions.vfc.logging;

public class Logger
{
  private static Log logger = null;

  public static void setExternalLogger( Log log )
  {
    logger = log;
  }

  private static void log( String msg )
  {
    if( logger == null )
    {
      System.out.println( msg );
    }
  }

  private static void logErrorMsg( String msg )
  {
    if( logger == null )
    {
      System.err.println( msg );
    }
  }

  private static void logErrorMsg( String msg, Throwable t )
  {
    if( logger == null )
    {
      System.err.println( msg );
      t.printStackTrace( );
    }
  }

  public static void logError( String msg, Throwable t )
  {
    logErrorMsg( msg, t );
    if( logger != null )
    {
      logger.logError( msg, t );
    }
  }

  public static void logError( String msg )
  {
    logErrorMsg( msg );
    if( logger != null )
    {
      logger.logError( msg );
    }
  }

  public static void logWarn( String msg )
  {
    log( msg );
    if( logger != null )
    {
      logger.logWarn( msg );
    }
  }

  public static void logInfo( String msg )
  {
    log( msg );
    if( logger != null )
    {
      logger.logInfo( msg );
    }
  }

  public static void logDebug( String msg )
  {
    log( msg );
    if( logger != null )
    {
      logger.logDebug( msg );
    }
  }

}
