package com.koivusolutions.vfc.client;

import org.json.JSONObject;

public abstract class VfcGetResponse extends VfcResponse
{

  public VfcGetResponse( VfcClient vfcClient, String URL )
  {
    super( vfcClient, URL );
  }

  @Override
  final protected UpdateStrategy getMethod( )
  {
    return null;
  }

  @Override
  final protected JSONObject getData( )
  {
    return null;
  }

}
