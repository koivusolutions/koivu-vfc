package com.koivusolutions.vfc.client.integrations;

public class ColumnValue
{
  private final Column column;
  private final String formattedValue;

  public ColumnValue( Column column, String formattedValue )
  {
    super( );
    this.column = column;
    this.formattedValue = formattedValue;
  }

  public Column getColumn( )
  {
    return column;
  }

  public String getFormattedValue( )
  {
    return formattedValue;
  }

}
