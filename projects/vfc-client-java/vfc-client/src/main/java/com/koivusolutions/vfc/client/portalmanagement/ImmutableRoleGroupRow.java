package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutableRoleGroupRow extends ImmutablePortalManagementTableRow
{

  public ImmutableRoleGroupRow( )
  {
    super( );
  }

  public ImmutableRoleGroupRow( JSONObject obj )
  {
    super( obj );
  }

}
