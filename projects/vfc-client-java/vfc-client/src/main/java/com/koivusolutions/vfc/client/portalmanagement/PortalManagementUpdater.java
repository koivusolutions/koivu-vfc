package com.koivusolutions.vfc.client.portalmanagement;

import com.koivusolutions.vfc.client.*;

public class PortalManagementUpdater<T extends VfcObject> extends VfcUpdateResponse<T>
{
  private final String URL;

  public PortalManagementUpdater( VfcClient vfcClient, String URL, UpdateStrategy updateStrategy )
  {
    super( vfcClient, updateStrategy );
    this.URL = URL;
  }

  @Override
  public VfcUpdater<T> addUpdate( T row )
  {
    return super.addUpdate( row );
  }

  @Override
  protected String getUrl( )
  {
    return URL;
  }

}
