package com.koivusolutions.vfc.client.requests;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;

public class VfcRequest extends VfcResponse
{
  private final List<Pair<String, String>> query;
  private final JSONObject data;
  private final UpdateStrategy method;

  VfcRequest( VfcClient vfcClient, String url, UpdateStrategy method,
      List<Pair<String, String>> query, JSONObject data )
  {
    super( vfcClient, url );
    this.method = method;
    this.query = Collections.unmodifiableList( query );
    this.data = data;
    initialize( );
  }

  @Override
  public String toString( )
  {
    return "VfcCall [" + getUrl( ) + " query=" + query + " has body:" + ( data != null ) + "]";
  }

  @Override
  public List<Pair<String, String>> getQueryParams( )
  {
    return query;
  }

  @Override
  public UpdateStrategy getMethod( )
  {
    return method;
  }

  @Override
  public JSONObject getData( )
  {
    return data;
  }

  public VfcIterator<JSONObject> getReponseBody( )
  {
    VfcIterator<JSONObject> jsonIt = super.read( );
    return jsonIt;
  }

}
