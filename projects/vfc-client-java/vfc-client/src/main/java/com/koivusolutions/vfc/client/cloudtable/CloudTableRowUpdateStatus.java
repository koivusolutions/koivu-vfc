package com.koivusolutions.vfc.client.cloudtable;

import com.koivusolutions.vfc.client.VfcRowUpdateStatus;

public class CloudTableRowUpdateStatus extends VfcRowUpdateStatus<CloudTableRow>
{
  public CloudTableRowUpdateStatus( CloudTableRow row )
  {
    super( row );
  }
}
