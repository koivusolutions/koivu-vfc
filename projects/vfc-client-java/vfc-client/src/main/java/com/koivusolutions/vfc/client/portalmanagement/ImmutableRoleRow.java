package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

public class ImmutableRoleRow extends ImmutablePortalManagementTableRow
{

  public ImmutableRoleRow( )
  {
    super( );
  }

  public ImmutableRoleRow( JSONObject obj )
  {
    super( obj );
  }

}
