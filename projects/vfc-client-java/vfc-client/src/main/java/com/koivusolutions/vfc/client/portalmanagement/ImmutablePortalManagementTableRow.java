package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.KeyableObject;

abstract class ImmutablePortalManagementTableRow extends KeyableObject
{
  public static final String ACTIVE = "active";

  protected ImmutablePortalManagementTableRow( )
  {
    this( new JSONObject( ) );
  }

  protected ImmutablePortalManagementTableRow( JSONObject obj )
  {
    super( obj );
  }

  public boolean isActive( )
  {
    return getFieldAsBoolean( ACTIVE );
  }

  @Override
  public String toString( )
  {
    return this.getClass( ).getSimpleName( ) + "[" + super.getAsJson( ).toString( ) + "]";
  }


}