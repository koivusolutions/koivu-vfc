package com.koivusolutions.vfc.client;

public class VfcRowUpdateStatus<T extends VfcObject> extends VfcQueryUpdateStatus
{
  private final T row;

  public VfcRowUpdateStatus( T row )
  {
    this.row = row;
  }

  public T getRow( )
  {
    return row;
  }

}
