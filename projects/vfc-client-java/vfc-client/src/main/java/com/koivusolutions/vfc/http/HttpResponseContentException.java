package com.koivusolutions.vfc.http;

import org.json.JSONObject;

public class HttpResponseContentException extends HttpResponseExceptionWithEntity
{
  public HttpResponseContentException( int statusCode, String reasonPhrase,
      JSONObject responseEntity )
  {
    super( statusCode, reasonPhrase, responseEntity );
  }

  @Override
  public String getMessage( )
  {
    return String.format( "Response is NOT as expected: %s", super.getMessage( ) );
  }

}
