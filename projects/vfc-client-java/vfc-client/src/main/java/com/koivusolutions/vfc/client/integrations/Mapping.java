package com.koivusolutions.vfc.client.integrations;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public class Mapping
{
  private static final String VALUE = "value";

  private final MappingKey key;
  private final String value;

  public Mapping( MappingKey key, CloudTableRow properties )
  {
    super( );
    this.key = key;
    this.value = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, VALUE, null );
  }

  public MappingKey getKey( )
  {
    return key;
  }

  public String getValue( )
  {
    return value;
  }
}
