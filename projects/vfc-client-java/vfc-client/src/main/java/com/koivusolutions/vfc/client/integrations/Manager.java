package com.koivusolutions.vfc.client.integrations;

import java.text.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.koivusolutions.vfc.client.VfcClientException;
import com.koivusolutions.vfc.client.VfcIterator;
import com.koivusolutions.vfc.client.cloudtable.CloudTable;
import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public class Manager
{
  private static final String DEFAULT_COLUMN_ERROR_MESSAGE = "Error in column: ''{0}''";
  private static final String DEFAULT_GENERIC_ERROR_MESSAGE = "Error";
  private static final int MAX_VARIABLE_INDEX = 10;

  private static final DateFormat DATE_FORMAT = new SimpleDateFormat( "yyyy-MM-dd" );

  private final ManagerParams params;
  private Map<PropertyKey, Property> properties;
  private Map<ColumnKey, Column> columns;
  private Map<MappingKey, Mapping> mappings;

  private CloudTableRow activeRow;
  private List<String> activeRowErrors;

  public Manager( ManagerParams params )
  {
    super( );
    this.params = params;
  }

  public void setActiveRow( CloudTableRow activeRow )
  {
    this.activeRow = activeRow;
    activeRowErrors = new ArrayList<String>( );
  }

  public List<String> getActiveRowErrors( )
  {
    return activeRowErrors;
  }

  public void addActiveRowError( String error )
  {
    activeRowErrors.add( error );
  }

  public void addColumnErrorMessage( Column column )
  {
    String error = null;
    if( column != null )
    {
      error = column.getError( );
    }
    if( StringUtils.isBlank( error ) )
    {
      if( column != null )
      {
        error = MessageFormat.format( DEFAULT_COLUMN_ERROR_MESSAGE, column.getColumn( ) );
      }
      else
      {
        error = DEFAULT_GENERIC_ERROR_MESSAGE;
      }
    }
    activeRowErrors.add( error );
  }

  public Column getColumn( String category, String group, String variable, int index )
      throws VfcClientException
  {
    initializeAll( );
    return columns.get( new ColumnKey( params.getId( ), category, group, variable, index ) );
  }

  public Mapping getMapping( String category, String group, String variable, String key )
      throws VfcClientException
  {
    initializeAll( );
    return mappings.get( new MappingKey( params.getId( ), category, group, variable, key ) );
  }

  public Map<ColumnKey, Column> getColumns( ) throws VfcClientException
  {
    initializeAll( );
    return columns;
  }

  public String getPropertyAsString( String property, String defaultValue )
      throws VfcClientException
  {
    initializeProperties( );
    String value = null;
    Property propertyValue = properties.get( new PropertyKey( params.getId( ), property ) );
    if( propertyValue != null )
    {
      value = propertyValue.getValue( );
    }
    return Objects.toString( value, defaultValue );
  }

  public int getPropertyAsInteger( String property, int defaultValue ) throws VfcClientException
  {
    initializeProperties( );
    String value = null;
    Property propertyValue = properties.get( new PropertyKey( params.getId( ), property ) );
    if( propertyValue != null )
    {
      value = propertyValue.getValue( );
    }
    return NumberUtils.toInt( value, defaultValue );
  }

  public String getActiveRowValueAsString( String category, String group, String variable )
      throws VfcClientException
  {
    ColumnValue value = getActiveRowValue( category, group, variable );
    return value != null ? value.getFormattedValue( ) : null;
  }

  public Date getActiveRowValueAsDate( String category, String group, String variable )
      throws VfcClientException
  {
    String value = getActiveRowValueAsString( category, group, variable );
    if( StringUtils.isNotBlank( value ) )
    {
      try
      {
        return DATE_FORMAT.parse( value );
      }
      catch( ParseException e )
      {
      }
    }
    return null;
  }

  public Integer getActiveRowValueAsInteger( String category, String group, String variable )
      throws VfcClientException
  {
    String value = getActiveRowValueAsString( category, group, variable );
    if( StringUtils.isNotBlank( value ) )
    {
      return Integer.valueOf( NumberUtils.toInt( value ) );
    }
    return null;
  }

  public ColumnValue getActiveRowValue( String category, String group, String variable )
      throws VfcClientException
  {
    initializeAll( );
    List<String> values = new ArrayList<>( );
    Column lastColumn = null;
    for( int index = 1; index <= MAX_VARIABLE_INDEX; index++ )
    {
      Column column = columns
          .get( new ColumnKey( params.getId( ), category, group, variable, index ) );
      if( column == null )
      {
        break;
      }
      String value = activeRow.getAsString( column.getColumnType( ), column.getColumn( ),
          column.getDefaultValue( ) );
      if( StringUtils.isBlank( value ) )
      {
        if( column.isMandatory( ) )
        {
          addColumnErrorMessage( column );
          return null;
        }
        break;
      }
      if( hasAnyMapping( category, group, variable ) )
      {
        MappingKey key = new MappingKey( params.getId( ), category, group, variable, value );
        Mapping mapping = mappings.get( key );
        if( mapping != null )
        {
          value = mapping.getValue( );
        }
        else
        {
          addColumnErrorMessage( column );
          return null;
        }
      }
      lastColumn = column;
      values.add( value );
    }
    String formattedValue;
    if( lastColumn != null && !values.isEmpty( ) )
    {
      String formatPattern = lastColumn.getFormatPattern( );
      if( StringUtils.isNotBlank( formatPattern ) )
      {
        formattedValue = MessageFormat.format( formatPattern, values.toArray( ) );
      }
      else
      {
        formattedValue = values.get( values.size( ) - 1 );
      }
      return new ColumnValue( lastColumn, formattedValue );
    }
    return null;
  }

  public void setActiveRowValueAsString( String category, String group, String variable, int index,
      String value ) throws VfcClientException
  {
    Column column = getColumn( category, group, variable, index );
    if( column != null )
    {
      activeRow.setAsString( column.getColumnType( ), column.getColumn( ), value );
    }
  }

  private boolean hasAnyMapping( String category, String group, String variable )
      throws VfcClientException
  {
    initializeAll( );
    for( MappingKey key : mappings.keySet( ) )
    {
      if( key.isSameVariable( params.getId( ), category, group, variable ) )
      {
        return true;
      }
    }
    return false;
  }

  private void initializeAll( ) throws VfcClientException
  {
    readProperties( );
    readColumns( );
    readMappings( );
  }

  private void initializeProperties( ) throws VfcClientException
  {
    readProperties( );
  }

  private void readProperties( ) throws VfcClientException
  {
    if( properties != null )
    {
      return;
    }
    properties = new HashMap<>( );
    String tableName = params.getPropertiesTable( );
    Map<PropertyKey, Property> map = properties;
    CloudTable table = params.getVfc( ).getCloudTables( ).getCloudTableKeysStartingWith( tableName,
        new String[]{params.getId( )} );
    VfcIterator<CloudTableRow> it = table.iteratorEx( );
    while( it.hasNext( ) )
    {
      CloudTableRow row = it.next( );
      PropertyKey key = new PropertyKey( row.getKeys( ) );
      Property value = new Property( key, row );
      map.put( key, value );
    }
  }

  private void readColumns( ) throws VfcClientException
  {
    if( columns != null )
    {
      return;
    }
    columns = new HashMap<>( );
    String tableName = params.getColumnsTable( );
    Map<ColumnKey, Column> map = columns;
    CloudTable table = params.getVfc( ).getCloudTables( ).getCloudTableKeysStartingWith( tableName,
        new String[]{params.getId( )} );
    VfcIterator<CloudTableRow> it = table.iteratorEx( );
    while( it.hasNext( ) )
    {
      CloudTableRow row = it.next( );
      ColumnKey key = new ColumnKey( row.getKeys( ) );
      Column value = new Column( key, row );
      map.put( key, value );
    }
  }

  private void readMappings( ) throws VfcClientException
  {
    if( mappings != null )
    {
      return;
    }
    mappings = new HashMap<>( );
    String tableName = params.getMappingTable( );
    Map<MappingKey, Mapping> map = mappings;
    CloudTable table = params.getVfc( ).getCloudTables( ).getCloudTableKeysStartingWith( tableName,
        new String[]{params.getId( )} );
    VfcIterator<CloudTableRow> it = table.iteratorEx( );
    while( it.hasNext( ) )
    {
      CloudTableRow row = it.next( );
      MappingKey key = new MappingKey( row.getKeys( ) );
      Mapping value = new Mapping( key, row );
      map.put( key, value );
    }
  }

}
