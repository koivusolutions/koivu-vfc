package com.koivusolutions.vfc.client.integrations;

import org.apache.commons.lang3.StringUtils;

public class MappingKey
{
  private final String id;
  private final String category;
  private final String group;
  private final String variable;
  private final String key;

  public MappingKey( String id, String category, String group, String variable, String key )
  {
    super( );
    this.id = id;
    this.category = category;
    this.group = group;
    this.variable = variable;
    this.key = key;
  }

  public MappingKey( String[] keys )
  {
    super( );
    this.id = keys[0];
    this.category = keys[1];
    this.group = keys[2];
    this.variable = keys[3];
    this.key = keys[4];
  }

  public boolean isSameVariable( String otherId, String otherCategory, String otherGroup,
      String otherVariable )
  {
    return StringUtils.equals( this.id, otherId )
        && StringUtils.equals( this.category, otherCategory )
        && StringUtils.equals( this.group, otherGroup )
        && StringUtils.equals( this.variable, otherVariable );
  }

  public String getId( )
  {
    return id;
  }

  public String getCategory( )
  {
    return category;
  }

  public String getGroup( )
  {
    return group;
  }

  public String getVariable( )
  {
    return variable;
  }

  public String getKey( )
  {
    return key;
  }

  @Override
  public int hashCode( )
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ( ( category == null ) ? 0 : category.hashCode( ) );
    result = prime * result + ( ( group == null ) ? 0 : group.hashCode( ) );
    result = prime * result + ( ( id == null ) ? 0 : id.hashCode( ) );
    result = prime * result + ( ( key == null ) ? 0 : key.hashCode( ) );
    result = prime * result + ( ( variable == null ) ? 0 : variable.hashCode( ) );
    return result;
  }

  @Override
  public boolean equals( Object obj )
  {
    if( this == obj )
    {
      return true;
    }
    if( obj == null )
    {
      return false;
    }
    if( getClass( ) != obj.getClass( ) )
    {
      return false;
    }
    MappingKey other = (MappingKey)obj;
    if( category == null )
    {
      if( other.category != null )
      {
        return false;
      }
    }
    else if( !category.equals( other.category ) )
    {
      return false;
    }
    if( group == null )
    {
      if( other.group != null )
      {
        return false;
      }
    }
    else if( !group.equals( other.group ) )
    {
      return false;
    }
    if( id == null )
    {
      if( other.id != null )
      {
        return false;
      }
    }
    else if( !id.equals( other.id ) )
    {
      return false;
    }
    if( key == null )
    {
      if( other.key != null )
      {
        return false;
      }
    }
    else if( !key.equals( other.key ) )
    {
      return false;
    }
    if( variable == null )
    {
      if( other.variable != null )
      {
        return false;
      }
    }
    else if( !variable.equals( other.variable ) )
    {
      return false;
    }
    return true;
  }

}
