package com.koivusolutions.vfc.client.system;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.VfcObject;

public abstract class SystemTableWithKeys<T extends VfcObject>
    extends SystemTableWithFilter<T>
{

  public SystemTableWithKeys( VfcClient vfcClient, String URL )
  {
    super( vfcClient, URL );
  }

  public SystemTableRows<T> getWithStartsWith( String startingWith )
  {
    List<Pair<String, String>> queryParams = addQueryParams( null, startingWith, null, null );
    return new SystemTableRows<T>( this, vfcClient, queryParams );
  }

  public SystemTableRows<T> getWithKey( String key )
  {
    List<Pair<String, String>> queryParams = addQueryParams( key, null, null, null );
    return new SystemTableRows<T>( this, vfcClient, queryParams );
  }

}
