package com.koivusolutions.vfc.client.globalcloudtables;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.cloudtable.CloudTablesBase;

public class GlobalCloudTables extends CloudTablesBase
{

  public GlobalCloudTables( VfcClient vfcClient )
  {
    super( vfcClient, "GlobalCloudTable", "globalproperties" );
  }
}
