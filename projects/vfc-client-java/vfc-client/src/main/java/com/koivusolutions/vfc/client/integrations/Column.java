package com.koivusolutions.vfc.client.integrations;

import com.koivusolutions.vfc.client.cloudtable.CloudTableRow;

public class Column
{
  private static final String COLUMN = "column";
  private static final String COLUMN_TYPE = "columnType";
  private static final String DEFAULT_VALUE = "defaultValue";
  private static final String ERROR = "error";
  private static final String TYPE = "type";
  private static final String TYPE_DETAILS = "typeDetails";
  private static final String FORMAT_PATTERN = "formatPattern";
  private static final String MANDATORY = "mandatory";

  private final ColumnKey key;
  private final String column;
  private final String columnType;
  private final String defaultValue;
  private final String error;
  private final String type;
  private final String typeDetails;
  private final String formatPattern;
  private final boolean mandatory;

  public Column( ColumnKey key, CloudTableRow properties )
  {
    super( );
    this.key = key;
    this.column = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, COLUMN, "" );
    this.columnType = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, COLUMN_TYPE,
        "" );
    this.defaultValue = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, DEFAULT_VALUE,
        "" );
    this.error = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, ERROR, "" );
    this.type = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, TYPE, "" );
    this.typeDetails = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES, TYPE_DETAILS,
        "" );
    this.formatPattern = properties.getAsString( CloudTableRow.COLUMN_TYPE_PROPERTIES,
        FORMAT_PATTERN, "" );
    this.mandatory = properties.getAsBoolean( CloudTableRow.COLUMN_TYPE_PROPERTIES, MANDATORY,
        false );
  }

  public ColumnKey getKey( )
  {
    return key;
  }

  public String getColumn( )
  {
    return column;
  }

  public String getColumnType( )
  {
    return columnType;
  }

  public String getDefaultValue( )
  {
    return defaultValue;
  }

  public String getError( )
  {
    return error;
  }

  public String getType( )
  {
    return type;
  }

  public String getTypeDetails( )
  {
    return typeDetails;
  }

  public String getFormatPattern( )
  {
    return formatPattern;
  }

  public boolean isMandatory( )
  {
    return mandatory;
  }

}
