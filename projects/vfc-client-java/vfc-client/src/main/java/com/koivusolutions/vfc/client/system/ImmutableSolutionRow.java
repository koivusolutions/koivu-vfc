package com.koivusolutions.vfc.client.system;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.objects.IdObject;

public class ImmutableSolutionRow extends IdObject
{
  public static final String SYSTEM_NAME = "name";
  public static final String SYSTEM_DEFAULT_SOLUTION = "defaultSolution";

  public ImmutableSolutionRow( )
  {
    super( new JSONObject( ) );
  }

  public ImmutableSolutionRow( JSONObject obj )
  {
    super( obj );
  }

  public boolean isDefaultSolution( )
  {
    return getFieldAsBoolean( SYSTEM_DEFAULT_SOLUTION );
  }

  public String getName( )
  {
    return getFieldAsString( SYSTEM_NAME );
  }

}
