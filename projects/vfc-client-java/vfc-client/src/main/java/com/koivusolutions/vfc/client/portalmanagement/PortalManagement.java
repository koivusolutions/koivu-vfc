package com.koivusolutions.vfc.client.portalmanagement;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClient;
import com.koivusolutions.vfc.client.cloudtable.NamedCloudTable;

public class PortalManagement
{
  private final VfcClient vfcClient;

  public PortalManagement( VfcClient vfcClient )
  {
    this.vfcClient = vfcClient;
  }

  public PortalManagementTable<UserRow> getUsers( )
  {
    return new PortalManagementTable<UserRow>( vfcClient, "portalmanagement/users" ) {

      @Override
      protected UserRow build( JSONObject obj )
      {
        return new UserRow( obj );
      }
    };
  }

  public PortalManagementTable<BusinessRow> getBusinesses( )
  {
    return new PortalManagementTable<BusinessRow>( vfcClient, "portalmanagement/businesses" ) {

      @Override
      protected BusinessRow build( JSONObject obj )
      {
        return new BusinessRow( obj );
      }
    };
  }

  public PortalManagementTable<CreationPrivilegeRow> getCreationPrivileges( )
  {
    return new PortalManagementTable<CreationPrivilegeRow>( vfcClient,
        "portalmanagement/creationprivileges" ) {

      @Override
      protected CreationPrivilegeRow build( JSONObject obj )
      {
        return new CreationPrivilegeRow( obj );
      }
    };
  }

  public PortalManagementTable<PrivilegeRow> getPrivileges( )
  {
    return new PortalManagementTable<PrivilegeRow>( vfcClient, "portalmanagement/privileges" ) {

      @Override
      protected PrivilegeRow build( JSONObject obj )
      {
        return new PrivilegeRow( obj );
      }
    };
  }

  public PortalManagementTable<RoleGroupRow> getRoleGroups( )
  {
    return new PortalManagementTable<RoleGroupRow>( vfcClient, "portalmanagement/rolegroups" ) {

      @Override
      protected RoleGroupRow build( JSONObject obj )
      {
        return new RoleGroupRow( obj );
      }
    };
  }

  public PortalManagementTable<RoleRow> getRoles( )
  {
    return new PortalManagementTable<RoleRow>( vfcClient, "portalmanagement/roles" ) {

      @Override
      protected RoleRow build( JSONObject obj )
      {
        return new RoleRow( obj );
      }
    };
  }

  public NamedCloudTable getTableDefinition( )
  {
    return new NamedCloudTable( vfcClient, "PortalManagementTableDefinition",
        "portalmanagement/properties/", "§propertiesCategories" );
  }

  public NamedCloudTable getColumnDefinition( )
  {
    return new NamedCloudTable( vfcClient, "PortalManagementColumnDefinition",
        "portalmanagement/properties/", "ColumnDefinitions" );
  }

}
