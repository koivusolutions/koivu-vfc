package com.koivusolutions.vfc.client.objects;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

public abstract class IdObject extends BaseObject
{
  public static final String SYSTEM_ID = "id";

  public IdObject( JSONObject obj )
  {
    super( obj );
  }

  public String getId( )
  {
    return getFieldAsString( SYSTEM_ID );
  }

  protected void setId( String id )
  {
    setFieldAsString( SYSTEM_ID, id );
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_ID, column ) )
      {
        return getId( );
      }
    }
    return super.getAsString( columnType, column, defaultValue );
  }

  @Override
  protected void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_ID, column ) )
      {
        setId( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }
}