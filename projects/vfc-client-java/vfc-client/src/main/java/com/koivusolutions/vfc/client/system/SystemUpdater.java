package com.koivusolutions.vfc.client.system;

import com.koivusolutions.vfc.client.*;

public class SystemUpdater<T extends VfcObject> extends VfcUpdateResponse<T>
{
  private final String URL;

  public SystemUpdater( VfcClient vfcClient, String URL, UpdateStrategy updateStrategy )
  {
    super( vfcClient, updateStrategy );
    this.URL = URL;
  }

  @Override
  public VfcUpdater<T> addUpdate( T row )
  {
    return super.addUpdate( row );
  }

  @Override
  protected String getUrl( )
  {
    return URL;
  }

}
