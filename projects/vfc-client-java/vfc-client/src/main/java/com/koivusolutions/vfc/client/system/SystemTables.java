package com.koivusolutions.vfc.client.system;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcClient;

public class SystemTables
{
  private final VfcClient vfcClient;

  public SystemTables( VfcClient vfcClient )
  {
    this.vfcClient = vfcClient;
  }

  public SystemTableWithAll<SolutionRow> getSolutions( )
  {
    return new SystemTableWithAll<SolutionRow>( vfcClient, "solutions" ) {

      @Override
      protected SolutionRow build( JSONObject obj )
      {
        return new SolutionRow( obj );
      }
    };
  }

  public SystemTableWithImpersonate<BranchRow> getBranches( )
  {
    return new SystemTableWithImpersonate<BranchRow>( vfcClient, "branches" ) {

      @Override
      protected BranchRow build( JSONObject obj )
      {
        return new BranchRow( obj );
      }
    };
  }

  public SystemTableWithKeys<SecretRow> getSecrets( )
  {
    return new SystemTableWithKeys<SecretRow>( vfcClient, "secrets" ) {

      @Override
      protected SecretRow build( JSONObject obj )
      {
        return new SecretRow( obj );
      }
    };
  }

  public SystemTableWithKeys<TaskRow> getTasks( )
  {
    return new SystemTableWithKeys<TaskRow>( vfcClient, "tasks" ) {

      @Override
      protected TaskRow build( JSONObject obj )
      {
        return new TaskRow( obj );
      }
    };
  }

  public SystemTableWithKeys<ChatRow> getChats( )
  {
    return new SystemTableWithKeys<ChatRow>( vfcClient, "chats" ) {

      @Override
      protected ChatRow build( JSONObject obj )
      {
        return new ChatRow( obj );
      }
    };
  }

  public SystemTableWithAll<SequenceRow> getSequences( )
  {
    return new SystemTableWithAll<SequenceRow>( vfcClient, "sequences" ) {

      @Override
      protected SequenceRow build( JSONObject obj )
      {
        return new SequenceRow( obj );
      }
    };
  }
}
