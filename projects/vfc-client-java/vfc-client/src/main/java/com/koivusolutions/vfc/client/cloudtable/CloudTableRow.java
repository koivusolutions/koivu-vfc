package com.koivusolutions.vfc.client.cloudtable;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class CloudTableRow extends ImmutableCloudTableRow implements VfcObject
{
  public CloudTableRow( )
  {
    super( );
  }

  public CloudTableRow( JSONObject obj )
  {
    super( obj );
  }

  public void setCategory( String category )
  {
    setFieldAsString( SYSTEM_CATEGORY, category );
  }

  @Override
  public void setKeys( String... keys )
  {
    super.setKeys( keys );
  }

  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setDescription( String description )
  {
    setFieldAsString( SYSTEM_DESCRIPTION, description );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_CATEGORY, column ) )
      {
        setCategory( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_DESCRIPTION, column ) )
      {
        setDescription( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  @Override
  public void setAsDate( String columnType, String column, Date newValue )
  {
    super.setAsDate( columnType, column, newValue );
  }

  @Override
  public void setAsInteger( String columnType, String column, int newValue )
  {
    super.setAsString( columnType, column, Integer.toString( newValue ) );
  }

  @Override
  public void setAsDouble( String columnType, String column, double newValue )
  {
    super.setAsString( columnType, column, Double.toString( newValue ) );
  }

  @Override
  public void setAsBoolean( String columnType, String column, boolean newValue )
  {
    super.setAsBoolean( columnType, column, newValue );
  }

  public ImmutableCloudTableRow asImmutableCloudTableRow( )
  {
    return this;
  }
}
