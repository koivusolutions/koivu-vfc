package com.koivusolutions.vfc.client;

import java.util.NoSuchElementException;

import com.koivusolutions.vfc.client.responses.VfcResponse;

public interface VfcIterator<T> extends VfcResponse
{
  boolean hasNext( ) throws VfcClientException;

  T next( ) throws NoSuchElementException,
      VfcClientException;
}
