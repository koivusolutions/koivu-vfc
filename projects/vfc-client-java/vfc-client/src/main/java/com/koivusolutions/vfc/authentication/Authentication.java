package com.koivusolutions.vfc.authentication;

import com.koivusolutions.vfc.client.VfcClientException;

public interface Authentication
{

  String getAccessToken( ) throws VfcClientException;

  String getTokenType( );

}