package com.koivusolutions.vfc.client;

public enum UpdateStrategy
{
  INSERT, UPDATE, REPLACE, DELETE, PATCH, PATCH_REPLACE
}