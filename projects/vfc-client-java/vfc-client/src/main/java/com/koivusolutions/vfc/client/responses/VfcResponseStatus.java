package com.koivusolutions.vfc.client.responses;

import java.util.*;

import org.json.JSONObject;

public class VfcResponseStatus
{
  private final JSONObject response;
  private final JSONObject error;
  private final JSONObject pagination;
  private final JSONObject meta;

  public VfcResponseStatus( JSONObject serverResponse )
  {
    if( serverResponse != null )
    {
      response = serverResponse.optJSONObject( "response" );
      error = serverResponse.optJSONObject( "error" );
      pagination = serverResponse.optJSONObject( "pagination" );
      meta = serverResponse.optJSONObject( "meta" );
    }
    else
    {
      response = null;
      error = null;
      pagination = null;
      meta = null;
    }
  }

  public VfcResponseStatus( )
  {
    response = null;
    error = null;
    pagination = null;
    meta = null;
  }

  private Map<String, String> get( JSONObject parent )
  {
    if( parent != null )
    {
      Map<String, String> values = new HashMap<>( );
      String[] names = parent.getNames( parent );
      for( String name : names )
      {
        values.put( name, parent.get( name ).toString( ) );
      }
      return values;
    }
    return Collections.emptyMap( );
  }

  public Map<String, String> getResponse( )
  {
    return get( response );
  }

  public Map<String, String> getError( )
  {
    return get( error );
  }

  public Map<String, String> getPagination( )
  {
    return get( pagination );
  }

  public Map<String, String> getMeta( )
  {
    return get( meta );
  }

  public JSONObject getResponseAsJson( )
  {
    return response == null ? new JSONObject( ) : response;
  }

  public JSONObject getErrorAsJson( )
  {
    return error == null ? new JSONObject( ) : error;
  }

  public JSONObject getPaginationAsJson( )
  {
    return pagination == null ? new JSONObject( ) : pagination;
  }

  public JSONObject getMetaAsJson( )
  {
    return meta == null ? new JSONObject( ) : meta;
  }

  @Override
  public String toString( )
  {
    return "VfcResponseStatus [response=" + response + ", error=" + error + ", pagination="
        + pagination + ", meta=" + meta + "]";
  }
}
