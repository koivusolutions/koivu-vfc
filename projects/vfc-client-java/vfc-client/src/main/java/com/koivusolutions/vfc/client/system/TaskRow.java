package com.koivusolutions.vfc.client.system;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class TaskRow extends ImmutableTaskRow implements VfcObject
{
  public TaskRow( )
  {
    super( );
  }

  public TaskRow( JSONObject obj )
  {
    super( obj );
  }


  @Override
  public void setProperties( Map<String, String> properties )
  {
    super.setProperties( properties );
  }

  @Override
  public void addProperty( String name, String value )
  {
    super.addProperty( name, value );
  }

  public void setDescription( String description )
  {
    setFieldAsString( SYSTEM_DESCRIPTION, description );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  @Override
  public void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_DESCRIPTION, column ) )
      {
        setDescription( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }

  public ImmutableTaskRow asImmutableTasksRow( )
  {
    return this;
  }
}
