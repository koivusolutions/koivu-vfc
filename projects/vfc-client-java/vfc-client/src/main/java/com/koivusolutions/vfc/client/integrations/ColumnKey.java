package com.koivusolutions.vfc.client.integrations;

import org.apache.commons.lang3.StringUtils;

public class ColumnKey
{
  private final String id;
  private final String category;
  private final String group;
  private final String variable;
  private final String index;

  public ColumnKey( String id, String category, String group, String variable, int index )
  {
    super( );
    this.id = id;
    this.category = category;
    this.group = group;
    this.variable = variable;
    this.index = Integer.toString( index );
  }

  public ColumnKey( String[] keys )
  {
    super( );
    this.id = keys[0];
    this.category = keys[1];
    this.group = keys[2];
    this.variable = keys[3];
    this.index = keys[4];
  }

  public boolean isSameGroup( String otherId, String otherCategory, String otherGroup )
  {
    return StringUtils.equals( this.id, otherId )
        && StringUtils.equals( this.category, otherCategory )
        && StringUtils.equals( this.group, otherGroup );
  }

  public String getId( )
  {
    return id;
  }

  public String getCategory( )
  {
    return category;
  }

  public String getGroup( )
  {
    return group;
  }

  public String getVariable( )
  {
    return variable;
  }

  public String getIndex( )
  {
    return index;
  }

  @Override
  public int hashCode( )
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + ( ( category == null ) ? 0 : category.hashCode( ) );
    result = prime * result + ( ( group == null ) ? 0 : group.hashCode( ) );
    result = prime * result + ( ( id == null ) ? 0 : id.hashCode( ) );
    result = prime * result + ( ( index == null ) ? 0 : index.hashCode( ) );
    result = prime * result + ( ( variable == null ) ? 0 : variable.hashCode( ) );
    return result;
  }

  @Override
  public boolean equals( Object obj )
  {
    if( this == obj )
    {
      return true;
    }
    if( obj == null )
    {
      return false;
    }
    if( getClass( ) != obj.getClass( ) )
    {
      return false;
    }
    ColumnKey other = (ColumnKey)obj;
    if( category == null )
    {
      if( other.category != null )
      {
        return false;
      }
    }
    else if( !category.equals( other.category ) )
    {
      return false;
    }
    if( group == null )
    {
      if( other.group != null )
      {
        return false;
      }
    }
    else if( !group.equals( other.group ) )
    {
      return false;
    }
    if( id == null )
    {
      if( other.id != null )
      {
        return false;
      }
    }
    else if( !id.equals( other.id ) )
    {
      return false;
    }
    if( index == null )
    {
      if( other.index != null )
      {
        return false;
      }
    }
    else if( !index.equals( other.index ) )
    {
      return false;
    }
    if( variable == null )
    {
      if( other.variable != null )
      {
        return false;
      }
    }
    else if( !variable.equals( other.variable ) )
    {
      return false;
    }
    return true;
  }

}
