package com.koivusolutions.vfc.client.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;

abstract public class SystemTable<T extends VfcObject> implements VfcTableUpdates<T>
{
  protected final VfcClient vfcClient;
  private final String URL;

  public SystemTable( VfcClient vfcClient, String URL )
  {
    this.vfcClient = vfcClient;
    this.URL = URL;
  }

  protected String getUrl( )
  {
    return URL;
  }

  abstract protected T build( JSONObject obj );

  private void addParam( List<Pair<String, String>> queryParams, String name, String value )
  {
    if( StringUtils.isNotBlank( value ) )
    {
      queryParams.add( new ImmutablePair<String, String>( name, value ) );
    }
  }

  @Override
  public SystemUpdater<T> deleteFrom( )
  {
    return new SystemUpdater<T>( vfcClient, URL, UpdateStrategy.DELETE );
  }

  @Override
  public SystemUpdater<T> insertTo( )
  {
    return new SystemUpdater<T>( vfcClient, URL, UpdateStrategy.INSERT );
  }

  @Override
  public SystemUpdater<T> update( )
  {
    return new SystemUpdater<T>( vfcClient, URL, UpdateStrategy.UPDATE );
  }

  @Override
  public SystemUpdater<T> patch( )
  {
    return new SystemUpdater<T>( vfcClient, URL, UpdateStrategy.PATCH );
  }

  protected List<Pair<String, String>> addQueryParams( String key, String startswith, String filter,
      String impersonate )
  {
    List<Pair<String, String>> queryParams = new ArrayList<>( );
    addParam( queryParams, "key", key );
    addParam( queryParams, "startswith", startswith );
    addParam( queryParams, "filter", filter );
    addParam( queryParams, "impersonatebusiness", impersonate );
    return queryParams;
  }

}
