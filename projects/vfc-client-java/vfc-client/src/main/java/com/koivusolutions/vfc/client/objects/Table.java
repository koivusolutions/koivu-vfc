package com.koivusolutions.vfc.client.objects;

import java.util.*;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import com.koivusolutions.vfc.client.*;
import com.koivusolutions.vfc.client.responses.VfcResponseStatus;

public abstract class Table<T> extends VfcGetResponse
{
  private final List<Pair<String, String>> query;

  protected Table( VfcClient vfcClient, String URL, List<Pair<String, String>> query )
  {
    super( vfcClient, URL );
    this.query = Collections.unmodifiableList( query );
    initialize( );
  }

  abstract protected T build( JSONObject next );

  @Override
  protected List<Pair<String, String>> getQueryParams( )
  {
    return query;
  }

  public VfcIterator<T> iteratorEx( )
  {
    VfcIterator<JSONObject> jsonIt = super.read( );
    return new VfcIterator<T>( ) {

      @Override
      public boolean hasNext( ) throws VfcClientException
      {
        return jsonIt.hasNext( );
      }

      @Override
      public T next( ) throws NoSuchElementException,
          VfcClientException
      {
        return build( jsonIt.next( ) );
      }

      @Override
      public VfcResponseStatus getResponseStatus( ) throws VfcClientException
      {
        return jsonIt.getResponseStatus( );
      }
    };
  }

  public void reset( )
  {
    initialize( );
  }
}
