package com.koivusolutions.vfc.client.system;

import org.json.JSONObject;

import com.koivusolutions.vfc.client.VfcObject;

public class SequenceRow extends ImmutableSequenceRow implements VfcObject
{
  public SequenceRow( )
  {
    super( );
  }

  public SequenceRow( JSONObject obj )
  {
    super( obj );
  }

  @Override
  public void setId( String id )
  {
    super.setId( id );
  }

  @Override
  public void setNextValue( int nextValue )
  {
    super.setNextValue( nextValue );
  }

  @Override
  public void setTemplate( String template )
  {
    super.setTemplate( template );
  }

  @Override
  public JSONObject getAsJson( )
  {
    return super.getAsJson( );
  }

  public ImmutableSequenceRow asImmutablesequenceRow( )
  {
    return this;
  }
}
