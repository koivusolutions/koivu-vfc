package com.koivusolutions.vfc.client;

import java.util.*;
import java.util.concurrent.Future;

import org.apache.commons.lang3.tuple.*;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class VfcUpdateResponse<T extends VfcObject> extends VfcUpdateResponseBase<T>
    implements VfcUpdater<T>
{
  protected final LinkedList<Pair<List<? extends VfcRowUpdateStatus<T>>, Future<JSONObject>>> responses = new LinkedList<>( );
  List<VfcRowUpdateStatus<T>> rows = new ArrayList<>( );
  private boolean skipIdentical = false;

  public VfcUpdateResponse( VfcClient vfcClient, UpdateStrategy updateStrategy )
  {
    super( vfcClient, updateStrategy );

  }

  @Override
  public VfcUpdater<T> addUpdate( T row )
  {
    synchronized ( this )
    {
      rows.add( new VfcRowUpdateStatus<T>( row ) );
      if( rows.size( ) >= vfcClient.getBatchSize( ) )
      {
        flush( );
      }
    }
    return this;
  }

  @Override
  public VfcUpdater<T> flush( )
  {
    List<VfcRowUpdateStatus<T>> batchRows;
    synchronized ( this )
    {
      batchRows = rows;
      rows = new ArrayList<>( );
    }
    if( batchRows != null && batchRows.size( ) > 0 )
    {
      write( batchRows );
    }
    return this;
  }

  Future<JSONObject> write( JSONArray dataArray )
  {
    JSONObject data = new JSONObject( );
    switch( updateStrategy )
    {
      case UPDATE:
      case PATCH:
      case PATCH_REPLACE:
        data.put( "editData", dataArray );
        break;

      default:
        data.put( "data", dataArray );
        break;
    }
    List<Pair<String, String>> requestQueryParams = null;
    if( skipIdentical )
    {
      requestQueryParams = new ArrayList<>( );
      requestQueryParams.add( new ImmutablePair<>( "skipIdentical", "true" ) );
    }

    Future<JSONObject> response = vfcClient.sendToVfcAsync( getUrl( ), requestQueryParams, data,
        updateStrategy );
    return response;
  }

  protected void write( List<? extends VfcRowUpdateStatus<T>> batchRows )
  {
    JSONArray dataArray = new JSONArray( );
    for( VfcRowUpdateStatus<T> rowUpdateStatus : batchRows )
    {
      dataArray.put( rowUpdateStatus.getRow( ).getAsJson( ) );
    }
    synchronized ( responses )
    {
      responses.add( new MutablePair<List<? extends VfcRowUpdateStatus<T>>, Future<JSONObject>>(
          batchRows, write( dataArray ) ) );
    }
  }

  @Override
  public VfcIterator<VfcRowUpdateStatus<T>> getResponses( )
  {
    return getResponses( false );
  }

  @Override
  public VfcIterator<VfcRowUpdateStatus<T>> getAllResponses( )
  {
    flush( );
    return getResponses( true );
  }

  private VfcIterator<VfcRowUpdateStatus<T>> getResponses( boolean all )
  {
    return new VfcResponseIterator<T>( responses, all );
  }

  @Override
  public void checkErrors( ) throws VfcClientException
  {
    checkErrors( false );
  }

  @Override
  public void checkAllErrors( ) throws VfcClientException
  {
    flush( );
    checkErrors( true );
  }

  private void checkErrors( boolean all ) throws VfcClientException
  {
    VfcIterator<VfcRowUpdateStatus<T>> resp = getResponses( all );
    while( resp.hasNext( ) )
    {
      VfcQueryUpdateStatus reponse = resp.next( );
      if( reponse.isError( ) )
      {
        throw new VfcClientException( "Data update error:" + reponse.getResponseMessage( ) );
      }
    }
  }

  public boolean isSkipIdentical( )
  {
    return skipIdentical;
  }

  @Override
  public void setSkipIdentical( boolean skipIdentical )
  {
    this.skipIdentical = skipIdentical;
  }

  @Override
  public String toString( )
  {
    return "VfcUpdateResponse [responses="
        + String.valueOf( responses != null ? responses.size( ) : 0 ) + ", rows="
        + String.valueOf( rows != null ? rows.size( ) : 0 ) + "]";
  }

}
