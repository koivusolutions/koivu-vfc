package com.koivusolutions.vfc.client.objects;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

public abstract class SequenceObject extends AuditableObject
{
  public static final String SYSTEM_ID = "id";
  public static final String SYSTEM_NEXT_VALUE = "nextValue";
  public static final String SYSTEM_TEMPLATE = "template";

  public SequenceObject( JSONObject obj )
  {
    super( obj );
  }

  public String getId( )
  {
    return getFieldAsString( SYSTEM_ID );
  }

  protected void setId( String id )
  {
    setFieldAsString( SYSTEM_ID, id );
  }

  public int getNextValue( )
  {
    return getFieldAsInt( SYSTEM_NEXT_VALUE );
  }

  protected void setNextValue( int nextValue )
  {
    setFieldAsInt( SYSTEM_NEXT_VALUE, nextValue );
  }

  public String getTemplate( )
  {
    return getFieldAsString( SYSTEM_TEMPLATE );
  }

  protected void setTemplate( String template )
  {
    setFieldAsString( SYSTEM_TEMPLATE, template );
  }

  @Override
  public String getAsString( String columnType, String column, String defaultValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_ID, column ) )
      {
        return getId( );
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_NEXT_VALUE, column ) )
      {
        return String.valueOf( getNextValue( ) );
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_TEMPLATE, column ) )
      {
        return getTemplate( );
      }
    }
    return super.getAsString( columnType, column, defaultValue );
  }

  @Override
  protected void setAsString( String columnType, String column, String newValue )
  {
    if( StringUtils.equals( COLUMN_TYPE_SYSTEM, columnType ) )
    {
      if( StringUtils.equalsIgnoreCase( SYSTEM_ID, column ) )
      {
        setId( newValue );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_NEXT_VALUE, column ) )
      {
        setNextValue( Integer.valueOf( newValue ).intValue( ) );
        return;
      }
      if( StringUtils.equalsIgnoreCase( SYSTEM_TEMPLATE, column ) )
      {
        setTemplate( newValue );
        return;
      }
    }
    super.setAsString( columnType, column, newValue );
  }
}