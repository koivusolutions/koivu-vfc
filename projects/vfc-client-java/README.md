# Koivu.Cloud

## Koivu.Cloud Client

This is a Java client library for Koivu.Could. The features include:

* Authentication (Firebase or Auth0)
* Client to Server communication batching
* Client to Server communication compression

### Creating a connection to Koivu.Cloud

#### Prerequisites

* Client id from authentication service
* Client secret from authentication service
* URL to Koivu.Cloud server instance
* Authentication tenant
* Koivu.Cloud solution id
* Koivu.Cloud tenant id

#### Creating a client connection

* Creating a properties object and providing implementation specific values

```java
    VfcProperties vfcProperties = new VfcProperties( );
    vfcProperties.put( "authentication.firebase.apiKey", "my_firebase_api_key" )
    vfcProperties.put( "vfc.api.tenant", "my_organization_id" )
    vfcProperties.put( "vfc.api.solution", "my_solution_name" )
```

OR you can use a property file for implementation specific values and pass that to the VfcProperties constructor.

```java
    VfcProperties vfcProperties = new VfcProperties( getClass( ).getResourceAsStream( VFC_CLIENT_PROPERTIES_FILE ) );
```

* Creating a URL

```java
    URL vfcURL = new URL( "my_koivu_cloud_instance_url" );
```

* Redirecting log messages from the Koivu.Cloud client library to your logging utility

```java
     import com.koivusolutions.vfc.logging.Log;
     import com.koivusolutions.vfc.logging.Logger;
     public class Logging implements Log
     { ... }

     Logging logging = new Logging( );
     Logger.setExternalLogger( logging );
```

* Creating a Koivu.Cloud client object

```java
     String vfcClientId = ...
     String vfcClientSecret = ...
     VfcClient vfc = new VfcClient( vfcURL, vfcProperties, vfcClientId, vfcClientSecret );
```

### Reading data

An example of how you can read all rows from a Cloud Table named 'Test' and that has keys one and two titled 'k1' and 'k2':

```java
CloudTable t = vfc.getCloudTables( ).getCloudTableKeysStartingWith( "Test", new String[]{ "k1","k2" } );
VfcIterator<CloudTableRow> it = t.iteratorEx( );
while( it.hasNext( ) )
{
   CloudTableRow row = it.next( );
   String category = row.getCategory( );
   String[] keys = row.getKeys( );
   String desc= row.getDescription( );
   Map<String, Object> prop = row.getProperties( );
   String modifier = row.getLastModifiedBy( );
}
```

It is possible to read by exact keys, keys starting with or by key range. You can further limit the results by defining a filter parameter and add calculated fields to the response with expression parameters.

### Updating data

Inserting data: Insert will fail, if the row already exists.

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );
```

Updating existing data. Update will fail, if row does not exists.

```java
CloudTableUpdater updater = vfc.getCloudTables( ).updateCloudTable( );
```

Inserting or Updating data: A new row will be added, if it does not already exist. Otherwise the row is updated.

```java
CloudTableUpdater updater = vfc.getCloudTables( ).replaceInCloudTable( );
```

Deleting existing data: Delete will fail, if the row does not exist.

```java
CloudTableUpdater updater = vfc.getCloudTables( ).deleteFromCloudTable( );
```

An example of inserting data rows without reading the response status:

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );

CloudTableRow r1 = new CloudTableRow( );
r1.setCategory( "Test" );
r1.setKeys( new String[]{"k1", "k2", "k3.1"} );
r1.setDescription( "Test row 1" );
r1.addProperty( "props1", "value1" );
r1.addProperty( "props2", "value2" );
updater.addUpdate( r1 );

CloudTableRow r2 = new CloudTableRow( );
r2.setCategory( "Test" );
r2.setKeys( new String[]{"k1", "k2", "k3.2"} );
r2.setDescription( "Test row 2" );
r2.addProperty( "props1", "value3" );
r2.addProperty( "props2", "value4" );
updater.addUpdate( r2 );

updater.flush( );
```

An example of inserting data rows and throwing an exception, if the inserts fail:

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );
Iterator<CloudTableRow> rowsToInsert = ...
while(rowsToInsert.hasNext( ))
{
   updater.addUpdate( rowsToInsert.next() );
}
updater.checkAllErrors(); // throws exception, if there are errors in inserts
```

An example of reading the insert results row by row:

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );
while(rowsToInsert.hasNext( ))
{
   updater.addUpdate( rowsToInsert.next() );
}

VfcIterator<VfcRowUpdateStatus<CloudTableRow>> responses = updater.getAllResponses( );
while( responses.hasNext( ) )
{
   VfcRowUpdateStatus<CloudTableRow> resp = responses.next( );
   System.out.println( resp.getResponse( ) + ":" + resp.getResponseMessage( ) );
}
```

### Handling large data sets

Reading large result sets from Cloud Tables are handled by default. Data is fetched in batches from the Koivu.Cloud server in background threads in compressed streams.

Updating large data sets are also automatically sent as batches in background threads and in compressed streams. It is good practise to read responses during the update loops as it frees memory and makes it possible to do early exits. There are two sets of methods for that:

* ``checkErrors()`` and ``getResponses()``
These methods do not flush the batch buffers and only process responses that already got back from the Koivu.Cloud servers in background threads. In other words, the iterator returned or used inside these methods can contain zero elements, if no batch is sent to the servers or the server has not yet responded.

* ``checkAllErrors()`` and ``getAllResponses()``
These methods flush the batch buffers and wait for responses from the Koivu.Cloud servers before execution. In other words, these methods guarantee that all data is sent to the server and that the server response is read.

Example 1 of handling large data sets:

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );
Iterator<CloudTableRow> rowsToInsert = ...
while(rowsToInsert.hasNext( ))
{
   updater.addUpdate( rowsToInsert.next() );
   updater.checkErrors(); // Throws an exception, if there are errors in the inserts. Does not block execution.
}
updater.checkAllErrors(); // Flushes buffers and throws an exception, if there are errors in the inserts.
```

Example 2 of handling large data sets:

```java
CloudTableUpdater updater = vfc.getCloudTables( ).insertToTable( );
while(rowsToInsert.hasNext( ))
{
   updater.addUpdate( rowsToInsert.next() );
   VfcIterator<VfcRowUpdateStatus<CloudTableRow>> responses = updater.getResponses( ); // Does not block execution.
   while( responses.hasNext( ) )
   {
     VfcRowUpdateStatus<CloudTableRow> resp = responses.next( );
     System.out.println( resp.getResponse( ) + ":" + resp.getResponseMessage( ) );
   }
}

VfcIterator<VfcRowUpdateStatus<CloudTableRow>> responses = updater.getAllResponses( );  // Flush buffers and reads the responses.
while( responses.hasNext( ) )
{
   VfcRowUpdateStatus<CloudTableRow> resp = responses.next( );
   System.out.println( resp.getResponse( ) + ":" + resp.getResponseMessage( ) );
}
```

### Default properties

```properties
### Authentication

# Authentication type: firebase / auth0 
authentication.type = firebase

# Firebase
# Firebase URL to retrieve access token
authentication.firebase.url.token = https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword
# Firebase URL to retrieve account info
authentication.firebase.url.account = https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo
# Firebase apiKey
authentication.firebase.apiKey = 

# Auth0
# Auth0 tenant name
authentication.auth0.tenant = 
# Auth0 URL to retrieve access token
authentication.auth0.url.token = https://%s.eu.auth0.com/oauth/token
# Auth0 API audience 
authentication.auth0.url.audience = https://koivuapi.koivusolutions.com


### Koivu.Cloud

# Koivu.Cloud Client Id
vfc.client.id = Koivu.Cloud Client
# Koivu.Cloud API version
vfc.api.version = valueflow/v1/
# Koivu.Cloud API tenant id
vfc.api.tenant =
# Koivu.Cloud API solution id
vfc.api.solution =
# Koivu.Cloud API branch
vfc.api.branch =
# Koivu.Cloud API batch size
vfc.api.batchsize = 2048

### HTTP

# Http client settings
http.connection.poolsize = 20
http.connection.sockettimeout = 5000
http.connection.connectiontimeout = 5000
http.connection.connectionrequesttimeout = 5000

# Proxy
http.proxy.host =
http.proxy.port =
http.proxy.scheme =

# Compression
http.gzip.limit = 1024
```